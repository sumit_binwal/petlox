//
//  SignUpVC.h
//  Petlox
//
//  Created by Sumit Sharma on 27/07/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

//Facebook login
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

//Twitter login
#import "CSTwitterEngine.h"

//Google Login
#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>

@class GPPSignInButton;

@interface SignUpVC : UIViewController<GPPSignInDelegate,CSTwitterEngineAccessTokenDelegate>

@property (weak, nonatomic) IBOutlet UILabel *profilePhotoLabel;
#pragma mark - IBAction Button
- (IBAction)btnGoogleLogin:(id)sender;

@property (strong, nonatomic) IBOutlet GPPSignInButton *btnGoogleLogin;

@end
