//
//  ChooseHoursCustomeCell.m
//  Petlox
//
//  Created by Sumit Sharma on 02/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "ChooseHoursCustomeCell.h"

@implementation ChooseHoursCustomeCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    
        self=(ChooseHoursCustomeCell *)[[[NSBundle mainBundle]loadNibNamed:@"ChooseHoursCustomeCell" owner:self options:nil]objectAtIndex:0];
   
    return self;
}
- (void)awakeFromNib {
    // Initialization code
    
    [_lblTime setFont:[UIFont fontWithName:self.lblTime.font.fontName size:self.lblTime.font.pointSize*SCREEN_XScale]];
    [_lblDayName setFont:[UIFont fontWithName:_lblDayName.font.fontName size:_lblDayName.font.pointSize*SCREEN_XScale]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
