//
//  BusinessSignupSecndVC.m
//  Petlox
//
//  Created by Sumit Sharma on 01/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "BusinessSignupSecndVC.h"
#import "ChooseBusinessCateVC.h"
@interface BusinessSignupSecndVC ()<UITextFieldDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    IBOutlet UIImageView *profileImgVw;
    
    IBOutlet UIView *vw1;
    IBOutlet UIView *vw2;
    IBOutlet UIView *vw3;
    IBOutlet UIButton *btnProfilePic;
    IBOutlet UIButton *btnNext;
    IBOutlet UIView *vwProfileImg;
    IBOutlet UIView *cntainerVw;
    
    IBOutlet UIImageView *imgVwProfile;
    IBOutlet UIScrollView *scrllVw;
    
    IBOutlet UITextField *txtPassword;

    IBOutlet UITextField *txtLastName;
    IBOutlet UITextField *txtFirstName;
    
    IBOutlet NSLayoutConstraint *heightCnstraint;
    
    IBOutlet UILabel *lblPrivacyPolicy;
}
@end

@implementation BusinessSignupSecndVC
@synthesize dataDict,claimDataDict;
#pragma mark - Life Cycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self isNotNull:[[NSUserDefaults standardUserDefaults] objectForKey:UD_CLAIM_ID]])
    {
        if (claimDataDict.count>0) {
          
            txtFirstName.text=[claimDataDict objectForKey:@"first_name"];
            txtLastName.text=[claimDataDict objectForKey:@"last_name"];
            
        }
    }
    
}

#pragma mark - View Setup Method

-(void)setUpView
{
    btnNext.layer.cornerRadius=btnNext.bounds.size.width/2*SCREEN_XScale;
    [btnNext.titleLabel setFont:[UIFont fontWithName:btnNext.titleLabel.font.fontName size:btnNext.titleLabel.font.pointSize*SCREEN_XScale]];
    
    txtLastName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    txtPassword.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    txtFirstName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    
    UIView *view1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 13.0*SCREEN_XScale, 0)];
    txtFirstName.leftViewMode=UITextFieldViewModeAlways;
    txtFirstName.leftView=view1;
    
    UIView *view2=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 13.0*SCREEN_XScale, 0)];
    txtLastName.leftView=view2;
    txtLastName.leftViewMode=UITextFieldViewModeAlways;
    
    UIView *view3=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 13.0*SCREEN_XScale, 0)];
    txtPassword.leftView=view3;
    txtPassword.leftViewMode=UITextFieldViewModeAlways;
    
    view1.layer.cornerRadius=5.0f;
    view2.layer.cornerRadius=5.0f;
    view3.layer.cornerRadius=5.0f;
    
    [lblPrivacyPolicy setFont:[UIFont fontWithName:lblPrivacyPolicy.font.fontName size:lblPrivacyPolicy.font.pointSize*SCREEN_XScale]];
    vw1.layer.cornerRadius=5.0f;
    vw2.layer.cornerRadius=5.0f;
    vw3.layer.cornerRadius=5.0f;
    
    
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    paragraphStyle.lineSpacing = 2.5;
    
    NSDictionary *nameAttributes=@{
                                   NSParagraphStyleAttributeName : paragraphStyle,
                                   NSBaselineOffsetAttributeName:@0.0
                                   };
    
    
    NSAttributedString *string=[[NSAttributedString alloc] initWithString:@"By clicking the button below, you represent that you have authority to claim this account on behalf of this business, and agree to Petlox's Terms of Service and Privacy Policy." attributes:nameAttributes];
    lblPrivacyPolicy.attributedText=string;
    
    if ([UIScreen mainScreen].bounds.size.height<568) {
        heightCnstraint.constant=20.0f;
    }
    
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Business Sign Up"];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];

    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tabGestureBtnClicked)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TextField Validation Method
-(BOOL)isTextFieldTextValidate
{
    if (![CommonFunctions isValueNotEmpty:txtFirstName.text]) {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter your first name."];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtLastName.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter your last name."];
        return NO;
    }
        else if (![CommonFunctions isValueNotEmpty:txtPassword.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter password."];
        return NO;
    }
    else if (txtPassword.text.length<6)
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Password length between 6 to 14 characters."];
        return NO;
    }
    return YES;
    
    
}

#pragma mark - IBAction Button

- (IBAction)nextBtnClicked:(id)sender {
    [self.view endEditing:YES];
    [scrllVw setContentOffset:CGPointZero];
    if ([self isTextFieldTextValidate]) {
        if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self submitBusinessDetail];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please check your internet connection."];
        }
    }
}

-(void)tabGestureBtnClicked
{
    [self.view endEditing:YES];
    [scrllVw setContentOffset:CGPointZero];
    [scrllVw setScrollEnabled:NO];
}
- (IBAction)setProfileImgButtonClicked:(id)sender {
    
    UIActionSheet *actionSht=[[UIActionSheet alloc]initWithTitle:@"Choose Image" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Picture",@"Choose From Gallery", nil];
    [actionSht showInView:self.view];
}
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Navigation Controller Delegate Method
-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    navigationController.navigationBar.backgroundColor=[UIColor colorWithRed:71.0f/255.0f green:13.0f/255.0f blue:52.0f/255.0f alpha:1];
    
    UIView *statusView=[[UIView alloc]initWithFrame:CGRectMake(0, -[UIApplication sharedApplication].statusBarFrame.size.height,[UIApplication sharedApplication].statusBarFrame.size.width, [UIApplication sharedApplication].statusBarFrame.size.height)];
    [statusView setBackgroundColor:[UIColor colorWithRed:71.0f/255.0f green:13.0f/255.0f blue:52.0f/255.0f alpha:1]];
    [navigationController.navigationBar addSubview:statusView];
    [navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:FONT_GULIM size:18.85f]}];
    
    viewController.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    
}

#pragma mark - UIActionSheet Delegate method
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1) {

        
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.allowsEditing = YES;
        imagePickerController.delegate = self;
        
       imagePickerController.modalPresentationCapturesStatusBarAppearance = true;

        [self presentViewController:imagePickerController animated:YES completion:NULL];


    }
    else if(buttonIndex==0)
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate = self;
        imagePickerController.allowsEditing = YES;
        imagePickerController.sourceType =  UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePickerController animated:YES completion:nil];

    }
}

#pragma mark - UIImagePicker Delegate Method

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //You can retrieve the actual UIImage
    UIImage *image = [info valueForKey:UIImagePickerControllerEditedImage];
    //Or you can get the image url from AssetsLibrary
    NSURL *path = [info valueForKey:UIImagePickerControllerReferenceURL];
    NSLog(@"%@",image);
        NSLog(@"%@",path);
    UIImage *imageEdited=  [self scaleAndRotateImage:image];

    [imgVwProfile setImage:imageEdited];
    
    imgVwProfile.clipsToBounds=YES;
    [picker dismissViewControllerAnimated:YES completion:^{
    }];
}

#pragma mark - UIScrollView Methods

-(void)scrollViewToCenterOfScreen:(UITextField *)textField
{
    [scrllVw setScrollEnabled:YES];
    float difference;
    if (scrllVw.contentSize.height == 600)
        difference = 50.0f;
    else
        difference = 50.0f;
    CGFloat viewCenterY = textField.center.y+cntainerVw.frame.origin.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat avaliableHeight = applicationFrame.size.height - 5.0f;
    CGFloat y = viewCenterY - avaliableHeight / 10.0f;
    NSLog(@"%f",y);
    if (y < 0)
        y = 0;
    [scrllVw setContentOffset:CGPointMake(0, y) animated:YES];
    [scrllVw setContentSize:CGSizeMake(320.0f, 630.0f)];
}


#pragma mark - UITextField Delegate Methods

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField==txtFirstName ||textField==txtLastName) {
        if (range.location>20) {
            return NO;
        }
        else
        {
            return YES;
        }
    }

    else if (textField==txtPassword) {
        if (range.location>14) {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else
    {
        return YES;
    }
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self scrollViewToCenterOfScreen:textField];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==txtFirstName) {
        [txtLastName becomeFirstResponder];
    }
    else if (textField==txtLastName)
    {
          [txtPassword becomeFirstResponder];
    }
    else if (textField==txtPassword)
    {
        [txtPassword resignFirstResponder];
        [scrllVw setContentOffset:CGPointZero];
        [scrllVw setScrollEnabled:NO];
    }
    return YES;
}

#pragma mark- WebServices API..

-(void)submitBusinessDetail
{
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:UD_CLAIM_ID]);
    NSMutableDictionary *param;
    NSString *url;
    if ([self isNotNull:[[NSUserDefaults standardUserDefaults] objectForKey:UD_CLAIM_ID]])
    {

        param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[CommonFunctions trimSpaceInString:txtFirstName.text],@"first_name",[CommonFunctions trimSpaceInString:txtLastName.text],@"last_name",dataDict[@"bEmail"],@"email",[CommonFunctions trimSpaceInString:txtPassword.text],@"password",@"business",@"usertype",[[NSUserDefaults standardUserDefaults] valueForKey:UD_CLAIM_ID],@"id",[dataDict objectForKey:@"bPhnNumber"],@"phone",[dataDict objectForKey:@"bName"],@"business_name",[dataDict objectForKey:@"bAddress"],@"address",[NSString stringWithFormat:@"%f",CurrentLatitude],@"lat",[NSString stringWithFormat:@"%f",CurrentLongitude],@"lon",pushDeviceToken,@"device_token",@"iphone",@"device_type", nil];
        url= [NSString stringWithFormat:@"%@updategetProfile",serverURL];
    }
    else
    {
        param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[CommonFunctions trimSpaceInString:txtFirstName.text],@"first_name",[CommonFunctions trimSpaceInString:txtLastName.text],@"last_name",dataDict[@"bEmail"],@"email",[CommonFunctions trimSpaceInString:txtPassword.text],@"password",@"business",@"usertype",[dataDict objectForKey:@"bPhnNumber"],@"phone",[dataDict objectForKey:@"bName"],@"business_name",[dataDict objectForKey:@"bAddress"],@"address",[NSString stringWithFormat:@"%f",CurrentLatitude],@"lat",[NSString stringWithFormat:@"%f",CurrentLongitude],@"lon",pushDeviceToken,@"device_token",@"iphone",@"device_type", nil];
       url = [NSString stringWithFormat:@"%@signup",serverURL];

    }
    
    NSLog(@"%@",param);
       AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:url]];
    
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    AFHTTPRequestOperation *op = [manager POST:@"" parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if (imgVwProfile.image != nil) {
            NSData *imageData = UIImageJPEGRepresentation(imgVwProfile.image, 0.8f);

            [formData appendPartWithFileData:imageData name:@"image" fileName:@"ProfileImage.jpg" mimeType:@"image/jpeg"];
        }
        
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        
        if (responseObject != nil)
        {
            
            NSDictionary *jsonDic = (NSDictionary *)responseObject;
            NSLog(@"Profile  JSON: %@",jsonDic);
            
            if (jsonDic != nil)
            {
                if ([[jsonDic objectForKey:@"replyCode"] isEqualToString:@"success"])
                {
                    ChooseBusinessCateVC *cbvc=[[ChooseBusinessCateVC alloc]init];
                    [[NSUserDefaults standardUserDefaults]setObject:[jsonDic objectForKey:@"token"] forKey:UD_TOKEN_ID];
                    [[NSUserDefaults standardUserDefaults]setObject:[jsonDic objectForKey:@"user_id"] forKey:UD_CLAIM_ID];
                    [[NSUserDefaults standardUserDefaults]setObject:[jsonDic objectForKey:@"type"] forKey:UD_BUSINESS_TYPE];
                    [[NSUserDefaults standardUserDefaults]setObject:[jsonDic objectForKey:@"first_name"] forKey:UD_FIRST_NAME];
                    [[NSUserDefaults standardUserDefaults]setObject:[jsonDic objectForKey:@"last_name"] forKey:UD_LAST_NAME];
                    [[NSUserDefaults standardUserDefaults]setObject:[jsonDic objectForKey:@"image"] forKey:UD_USER_IMG];
                    [[NSUserDefaults standardUserDefaults]setObject:[jsonDic objectForKey:@"business"] forKey:UD_BUSINESS_NAME];
                    [[NSUserDefaults standardUserDefaults]setObject:[jsonDic objectForKey:@"email"] forKey:UD_BUSINESS_EMAIL];
                    [[NSUserDefaults standardUserDefaults]setObject:[jsonDic objectForKey:@"phone"] forKey:UD_BUSINESS_PHONE];
                    
                    [[NSUserDefaults standardUserDefaults]synchronize];

                    [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_STATUS_COMPLETE];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    [self.navigationController pushViewController:cbvc animated:YES];
                }
                else
                {
                    [CommonFunctions alertTitle:@"" withMessage:[jsonDic objectForKey:@"replyMsg"]];
                }

            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[jsonDic objectForKey:@"replyMsg"]];
            }
        }
        [CommonFunctions removeActivityIndicator];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
        [CommonFunctions removeActivityIndicator];
    }];
    [op start];
}


//image Scale And Mirror image Rotation
-(UIImage *)scaleAndRotateImage:(UIImage *)image
{
    int kMaxResolution = 1242; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}
@end
