//
//  MapVC.h
//  Petlox
//
//  Created by Sumit Sharma on 27/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapVC : UIViewController
@property(nonatomic,strong)NSMutableDictionary *dictUserData;
@property(nonatomic,strong)NSMutableArray *arrayUserData;
@end
