//
//  SettingsCell.h
//  Petlox
//
//  Created by Suchita Bohra on 21/07/16.
//  Copyright © 2016 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *lblTxt;
@property (nonatomic, strong) IBOutlet UIImageView *imgViewArrow;
@property (strong, nonatomic) IBOutlet UIImageView *imgVwDevider;

@end
