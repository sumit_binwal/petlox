//
//  PetGrommingVC.m
//  DemoPetLox
//
//  Created by Chagan Singh on 11/09/15.
//  Copyright (c) 2015 Chagan Singh. All rights reserved.
//

#import "PetGrommingVC.h"
#import "PetGromingCell.h"

#import "ConnectionManager.h"
#import "UIImageView+WebCache.h"
#import "ServiceProviderVC.h"
#import "MapVC.h"
@interface PetGrommingVC ()<UITextFieldDelegate>
{
    NSMutableArray *arrCompleteLoadRecords;
}
@end

@implementation PetGrommingVC

@synthesize strKeyword,strSearchCategoryType,strBusinessCategory;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSLog(@"%@",strKeyword);
    
    if (!strKeyword.length>0) {
        strKeyword=@"";
    }
    txtSearchField.text=strKeyword;
//    if ([strKeyword isEqualToString:@"Grooming"] || [strKeyword isEqualToString:@"Training"] || [strKeyword isEqualToString:@"Walking"] || [strKeyword isEqualToString:@"Sitting"] ) {
//        strBusinessCategory=@"";
//    }
    
    [searchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"search-bg"] forState:UIControlStateNormal];

    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    [txtSearchField setValue:[UIColor colorWithRed:133.0f/255.0f green:133.0f/255.0f blue:133.0f/255.0f alpha:1]
                  forKeyPath:@"_placeholderLabel.textColor"];


    [self.navigationItem setHidesBackButton:YES];
    
    UIBarButtonItem *backButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self
action:@selector(barButtonBackPressed)];
    backButton.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    
    self.navigationItem.leftBarButtonItem = backButton;
    self.navigationController.navigationBarHidden=YES;
    
    arrGroomers=[[NSMutableArray alloc]init];
    arrAllrecords=[[NSMutableArray alloc]init];
    
    pageValue=1;
    lastLoad = 0;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideAllKeyboards)];
    tapGesture.cancelsTouchesInView = NO;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];
    
  }
-(void) hideAllKeyboards {
    
//    if (txtSearchField.text.length>1) {
//        pageValue=1;
//lastLoad = 0;
//        [arrGroomers removeAllObjects];
//        arrGroomers=[[NSMutableArray alloc]init];
//        [self.view endEditing:YES];
//        strKeyword=@"";
//        txtSearchField.text=@"";
//        [self getResultsForKey:@"" andForCategory:@"1" withPage:pageValue];
//        
//    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch
{
    NSLog(@"%@",[touch.view class]);
    if([touch.view class] == tblPetGromming.class){
        return NO;
    }
    return YES;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
//    [APPDELEGATE showPetReservationTabBar:NO];
    
//    [APPDELEGATE.petReservationTabBar removeFromSuperview];
//    [APPDELEGATE.bottomBar setFrame:CGRectZero];
//    [APPDELEGATE.bottomBar setHidden:YES];
//    [APPDELEGATE.window  sendSubviewToBack:APPDELEGATE.bottomBar];
    [APPDELEGATE.bottomBar setBackgroundColor:[UIColor clearColor]];
    [APPDELEGATE.petReservationTabBar removeFromSuperview];
    [APPDELEGATE.window.maskView removeFromSuperview];
    
    NSLog(@"%@",APPDELEGATE.window.subviews);
    arrGroomers=[[NSMutableArray alloc]init];
    arrAllrecords=[[NSMutableArray alloc]init];
    pageValue=1;
    lastLoad=0;
        [self.navigationController.navigationBar setHidden:YES];
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getResultsForKey:strKeyword andForCategory:strSearchCategoryType withPage:pageValue];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }
}
-(void)barButtonBackPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnBackPressed:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnMapClicked:(id)sender {
    
    if(arrGroomers.count > 0){
        MapVC *mapVC=[[MapVC alloc]init];
        mapVC.arrayUserData=arrGroomers;
        [self.navigationController pushViewController:mapVC animated:YES];
    } else {
        [CommonFunctions alertTitle:@"" withMessage:@"No records found to show on map"];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrGroomers.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PetGromingCell *cell=[tableView dequeueReusableCellWithIdentifier:@"PetGromingCell"];
    
    if (cell==nil)
    {
        cell=[[PetGromingCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PetGromingCell"];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(PetGromingCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *firstName=[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"business_name"];

    cell.lblNameGrommer.text=[NSString stringWithFormat:@"%d. %@",(int)indexPath.row+1,[firstName capitalizedString]];
    cell.lblAddress.text=[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"address"];
    
    if ([[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"review_count"]!=[NSNull null])
    {
        int reviewCount=[[[arrGroomers objectAtIndex:indexPath.row]objectForKey:@"review_count"] intValue];
        if (reviewCount>1) {
            cell.lblNoOfreview.text=[NSString stringWithFormat:@"%@ reviews",[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"review_count"]];
        }
        else
        {
            cell.lblNoOfreview.text=[NSString stringWithFormat:@"%@ review",[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"review_count"]];
        }
    }
    NSLog(@"%@",[[arrGroomers objectAtIndex:indexPath.row]objectForKey:@"fav"]);

    if ([self isNotNull:[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"fav"]]) {
            NSString *favValue=[NSString stringWithFormat:@"%@",[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"fav"]];
        if ([favValue isEqualToString:@"1"]) {
            [cell.imgFavourate setImage:[UIImage imageNamed:@"favorite_foucs_icon"]];
        }
        else
        {
            [cell.imgFavourate setImage:[UIImage imageNamed:@"favorite_icon"]];
        }
    }
    else
    {
          //  [cell.imgFavourate setImage:[UIImage imageNamed:@"favorite_icon"]];
    }
    
//    cell.imgProfileView.layer.cornerRadius=cell.imgProfileView.frame.size.width/2;
    cell.imgProfileView.layer.borderWidth=2;
    cell.imgProfileView.layer.borderColor=[UIColor clearColor].CGColor;
    cell.imgProfileView.layer.masksToBounds=YES;
    
    NSString *imgURL=[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"image"];
    if (imgURL.length>1) {

        [cell.imgProfileView setImageWithURL:[NSURL URLWithString:[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"image"]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
    }
    else
    {
        [cell.imgProfileView setImage:[UIImage imageNamed:@"DefaultProfileImg"]];
    }
    

    if ([[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"distance"] !=[NSNull null])
    {
        NSString *strDistance=[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"distance"];
        if (strDistance.length>0)
        {
            NSString *strMile=[NSString stringWithFormat:@"%@mi",strDistance];
            strMile=[strMile stringByReplacingOccurrencesOfString:@"mi" withString:@""];
             cell.lblDistance.text=[NSString stringWithFormat:@"%@mi",strMile];
        }
    }
    
    
    if ([[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"avg_rate"] !=[NSNull null])
    {
        int stars=[[[arrGroomers objectAtIndex:indexPath.row] objectForKey:@"avg_rate"] intValue];
        
        for (int i=0; i<stars; i++)
        {
            [[cell.imsStars objectAtIndex:i] setHighlighted:YES];
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ServiceProviderVC *service=[[ServiceProviderVC alloc]initWithNibName:@"ServiceProviderVC" bundle:nil];
    
    APPDELEGATE.K_dictUserDdetails = (NSMutableDictionary*)[arrGroomers objectAtIndex:indexPath.row];
    service.dictUserDdetails=(NSMutableDictionary*)[arrGroomers objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:service animated:YES];
}


- (IBAction)searchTextChanged:(UITextField *)searchText {
    
    arrGroomers=[[NSMutableArray alloc]init];
    
    NSString *strTxtSearch = [searchText.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    if (strTxtSearch.length>0)
    {
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"self.business_name contains[cd] %@",strTxtSearch];
        arrGroomers = [[arrCompleteLoadRecords filteredArrayUsingPredicate:resultPredicate] mutableCopy];
        [tblPetGromming reloadData];
    }
    else
    {
        arrGroomers = arrCompleteLoadRecords;
        [tblPetGromming reloadData];
    }
    
}
#pragma mark - textfield Delegate Methods


-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    
    if (txtSearchField.text.length>1) {
                pageValue=1;
        lastLoad = 0;
                [arrGroomers removeAllObjects];
                arrGroomers=[[NSMutableArray alloc]init];
                [self.view endEditing:YES];
                strKeyword=@"";
                txtSearchField.text=@"";
                [self getResultsForKey:@"" andForCategory:@"1" withPage:pageValue];
                
            }

    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{

     NSString *strTxtSearch = [txtSearchField.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    arrGroomers=[[NSMutableArray alloc]init];
    if (strTxtSearch.length>0)
    {
            pageValue=1;
        lastLoad=0;
        NSString *strTeXtSearch=[NSString stringWithFormat:@"%@ Searching...",strTxtSearch];
        [CommonFunctions showActivityIndicatorWithText:strTeXtSearch];
        [self getResultsForKey:strTxtSearch andForCategory:@"1" withPage:pageValue];
        //        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"self.first_name contains[cd] %@",strTxtSearch];
        //        arrGroomers = [[arrAllrecords filteredArrayUsingPredicate:resultPredicate] mutableCopy];
        //        [tblPetGromming reloadData];
        [textField resignFirstResponder];
    }
    else
    {
        
arrGroomers = arrCompleteLoadRecords;
        [tblPetGromming reloadData];
       // [textField resignFirstResponder];
    }
    return YES;
}


- (IBAction)btnSearchText:(id)sender {
    NSString *strTxtSearch = [txtSearchField.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    arrGroomers=[[NSMutableArray alloc]init];
    if (strTxtSearch.length>0)
    {
        NSString *strTeXtSearch=[NSString stringWithFormat:@"%@ Searching...",strTxtSearch];
  //      [CommonFunctions showActivityIndicatorWithText:strTeXtSearch];
//        [self getResultsForKey:strTxtSearch andForCategory:@"1" withPage:pageValue];
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"self.business_name contains[cd] %@",strTxtSearch];
        arrGroomers = [[arrGroomers filteredArrayUsingPredicate:resultPredicate] mutableCopy];
        [tblPetGromming reloadData];
    }
    else
    {
        
        arrGroomers = arrCompleteLoadRecords;
        [tblPetGromming reloadData];
    }

}

#pragma mark- WebService integration

-(void)getResultsForKey:(NSString*)strKey andForCategory:(NSString*)strCategory withPage:(int)page
{
    NSString *tokenID=[[NSUserDefaults standardUserDefaults]valueForKey:UD_TOKEN_ID];
    NSMutableDictionary *params=[[NSMutableDictionary alloc]initWithDictionary:@{@"lat":[NSString stringWithFormat:@"%f",CurrentLatitude],@"lon":[NSString stringWithFormat:@"%f",CurrentLongitude],@"user_id":tokenID,@"keyword":strKey,@"page":@(page).stringValue,@"type":strCategory,@"category":strBusinessCategory}];
        NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
//    [CommonFunctions alertTitle:@"" withMessage:[NSString stringWithFormat:@"%@",params]];
    [[ConnectionManager sharedInstance]startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:@"servicelisting" withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
    {

         [CommonFunctions removeActivityIndicator];
        [actiVItyLoader stopAnimating];
        actiVItyLoader.hidden=YES;
      //  txtSearchField.text=@"";
        [txtSearchField resignFirstResponder];
        NSDictionary* result = [NSJSONSerialization JSONObjectWithData:responseObject
                                                               options:kNilOptions
                                                                 error:nil];
        NSLog(@"%@",result);
        
        if ([[result valueForKey:@"replyCode"] isEqualToString:@"error"])
        {
            arrAllrecords=[[NSMutableArray alloc]init];

            if (arrAllrecords.count>1) {
            lblNoResultsFound.hidden=YES;
            }
            else
            {
                            lblNoResultsFound.hidden=NO;
            }

            
            tblPetGromming.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
           
            
            [arrGroomers addObjectsFromArray:arrAllrecords];
            [tblPetGromming reloadData];
        }
        else
        {
        tblPetGromming.tableFooterView=footerView;
        arrAllrecords=[[NSMutableArray alloc]init];
        arrAllrecords=[[result valueForKey:@"Services"] mutableCopy]; 
        NSLog(@"%d",arrAllrecords.count);
        
        
        int totalRecords=[[result valueForKey:@"total_records"] intValue];
            if (arrAllrecords.count==0)
            {
                 lblNoResultsFound.hidden=NO;
            }
            else
            {
                 lblNoResultsFound.hidden=YES;
            }
            
        [arrGroomers addObjectsFromArray:arrAllrecords];
        arrCompleteLoadRecords=[[NSMutableArray alloc]init];
        arrCompleteLoadRecords=arrGroomers;
        [tblPetGromming reloadData];
        
        if (totalRecords<21)
        {
            tblPetGromming.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
            return ;
        }
        
        if (lastLoad == 0)
        {
            int nextLoadCount = (page+1) * 20;
            
            NSString *strTesults=@"Results";
            
            float records=totalRecords%20;
            
            if (records>1)
            {
                strTesults=@"Results";
            }
            else
            {
                strTesults=@"Result";
            }
            
            if (totalRecords > nextLoadCount)
            {
                [btnFooter setTitle:[NSString stringWithFormat:@"Next %d %@",20,strTesults] forState:UIControlStateNormal];
            }
            else{
                lastLoad = 1;
                [btnFooter setTitle:[NSString stringWithFormat:@"Next %d %@",totalRecords%20,strTesults] forState:UIControlStateNormal];
            }
        }
        else{
             actiVItyLoader.hidden=YES;
            tblPetGromming.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        }
        }
    } withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [actiVItyLoader stopAnimating];
          actiVItyLoader.hidden=YES;
        [tblPetGromming reloadData];
        [CommonFunctions removeActivityIndicator];

    }];
}

- (IBAction)btnFooterPressed:(id)sender {
    
    [actiVItyLoader startAnimating];
    actiVItyLoader.hidden=NO;
    pageValue+=1;
    [self getResultsForKey:strKeyword andForCategory:strSearchCategoryType withPage:pageValue];
    
}


@end
