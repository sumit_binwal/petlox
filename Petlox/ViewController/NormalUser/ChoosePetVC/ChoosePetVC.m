//
//  ChoosePetVC.m
//  Petlox
//
//  Created by Sumit Sharma on 10/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "ChoosePetVC.h"
#import "SelectBreedVC.h"
@interface ChoosePetVC ()
{

    IBOutlet UIImageView *imgVw;
    NSString *petType;
}
@end

@implementation ChoosePetVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    [self selectDog];
 
        // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=NO;
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.nextBtn.layer.cornerRadius = self.nextBtn.frame.size.width/2;
}


-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    
    self.navigationItem.title = @"Add A Pet";
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction Button Method
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnClicked:(id)sender {
    UIButton *btn=(UIButton *)sender;
    
    if (btn.tag==0) {
           imgVw.frame = CGRectMake(10, 10, 146, 146);
        petType=@"dogs";
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: UIViewAnimationOptionTransitionCurlUp
                         animations:^{
                             imgVw.frame = CGRectMake(10, 135, 146, 146);
                         }
                         completion:^(BOOL finished){

                             [imgVw setImage:[UIImage imageNamed:@"selectDogImg"]];
                             petType=@"dogs";

                         }];
        btn.tag=1;

    }
    else if (btn.tag==1)
    {
petType=@"cats";
        imgVw.frame = CGRectMake(10, 135, 146, 146);
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: UIViewAnimationOptionTransitionCurlUp
                         animations:^{
                             imgVw.frame = CGRectMake(10, 10, 146, 146);
                         }
                         completion:^(BOOL finished){
                             [imgVw setImage:[UIImage imageNamed:@"selectCatImg"]];
petType=@"cats";
                         }];
        btn.tag=0;
        
    }
}

- (IBAction)nextBtnClicked:(id)sender {
    SelectBreedVC *sbvc=[[SelectBreedVC alloc]init];
    sbvc.strBreed=petType;
    [self.navigationController pushViewController:sbvc animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)dogSelected:(id)sender {
    [self selectDog];
}

- (IBAction)catSelected:(id)sender {
    [self selectCat];
}

-(void)selectDog{
    self.dogImageView.image = [UIImage imageNamed:@"dog_icon"];
    petType=@"dogs";
    [self deselectCat];
}

-(void)deselectDog{
    self.dogImageView.image = [UIImage imageNamed:@"dog_icon_unfoucs"];
}

-(void)selectCat{
    self.catImageView.image = [UIImage imageNamed:@"cat_icon"];
    petType=@"cats";
    [self deselectDog];
}

-(void)deselectCat{
    self.catImageView.image = [UIImage imageNamed:@"cat_icon_unfoucs"];
}

@end
