//
//  MyReservationDetailVC.h
//  Petlox
//
//  Created by Sumit Sharma on 14/12/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyReservationDetailVC : UIViewController
@property(nonatomic,strong)NSMutableDictionary *dictData;
@property(nonatomic,strong)NSString *strSelectedIndex;
@end
