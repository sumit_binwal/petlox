//
//  SelectBreedCustomeCell.h
//  Petlox
//
//  Created by Sumit Sharma on 11/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectBreedCustomeCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblBreedName;
@property (strong, nonatomic) IBOutlet UIImageView *imgVwSelected;

@end
