//
//  ShowPetVC.h
//  Petlox
//
//  Created by Sumit Sharma on 03/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//
#import "iCarousel.h"
#import <UIKit/UIKit.h>

@interface ShowPetVC : UIViewController <iCarouselDataSource,iCarouselDelegate, UICollectionViewDelegate, UICollectionViewDataSource>
{
    
}
@property (nonatomic, strong) IBOutlet iCarousel *carousel;
@property (nonatomic, strong) NSMutableDictionary *dictBusinessDetail;
@property(nonatomic,strong)NSMutableArray *arrPets;
@property (weak, nonatomic) IBOutlet UICollectionView *petsCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *btnContinue;
@end
