//
//  FavoriteVC.m
//  Petlox
//
//  Created by Sumit Sharma on 13/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "FavoriteVC.h"
#import "FavoriteCustomeCell.h"
#import "AllBusinessListingVC.h"
#import "HomeScreenSpecsVC.h"
#import "ServiceProviderVC.h"
#import "IIViewDeckController.h"
#import "PetGromingCell.h"

@interface FavoriteVC ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UIImageView *imgStar5;
    IBOutlet UILabel *lblErrorMsg;
    IBOutlet UILabel *lblMiles;
    IBOutlet UILabel *lblReview;
    IBOutlet UILabel *lblBusinessAddress;
    IBOutlet UILabel *lblMsgError;
    IBOutlet UILabel *lblBusinessName;

    IBOutlet UIImageView *imgStar4;
    IBOutlet UIImageView *imgStar3;
    IBOutlet UIImageView *imgStar2;
    IBOutlet UIImageView *imgStar1;

    IBOutlet UICollectionView *favCollectnView;
    
    NSMutableArray *arrFavoriteList;
    NSInteger selectIndexpath;
    NSMutableDictionary *dictBusinessData;
    IBOutlet UITableView *tblView;
}
@end

@implementation FavoriteVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    wbServiceCount=1;
      self.navigationController.navigationBarHidden = NO;
    // Do any additional setup after loading the view from its nib.
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [APPDELEGATE showPetReservationTabBar:NO];
    [self.navigationController.navigationBar setHidden:NO];
    self.navigationController.navigationBarHidden=NO;
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getFavoriteList];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }
}
-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Favorite Business"];
    [favCollectnView registerNib:[UINib nibWithNibName:@"FavoriteCustomeCell" bundle:nil] forCellWithReuseIdentifier:@"FavoriteCustomeCell"];
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"menu-icon"] style:UIBarButtonItemStylePlain target:self.viewDeckController action:@selector(toggleLeftView)];
    [self.navigationItem setLeftBarButtonItem:btn];
    
    UIBarButtonItem *backButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"new_add_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(plushBtnClicked:)];
    [self.navigationItem setRightBarButtonItem:backButton];

}

- (IBAction)plushBtnClicked:(id)sender {
    AllBusinessListingVC *ablvc=[[AllBusinessListingVC alloc]init];
    [self.navigationController pushViewController:ablvc animated:YES];
}

-(IBAction)backBarButtonClicked:(id)sender
{
    IIViewDeckController *ivdck=[[IIViewDeckController alloc]init];
    [ivdck toggleLeftView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UICollection View Delegate Methods
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrFavoriteList.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //DLog(@"SETTING SIZE FOR ITEM AT INDEX %d", indexPath.row);
    /*
    if ([UIScreen mainScreen].bounds.size.width==414) {
        CGSize mElementSize = CGSizeMake(120,120);
        return mElementSize;
        
    }
    else if ([UIScreen mainScreen].bounds.size.width==375)
    {
        CGSize mElementSize = CGSizeMake(110,110);
        return mElementSize;
        
    }
    else
    {
        CGSize mElementSize = CGSizeMake(80,120);
        return mElementSize;
        
    }
    */
//    CGSize mElementSize = CGSizeMake(100,140);
    CGFloat width = [APPDELEGATE window].frame.size.width * 100 / 320;
    CGFloat height = [APPDELEGATE window].frame.size.height * 140 / 568;
    CGSize mElementSize = CGSizeMake(width,height);
    return mElementSize;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FavoriteCustomeCell *cell=(FavoriteCustomeCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"FavoriteCustomeCell" forIndexPath:indexPath];
    
    if (arrFavoriteList.count>0)
    {
        lblMsgError.text=@"";
        cell.imgBusinessProfile.clipsToBounds=YES;
        
        NSString *imgURL=[[arrFavoriteList objectAtIndex:indexPath.row] objectForKey:@"image"];
        
        if (imgURL.length>1) {
            [cell.imgBusinessProfile setImageWithURL:[NSURL URLWithString:imgURL] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        }
        else
        {
            [cell.imgBusinessProfile setImage:[UIImage imageNamed:@"DefaultProfileImg"]];
        }
        
        cell.lblBusinessName.text=[[arrFavoriteList objectAtIndex:indexPath.row] objectForKey:@"business_name"];
    
        if ([self isNotNull:[[arrFavoriteList objectAtIndex:indexPath.row]valueForKey:@"review_count"]])
        {
            int reviewCount=[[[arrFavoriteList objectAtIndex:indexPath.row]valueForKey:@"review_count"] intValue];
            if (reviewCount>1) {
                cell.lblReviews.text=[NSString stringWithFormat:@"%@ reviews",[[arrFavoriteList objectAtIndex:indexPath.row]valueForKey:@"review_count"]];
            }
            else
            {
                cell.lblReviews.text=[NSString stringWithFormat:@"%@ review",[[arrFavoriteList objectAtIndex:indexPath.row]valueForKey:@"review_count"]];
            }
        }
        else
        {
            cell.lblReviews.text=[NSString stringWithFormat:@"0 review"];
        }
        //Favorite Rating Code
        if ([self isNotNull:[[arrFavoriteList objectAtIndex:indexPath.row] objectForKey:@"avg_rate"]]) {
            int avgRating=[[[arrFavoriteList objectAtIndex:indexPath.row] objectForKey:@"avg_rate"]intValue];
            switch (avgRating) {
                case 0:
                {
                    [cell.imgStar1 setImage:[UIImage imageNamed:@"new_favRatingStarInactive"]];
                    [cell.imgStar2 setImage:[UIImage imageNamed:@"new_favRatingStarInactive"]];
                    [cell.imgStar3 setImage:[UIImage imageNamed:@"new_favRatingStarInactive"]];
                    [cell.imgStar4 setImage:[UIImage imageNamed:@"new_favRatingStarInactive"]];
                    [cell.imgStar5 setImage:[UIImage imageNamed:@"new_favRatingStarInactive"]];
                    break;
                }
                case 1:
                {
                    [cell.imgStar1 setImage:[UIImage imageNamed:@"rating_foucs"]];
                    [cell.imgStar2 setImage:[UIImage imageNamed:@"new_favRatingStarInactive"]];
                    [cell.imgStar3 setImage:[UIImage imageNamed:@"new_favRatingStarInactive"]];
                    [cell.imgStar4 setImage:[UIImage imageNamed:@"new_favRatingStarInactive"]];
                    [cell.imgStar5 setImage:[UIImage imageNamed:@"new_favRatingStarInactive"]];
                    break;
                }
                case 2:
                {
                    [cell.imgStar1 setImage:[UIImage imageNamed:@"rating_foucs"]];
                    [cell.imgStar2 setImage:[UIImage imageNamed:@"rating_foucs"]];
                    [cell.imgStar3 setImage:[UIImage imageNamed:@"new_favRatingStarInactive"]];
                    [cell.imgStar4 setImage:[UIImage imageNamed:@"new_favRatingStarInactive"]];
                    [cell.imgStar5 setImage:[UIImage imageNamed:@"new_favRatingStarInactive"]];
                    break;
                }
                case 3:
                {
                    [cell.imgStar1 setImage:[UIImage imageNamed:@"rating_foucs"]];
                    [cell.imgStar2 setImage:[UIImage imageNamed:@"rating_foucs"]];
                    [cell.imgStar3 setImage:[UIImage imageNamed:@"rating_foucs"]];
                    [cell.imgStar4 setImage:[UIImage imageNamed:@"new_favRatingStarInactive"]];
                    [cell.imgStar5 setImage:[UIImage imageNamed:@"new_favRatingStarInactive"]];
                    
                    break;
                }
                case 4:
                {
                    [cell.imgStar1 setImage:[UIImage imageNamed:@"rating_foucs"]];
                    [cell.imgStar2 setImage:[UIImage imageNamed:@"rating_foucs"]];
                    [cell.imgStar3 setImage:[UIImage imageNamed:@"rating_foucs"]];
                    [cell.imgStar4 setImage:[UIImage imageNamed:@"rating_foucs"]];
                    [cell.imgStar5 setImage:[UIImage imageNamed:@"new_favRatingStarInactive"]];
                    break;
                }
                case 5:
                {
                    [cell.imgStar1 setImage:[UIImage imageNamed:@"rating_foucs"]];
                    [cell.imgStar2 setImage:[UIImage imageNamed:@"rating_foucs"]];
                    [cell.imgStar3 setImage:[UIImage imageNamed:@"rating_foucs"]];
                    [cell.imgStar4 setImage:[UIImage imageNamed:@"rating_foucs"]];
                    [cell.imgStar5 setImage:[UIImage imageNamed:@"rating_foucs"]];
                    break;
                }
                default:
                break;
            }
        }
        else
        {
            [cell.imgStar1 setImage:[UIImage imageNamed:@"new_favRatingStarInactive"]];
            [cell.imgStar2 setImage:[UIImage imageNamed:@"new_favRatingStarInactive"]];
            [cell.imgStar3 setImage:[UIImage imageNamed:@"new_favRatingStarInactive"]];
            [cell.imgStar4 setImage:[UIImage imageNamed:@"new_favRatingStarInactive"]];
            [cell.imgStar5 setImage:[UIImage imageNamed:@"new_favRatingStarInactive"]];
        }
        NSLog(@"%ld",(long)selectIndexpath);
        
        if (selectIndexpath == indexPath.row)
        {
            [cell.imgTickMark setImage:[UIImage imageNamed:@"cardCheck"]];
            
        }
        else
        {
            [cell.imgTickMark setImage:[UIImage imageNamed:@""]];
        }

    }
    else
    {
        lblMsgError.text=@"No Favorite Found.";
    }

    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    dictBusinessData = [arrFavoriteList objectAtIndex:indexPath.row];
    
    selectIndexpath = indexPath.row;
    [collectionView reloadData];
    
    [tblView reloadData];
}

#pragma mark- WebServices API..

-(void)getFavoriteList
{
    NSMutableDictionary *param;
        NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"user_id",[NSString stringWithFormat:@"%f",CurrentLatitude],@"lat",[NSString stringWithFormat:@"%f",CurrentLongitude],@"lon", nil];
    
    NSString *url = [NSString stringWithFormat:@"userBusinessFavList"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/userBusinessFavList
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            arrFavoriteList=[responseDict objectForKey:@"data"];
            dictBusinessData=[arrFavoriteList objectAtIndex:0];
            selectIndexpath = 0;
            [favCollectnView reloadData];
            [tblView setHidden:NO];
            [tblView reloadData];
        }
        else if([operation.response statusCode]==206)
        {
            imgStar1.image=nil;
            imgStar2.image=nil;
            imgStar3.image=nil;
            imgStar4.image=nil;
            imgStar5.image=nil;
            lblMsgError.text=@"No Favorite Found.";
            lblBusinessAddress.text=@"";
            lblBusinessName.text=@"";
            lblMiles.text=@"";
            lblReview.text=@"";
            [dictBusinessData removeAllObjects];
            [arrFavoriteList removeAllObjects];
            [favCollectnView reloadData];
            [tblView reloadData];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            NSLog(@"%ld",(long)[operation.response statusCode]);
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self getFavoriteList];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}


#pragma mark - IBAction Button Clicked

- (IBAction)favDetailButtonClicked:(id)sender {
    if (dictBusinessData.count>0) {
        ServiceProviderVC *service=[[ServiceProviderVC alloc]initWithNibName:@"ServiceProviderVC" bundle:nil];
        
        service.dictUserDdetails = dictBusinessData;
        APPDELEGATE.K_dictUserDdetails = dictBusinessData;
        [self.navigationController pushViewController:service animated:YES];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDelegate / DataSource Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float rowHeight = 0.0f;
    rowHeight = 125;
    return rowHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *settingsCell   = @"PetGromingCell";
    
    PetGromingCell *cell = (PetGromingCell *)[tableView dequeueReusableCellWithIdentifier:settingsCell];
    
    if (cell == nil)
    {
        cell = (PetGromingCell*)[[[NSBundle mainBundle] loadNibNamed:@"PetGromingCell" owner:self options:nil] objectAtIndex:0];
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    if (dictBusinessData.count>0) {
        cell.lblNameGrommer.text = [dictBusinessData objectForKey:@"first_name"];
        //    cell.lblDistance.text = [dictBusinessData objectForKey:@"distance"];
        
        if ([dictBusinessData objectForKey:@"distance"] !=[NSNull null])
        {
            NSString *strDistance=[dictBusinessData objectForKey:@"distance"];
            if (strDistance.length>0)
            {
                NSString *strMile=[NSString stringWithFormat:@"%@ mi",strDistance];
                strMile=[strMile stringByReplacingOccurrencesOfString:@"mi" withString:@""];
                cell.lblDistance.text=[NSString stringWithFormat:@"%@",strDistance];
            }
        }
        
        if ([self isNotNull:[dictBusinessData objectForKey:@"review_count"]]) {
            cell.lblNoOfreview.text = ([[dictBusinessData objectForKey:@"review_count"]integerValue] > 1) ? [NSString stringWithFormat:@"%@ reviews", [dictBusinessData objectForKey:@"review_count"]]: [NSString stringWithFormat:@"%@ review", [dictBusinessData objectForKey:@"review_count"]];
            
        }
        
        
        cell.lblAddress.text =  [dictBusinessData objectForKey:@"address"];
        
        
        
        if ([dictBusinessData objectForKey:@"review_count"]!=[NSNull null])
        {
            int reviewCount=[[dictBusinessData objectForKey:@"review_count"] intValue];
            if (reviewCount>1) {
                cell.lblNoOfreview.text=[NSString stringWithFormat:@"%@ reviews",[dictBusinessData objectForKey:@"review_count"]];
            }
            else
            {
                cell.lblNoOfreview.text=[NSString stringWithFormat:@"%@ review",[dictBusinessData objectForKey:@"review_count"]];
            }
        }
        
        if ([self isNotNull:[dictBusinessData objectForKey:@"fav"]]) {
            NSString *strFav = [NSString stringWithFormat:@"%@",[dictBusinessData objectForKey:@"fav"]];
            if ([strFav isEqualToString:@"1"]) {
                [cell.imgFavourate setImage:[UIImage imageNamed:@"favorite_foucs_icon"]];
            }
            else
            {
                [cell.imgFavourate setImage:[UIImage imageNamed:@"favorite_icon"]];
            }
        }
        else
        {
            //  [cell.imgFavourate setImage:[UIImage imageNamed:@"hart"]];
        }
        
        
        NSString *imgURL=[dictBusinessData objectForKey:@"image"];
        if (imgURL.length>1) {
            
            [cell.imgProfileView setImageWithURL:[NSURL URLWithString:[dictBusinessData objectForKey:@"image"]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            
        }
        else
        {
            [cell.imgProfileView setImage:[UIImage imageNamed:@"DefaultProfileImg"]];
        }
        
        
        if ([dictBusinessData objectForKey:@"distance"] !=[NSNull null])
        {
            NSString *strDistance=[dictBusinessData objectForKey:@"distance"];
            if (strDistance.length>0)
            {
                NSString *strMile=[NSString stringWithFormat:@"%@ mi",strDistance];
                strMile=[strMile stringByReplacingOccurrencesOfString:@"mi" withString:@""];
                cell.lblDistance.text=[NSString stringWithFormat:@"%@",strDistance];
            }
            
        }
        
        
        if ([dictBusinessData objectForKey:@"avg_rate"] !=[NSNull null])
        {
            int stars=[[dictBusinessData objectForKey:@"avg_rate"] intValue];
            
            for (int i=0; i<stars; i++)
            {
                [[cell.imsStars objectAtIndex:i] setHighlighted:YES];
            }
            
        }
        cell.imgProfileView.layer.masksToBounds=YES;
    }
    else
    {
        [tableView setHidden:YES];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self favDetailButtonClicked:nil];
}


@end
