//
//  CancelReservationVC.h
//  Petlox
//
//  Created by Sumit Sharma on 02/11/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CancelReservationVC : UIViewController
@property(nonatomic,strong)NSString *strReservationID;
@end
