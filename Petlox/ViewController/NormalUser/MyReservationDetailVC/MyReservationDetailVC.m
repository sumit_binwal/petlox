//
//  MyReservationDetailVC.m
//  Petlox
//
//  Created by Sumit Sharma on 14/12/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "MyReservationDetailVC.h"
#import "CalendarVC.h"
#import "CancelReasonVC.h"
#import "MyReservationCustomeCell.h"
#import "MyReservationSecondCell.h"
#import "MyReservationLastCell.h"

@interface MyReservationDetailVC ()<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UIImageView *imgVwBusinessImg;
    IBOutlet UIImageView *imgVwPetImg;
    IBOutlet UILabel *lblBusinessName;
    IBOutlet UIImageView *imgIcon1;
    IBOutlet UILabel *lblDIrection;
    IBOutlet UILabel *lblStaticTimeRemaining;
    IBOutlet UILabel *lblBusinessDiscription;
    IBOutlet UILabel *lblDistance;
    IBOutlet UIImageView *imgIcon2;
    IBOutlet UILabel *lblPetName;
    IBOutlet UILabel *lblResevationDate;
    IBOutlet UILabel *lblTimeRemainin;
    
    IBOutlet UITableView *tblViewReservationDetail;
    
}
@end

@implementation MyReservationDetailVC
@synthesize dictData,strSelectedIndex;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    // Do any additional setup after loading the view from its nib.
}

-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    
self.navigationItem.hidesBackButton = YES;

    UIBarButtonItem *backButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    backButton.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:backButton];


    
    
    [self.navigationItem setTitle:@"Reservations Details"];
    
/*
    imgVwBusinessImg.layer.borderColor=[UIColor whiteColor].CGColor;
    imgVwBusinessImg.layer.borderWidth=1;
    imgVwBusinessImg.layer.cornerRadius=imgVwBusinessImg.frame.size.width/2;
    imgVwBusinessImg.clipsToBounds=YES;
    
    if ([strSelectedIndex isEqualToString:@"2"]) {
        [lblDistance setHidden:YES];
        [lblTimeRemainin setHidden:YES];
        [lblDIrection setHidden:YES];
        [lblStaticTimeRemaining setHidden:YES];
        [imgIcon1 setHidden:YES];
        [imgIcon2 setHidden:YES];
    }
    else
    {
    lblTimeRemainin.text=[dictData valueForKey:@"interval"];
    lblDistance.text=[dictData valueForKey:@"distance"];
    }
    imgVwPetImg.layer.borderColor=[UIColor whiteColor].CGColor;
    imgVwPetImg.layer.borderWidth=1;
    imgVwPetImg.layer.cornerRadius=5.0f;
    imgVwPetImg.clipsToBounds=YES;
    lblBusinessName.text=[dictData valueForKey:@"business_name"];
    lblBusinessDiscription.text=[dictData valueForKey:@"address"];
    lblPetName.text=[dictData valueForKey:@"pet_name"];
    lblResevationDate.text=[dictData valueForKey:@"ReservationDate"];
    NSString *businessImg=[dictData valueForKey:@"image"];
    if (businessImg.length>0) {
        [imgVwBusinessImg setImageWithURL:[NSURL URLWithString:[dictData valueForKey:@"image"]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    else
    {
        [imgVwBusinessImg setImage:[UIImage imageNamed:@"DefaultProfileImg"]];
    }
    
    NSString *petImg=[dictData valueForKey:@"pet_image"];
    if (petImg.length>0) {
        [imgVwPetImg setImageWithURL:[NSURL URLWithString:[dictData valueForKey:@"pet_image"]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    else
    {
        [imgVwBusinessImg setImage:[UIImage imageNamed:@"defaultPetImage"]];
    }
 */
}
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - IBAction Button Methods
- (IBAction)addToCalenderBtnClicked:(id)sender {
    CalendarVC *cvc = [[CalendarVC alloc]init];
    cvc.strbusiessName = [dictData valueForKey:@"business_name"];
    cvc.strAppointedDate = [dictData valueForKey:@"ReservationDate"];
    [self.navigationController pushViewController:cvc animated:YES];
}
- (IBAction)bckToReservationBtnClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}
- (IBAction)cancelReservationBtnClicked:(id)sender {
    CancelReasonVC *crvc=[[CancelReasonVC alloc]init];
    crvc.reservationID=[dictData valueForKey:@"id"];
    [self.navigationController pushViewController:crvc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDelegate / DataSource Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float rowHeight = 0.0f;
    if (indexPath.row == 0)
    {
        rowHeight = 142.0f;
    }
    else if (indexPath.row == 1)
    {
        rowHeight = 115.0f;
    }
    else if (indexPath.row == 2)
    {
        rowHeight = 44.0f;
    }
    else if (indexPath.row == 3)
    {
        rowHeight = 44.0f;
    }
    return rowHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 4;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        static NSString *cellIdentifier=@"MyReservationCustomeCell";
        MyReservationCustomeCell *cell=[[MyReservationCustomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        if (cell==nil) {
            cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.imgBusinessProfileImg.clipsToBounds=YES;
        cell.imgPetProfileImg.clipsToBounds=YES;
        
        [cell.lblTimeLeft setHidden:YES];
        [cell.lblDistance setHidden:YES];
        [cell.lblDirection setHidden:YES];
        [cell.lblTimeReaming setHidden:YES];
        [cell.lblIcon1 setHidden:YES];
        [cell.lblIcon2 setHidden:YES];
        [cell.btnDirection setHidden:YES];
        cell.btnDirection.frame=CGRectMake(cell.btnDirection.frame.origin.x, cell.btnDirection.frame.origin.y, 0, 0);
        cell.directionwidthCnstraint.constant=0;
        cell.imgViewSep.hidden = YES;
        
        
        cell.lblBusinessName.text=[dictData valueForKey:@"business_name"];
        cell.lblBusinessAddress.text=[dictData valueForKey:@"address"];
        cell.lblPetname.text=[dictData valueForKey:@"pet_name"];
        cell.lblReserveDate.text=[dictData valueForKey:@"ReservationDate"];
        
        NSString *strBusinessImg=[dictData valueForKey:@"image"];
        if (strBusinessImg.length>0) {
            
            [cell.imgBusinessProfileImg setImageWithURL:[NSURL URLWithString:[dictData valueForKey:@"image"]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        }
        else
        {
            [cell.imgBusinessProfileImg setImage:[UIImage imageNamed:@"DefaultProfileImg"]];
        }
        
        
        NSString *strPetImg=[dictData valueForKey:@"pet_image"];
        if (strPetImg.length>0) {
            [cell.imgPetProfileImg setImageWithURL:[NSURL URLWithString:[dictData valueForKey:@"pet_image"]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        }
        else
        {
            [cell.imgPetProfileImg setImage:[UIImage imageNamed:@"defaultPetImage"]];
        }
        
        return cell;
        
    }
    else if (indexPath.row == 1)
    {
        static NSString *cellIdentifier=@"MyReservationSecondCell";
        MyReservationSecondCell *cell=[[MyReservationSecondCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        if (cell==nil) {
            cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.lblTimeRemainingTitle.text=[dictData valueForKey:@"time_flag"];
        cell.lblTimeRemainingValue.text = [dictData valueForKey:@"interval"];
        cell.lblDistanceValue.text = [dictData valueForKey:@"distance"];
        
        return cell;
    }
    else
    {
        static NSString *cellIdentifier=@"MyReservationLastCell";
        MyReservationLastCell *cell=[[MyReservationLastCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        if (cell==nil) {
            cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row == 2)
        {
           cell.lblTitle.text = @"Add to Calendar";
        }
        else
        {
            cell.lblTitle.text = @"Cancel reservation";
            cell.imgViewArrow.image = [UIImage imageNamed:@"redArrow"];
            cell.lblTitle.textColor = [UIColor colorWithRed:222/255.0 green:0/255.0 blue:0/255.0 alpha:1.0];
        }
        
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 2)
    {
        [self addToCalenderBtnClicked:nil];

    }
    else if (indexPath.row == 3)
    {
        [self cancelReservationBtnClicked:nil];
    }
}

@end
