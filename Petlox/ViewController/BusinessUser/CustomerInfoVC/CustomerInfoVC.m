//
//  CustomerInfoVC.m
//  Petlox
//
//  Created by Sumit Sharma on 15/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "CustomerInfoVC.h"
#import "DeclineVC.h"
#import "ReservationAcceptedVC.h"
#import "JobProgressVC.h"
#import "CancelReservationVC.h"
@interface CustomerInfoVC ()
{
    
    IBOutlet UIView *vwBottomVw;
    IBOutlet UIButton *btnAccepted;
    IBOutlet UIButton *btnDecline;
    IBOutlet UILabel *lblTxtStatus;
    IBOutlet UILabel *lblTxtBreed;
    IBOutlet UILabel *lblTxtName;
    IBOutlet UILabel *lblTxtWeight;
    IBOutlet UILabel *lblTxtAge;
    IBOutlet UILabel *lblTextPetInfo;
    IBOutlet UIButton *btnMore;
    IBOutlet NSLayoutConstraint *btnwidthCnstraint;
    IBOutlet UIButton *btnStartJob;
    IBOutlet UIButton *btnCancleReservation;

    IBOutlet UILabel *lblTextServiceRequested;
    IBOutlet NSLayoutConstraint *btnWdthConstraint1;
    IBOutlet UIScrollView *scrllVw;
    IBOutlet UIImageView *imgPetImage;
    IBOutlet UIImageView *imgUserImage;
    IBOutlet UILabel *lblDIscription;
    IBOutlet UILabel *lblPetWeight;
    IBOutlet UILabel *lblPetAge;
    IBOutlet UILabel *lblPetBreed;
    IBOutlet UILabel *lblStatus;
//    IBOutlet UIButton *btnCancleReservation;
    IBOutlet UILabel *lblReservationTime;
    IBOutlet UILabel *lblPetName;
    IBOutlet UIButton *acceptBtn;
    IBOutlet UILabel *lblUserName;
    IBOutlet UIView *vwDiscription;
    IBOutlet UIButton *declineBtn;
    IBOutlet UIView *vwForNewReservation;
    IBOutlet UIView *vwForAcceptedReservation;
    IBOutlet UILabel *lblServiceRequested;
}
@end

@implementation CustomerInfoVC
@synthesize dictCustomerInfo,strFromTap;
- (void)viewDidLoad {
    [super viewDidLoad];
    wbServiceCount=1;
    [self setUpView];
    NSLog(@"%f",[UIScreen mainScreen].bounds.size.height);
    if ([UIScreen mainScreen].bounds.size.width>375) {
        [btnMore setHidden:YES];
        btnwidthCnstraint.constant=30.0f;
        btnWdthConstraint1.constant=30.0f;
        lblDIscription.numberOfLines=0;
        //        [vwDiscription setHidden:NO];
    }
    else if ([UIScreen mainScreen].bounds.size.width==375) {
        int numberofLine=(int)[self lineCountForText:[dictCustomerInfo objectForKey:@"petdiscription"]];
        NSLog(@"%d",numberofLine);
        if (numberofLine>3) {
            [btnMore setHidden:NO];
        }
        else
        {
            [btnMore setHidden:YES];
        }
    }
    else if ([UIScreen mainScreen].bounds.size.height==568) {
        [vwDiscription setHidden:NO];
        [scrllVw setScrollEnabled:NO];
        
    }
    else if ([UIScreen mainScreen].bounds.size.height<568)
    {
        [vwDiscription setHidden:NO];
        [btnMore setHidden:YES];
        lblDIscription.numberOfLines=0;
        [scrllVw setScrollEnabled:YES];
        [scrllVw setContentSize:CGSizeMake(320.0f, self.view.frame.size.height+vwDiscription.frame.size.height+40)];
    }
    // Do any additional setup after loading the view from its nib.
}

-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
    
    imgUserImage.clipsToBounds=YES;
    
    [imgUserImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dictCustomerInfo objectForKey:@"image"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    
    imgPetImage.clipsToBounds=YES;
    
    [imgPetImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dictCustomerInfo objectForKey:@"petimage"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    lblPetAge.text=[NSString stringWithFormat:@"%@ Years",[dictCustomerInfo objectForKey:@"age"]];
    lblPetBreed.text=[NSString stringWithFormat:@"%@, %@",[dictCustomerInfo objectForKey:@"pet"],[dictCustomerInfo objectForKey:@"breed"]];
    lblPetName.text=[dictCustomerInfo objectForKey:@"pet_name"];
    lblServiceRequested.text=[dictCustomerInfo objectForKey:@"service"];
    lblPetWeight.text=[NSString stringWithFormat:@"%@ lbs",[dictCustomerInfo objectForKey:@"weight"]];

    if ([UIScreen mainScreen].bounds.size.height<=568) {
        [btnMore setHidden:NO];
        lblDIscription.text=[dictCustomerInfo objectForKey:@"petdiscription"];
    }
    else
    {
            lblDIscription.text=[dictCustomerInfo objectForKey:@"petdiscription"];
    }
    
    lblReservationTime.text=[dictCustomerInfo objectForKey:@"reservation_date"];
    lblUserName.text=[NSString stringWithFormat:@"%@ %@",[dictCustomerInfo objectForKey:@"first_name"],[dictCustomerInfo objectForKey:@"last_name"]];
    
    if ([strFromTap isEqualToString:@"0"])
    {
        [vwForAcceptedReservation setHidden:YES];
        [vwForNewReservation setHidden:NO];
        [self.navigationItem setTitle:@"Customer Info"];
    }
    else if ([strFromTap isEqualToString:@"1"])
    {
        [self.navigationItem setTitle:@"Job Info"];
        [vwForAcceptedReservation setHidden:NO];
        [vwForNewReservation setHidden:YES];
        
    }
    else
    {
       [self.navigationItem setTitle:@"Job Complete"];
        [btnMore setHidden:YES];
        [vwForAcceptedReservation setHidden:YES];
        [vwForNewReservation setHidden:YES];
    }
 
    [lblTxtAge setFont:[UIFont fontWithName:lblTxtAge.font.fontName size:lblTxtAge.font.pointSize*SCREEN_XScale]];
    [lblPetAge setFont:[UIFont fontWithName:lblPetAge.font.fontName size:lblPetAge.font.pointSize*SCREEN_XScale]];
    [lblStatus setFont:[UIFont fontWithName:lblStatus.font.fontName size:lblStatus.font.pointSize*SCREEN_XScale]];
    [lblPetName setFont:[UIFont fontWithName:lblPetName.font.fontName size:lblPetName.font.pointSize*SCREEN_XScale]];
    [lblTxtName setFont:[UIFont fontWithName:lblTxtName.font.fontName size:lblTxtName.font.pointSize*SCREEN_XScale]];

    [lblPetBreed setFont:[UIFont fontWithName:lblPetBreed.font.fontName size:lblPetBreed.font.pointSize*SCREEN_XScale]];
[lblTxtBreed setFont:[UIFont fontWithName:lblTxtBreed.font.fontName size:lblTxtBreed.font.pointSize*SCREEN_XScale]];
    [lblUserName setFont:[UIFont fontWithName:lblUserName.font.fontName size:lblUserName.font.pointSize*SCREEN_XScale]];
    [lblPetWeight setFont:[UIFont fontWithName:lblPetWeight.font.fontName size:lblPetWeight.font.pointSize*SCREEN_XScale]];
    [lblTxtStatus setFont:[UIFont fontWithName:lblTxtStatus.font.fontName size:lblTxtStatus.font.pointSize*SCREEN_XScale]];
    [lblTxtWeight setFont:[UIFont fontWithName:lblTxtWeight.font.fontName size:lblTxtWeight.font.pointSize*SCREEN_XScale]];
[lblTextPetInfo setFont:[UIFont fontWithName:lblTextPetInfo.font.fontName size:lblTextPetInfo.font.pointSize*SCREEN_XScale]];
    [lblReservationTime setFont:[UIFont fontWithName:lblReservationTime.font.fontName size:lblReservationTime.font.pointSize*SCREEN_XScale]];
    [lblServiceRequested setFont:[UIFont fontWithName:lblServiceRequested.font.fontName size:lblServiceRequested.font.pointSize*SCREEN_XScale]];
    [lblTextServiceRequested setFont:[UIFont fontWithName:lblTextServiceRequested.font.fontName size:lblTextServiceRequested.font.pointSize*SCREEN_XScale]];
    
    btnAccepted.layer.cornerRadius=btnAccepted.bounds.size.width/2*SCREEN_XScale;
    [btnAccepted.titleLabel setFont:[UIFont fontWithName:btnAccepted.titleLabel.font.fontName size:btnAccepted.titleLabel.font.pointSize*SCREEN_XScale]];
    btnDecline.layer.cornerRadius=btnDecline.bounds.size.width/2*SCREEN_XScale;
    [btnDecline.titleLabel setFont:[UIFont fontWithName:btnDecline.titleLabel.font.fontName size:btnDecline.titleLabel.font.pointSize*SCREEN_XScale]];
    
    btnCancleReservation.layer.cornerRadius=btnCancleReservation.bounds.size.width/2*SCREEN_XScale;
    [btnCancleReservation.titleLabel setFont:[UIFont fontWithName:btnCancleReservation.titleLabel.font.fontName size:btnCancleReservation.titleLabel.font.pointSize*SCREEN_XScale]];
    
    btnStartJob.layer.cornerRadius=btnStartJob.bounds.size.width/2*SCREEN_XScale;
    [btnStartJob.titleLabel setFont:[UIFont fontWithName:btnStartJob.titleLabel.font.fontName size:btnStartJob.titleLabel.font.pointSize*SCREEN_XScale]];
    
    btnStartJob.titleLabel.textAlignment=NSTextAlignmentCenter;
    btnStartJob.titleLabel.numberOfLines=0;
    
    btnCancleReservation.titleLabel.textAlignment=NSTextAlignmentCenter;
    btnCancleReservation.titleLabel.numberOfLines=0;
}
-(void)viewDidLayoutSubviews
{
    if ([UIScreen mainScreen].bounds.size.height<568) {
        [scrllVw setContentSize:CGSizeMake(320.0f, self.view.frame.size.height+lblDIscription.frame.size.height+160)];
        [scrllVw setScrollEnabled:YES];
    }

    else if ([UIScreen mainScreen].bounds.size.height==568 && [strFromTap isEqualToString:@"2"])  {
        lblDIscription.numberOfLines=0;
        [scrllVw setContentSize:CGSizeMake(320.0f, self.view.frame.size.height+lblDIscription.frame.size.height)];
        [scrllVw setScrollEnabled:YES];
    }
    else if ([UIScreen mainScreen].bounds.size.width>=375 && [strFromTap isEqualToString:@"2"])  {
        lblDIscription.numberOfLines=0;
        [btnMore setHidden:YES];
        [scrllVw setScrollEnabled:NO];
    }
//    else if ([UIScreen mainScreen].bounds.size.height==568) {
//        NSLog(@"%f",vwDiscription.frame.size.height+vwDiscription.frame.origin.y);
//        [scrllVw setContentSize:CGSizeMake(320.0f, 667.0f)];
//
//        if (vwDiscription.isHidden==YES) {
//        [scrllVw setScrollEnabled:NO];
//            NSLog(@"fsdaf");
//        }
//        else
//        {
//        [scrllVw setScrollEnabled:YES];
//        }
//    }
    else if ([UIScreen mainScreen].bounds.size.width==375)
    {
        [scrllVw setContentSize:CGSizeMake(320.0f,667.0f)];
        if (lblDIscription.numberOfLines==3) {
             [scrllVw setScrollEnabled:NO];
        }
        else
        {
             [scrllVw setScrollEnabled:YES];
        }
       
    }
}
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction Methods

- (IBAction)btnAcceptClicked:(id)sender {
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self acceptReservation];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }
}
- (IBAction)btnDeclineClicked:(id)sender {
    DeclineVC *dvc=[[DeclineVC alloc]init];
    dvc.fromCancel=@"";
    dvc.reservationID=[dictCustomerInfo objectForKey:@"id"];
    [self.navigationController pushViewController:dvc animated:YES];
}

- (IBAction)moreBtnClicked:(id)sender {
    
    NSLog(@"%d",[self lineCountForText:[dictCustomerInfo objectForKey:@"petdiscription"]]);
    
    UIButton *btn=(UIButton *)sender;
    if ([UIScreen mainScreen].bounds.size.width==375) {
        if (btn.tag==0) {
            [btnMore setImage:[UIImage imageNamed:@"btnLessImg"] forState:UIControlStateNormal];
            [scrllVw setScrollEnabled:YES];
            [UIView animateWithDuration:0.2 animations:^ {
                lblDIscription.numberOfLines=0;
                [scrllVw setScrollEnabled:YES];
                [scrllVw setContentSize:CGSizeMake(320.0f, ((self.view.frame.size.height+lblDIscription.frame.size.height)-90))];
                [scrllVw setContentOffset:CGPointMake(0.0f,lblDIscription.frame.size.height)];
            }];
            btn.tag=1;
        }
        else
        {
            [btnMore setImage:[UIImage imageNamed:@"btnMoreImg"] forState:UIControlStateNormal];
            [UIView animateWithDuration:0.2 animations:^ {
                [scrllVw setScrollEnabled:NO];
                lblDIscription.numberOfLines=3;
                [scrllVw setContentOffset:CGPointZero];
            }];
            btn.tag=0;
        }
    }
    else if ([UIScreen mainScreen].bounds.size.height==568)
    {
        if (btn.tag==0) {
            [vwDiscription setHidden:NO];
            [btnMore setImage:[UIImage imageNamed:@"btnLessImg"] forState:UIControlStateNormal];
            [scrllVw setScrollEnabled:YES];
            [UIView animateWithDuration:0.2 animations:^ {
                lblDIscription.numberOfLines=0;
                [scrllVw setContentSize:CGSizeMake(320.0f, ((self.view.frame.size.height+vwDiscription.frame.size.height)+110))];
                [scrllVw setContentOffset:CGPointMake(0.0f,vwDiscription.frame.size.height+110)];
            }];
            btn.tag=1;
        }
        else
        {
            [btnMore setImage:[UIImage imageNamed:@"btnMoreImg"] forState:UIControlStateNormal];
            [UIView animateWithDuration:0.2 animations:^ {
                [scrllVw setScrollEnabled:NO];
                [scrllVw setContentOffset:CGPointZero];
                [vwDiscription setHidden:YES];
            }];
            btn.tag=0;
        }
    }
    
    
}
- (int)lineCountForText:(NSString *) text
{
    UIFont *font = [UIFont fontWithName:@"Arial" size:14.0f];
    
    CGRect rect = [text boundingRectWithSize:CGSizeMake(200, MAXFLOAT)
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:@{NSFontAttributeName : font}
                                     context:nil];
    
    return ceil(rect.size.height / font.lineHeight);
}

- (IBAction)btnStartdisJobBtnClicked:(id)sender
{

    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self startReservation];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection first."];
    }
    
}
- (IBAction)cancelreservationBtnClicked:(id)sender
{
    CancelReservationVC *crvc=[[CancelReservationVC alloc]init];
    crvc.strReservationID=[dictCustomerInfo valueForKey:@"id"];
    [self.navigationController pushViewController:crvc animated:YES];
}


-(void)acceptReservation
{
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    
    NSString *url = [NSString stringWithFormat:@"acceptReservation"];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[dictCustomerInfo objectForKey:@"id"],@"id", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/acceptReservation
    
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            ReservationAcceptedVC *ravc=[[ReservationAcceptedVC alloc]initWithNibName:@"ReservationAcceptedVC" bundle:nil];
            ravc.dictUserData=dictCustomerInfo;
            [self.navigationController pushViewController:ravc animated:YES];
            
        }
        else
        {
            
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self acceptReservation];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}

-(void)startReservation
{
    NSString *url = [NSString stringWithFormat:@"startReservation"];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[dictCustomerInfo objectForKey:@"id"],@"id", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/acceptReservation
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            JobProgressVC *jpvc=[[JobProgressVC alloc]init];
            jpvc.dictCustomerInfo=dictCustomerInfo;
            [self.navigationController pushViewController:jpvc animated:YES];

        }
        else
        {
            
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self startReservation];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
