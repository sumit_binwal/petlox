//
//  SelectBreedVC.h
//  Petlox
//
//  Created by Sumit Sharma on 11/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectBreedVC : UIViewController
{
    NSMutableArray  *arrAllrecords;
}
@property (nonatomic,strong)NSString *strBreed;
- (IBAction)searchTextFiled:(UITextField *)sender;
- (IBAction)backButtonAction:(id)sender;
@end
