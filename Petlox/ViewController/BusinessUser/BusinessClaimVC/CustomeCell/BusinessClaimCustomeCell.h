//
//  BusinessClaimCustomeCell.h
//  Petlox
//
//  Created by Sumit Sharma on 25/08/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BusinessClaimCustomeCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgBusinessimg;
@property (strong, nonatomic) IBOutlet UILabel *lblBusinessName;
@property (strong, nonatomic) IBOutlet UILabel *lblBusinessAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblBusinessPhnNumber;
@property (strong, nonatomic) IBOutlet UIImageView *imgTopDevider;
@property (strong, nonatomic) IBOutlet UIView *vwImgBorder;
@property (strong, nonatomic) IBOutlet UIButton *claimBtn;
@property (strong, nonatomic) IBOutlet UIImageView *imgClaimedBg;

@end
