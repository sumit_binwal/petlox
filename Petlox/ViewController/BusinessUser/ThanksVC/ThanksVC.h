//
//  ThanksVC.h
//  Petlox
//
//  Created by Sumit Sharma on 03/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThanksVC : UIViewController
@property (nonatomic,strong)NSString *reservationID;
@property (nonatomic,strong)NSString *businessName;
@property(nonatomic,strong)NSMutableDictionary *dictReservationDetail;
@end
