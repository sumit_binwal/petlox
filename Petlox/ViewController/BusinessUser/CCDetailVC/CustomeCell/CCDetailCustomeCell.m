//
//  CCDetailCustomeCell.m
//  Petlox
//
//  Created by Sumit Sharma on 03/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "CCDetailCustomeCell.h"

@implementation CCDetailCustomeCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    
    self=(CCDetailCustomeCell *)[[[NSBundle mainBundle]loadNibNamed:@"CCDetailCustomeCell" owner:self options:nil]objectAtIndex:0];
    
    return self;
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
