//
//  BusinessSignupSecndVC.h
//  Petlox
//
//  Created by Sumit Sharma on 01/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BusinessSignupSecndVC : UIViewController
{
    
}
@property (nonatomic,strong)NSMutableDictionary *dataDict;
@property (nonatomic,strong)NSMutableDictionary *claimDataDict;
@property(nonatomic,strong)NSMutableDictionary *dictPhnVerifyData;
@end
