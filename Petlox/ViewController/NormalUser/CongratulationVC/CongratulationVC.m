//
//  CongratulationVC.m
//  Petlox
//
//  Created by Sumit Sharma on 14/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "CongratulationVC.h"
#import "HomeScreenSpecsVC.h"
@interface CongratulationVC ()

@end

@implementation CongratulationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    wbServiceCount=1;
    [self setUpView];
    // Do any additional setup after loading the view from its nib.
}
-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    self.finishBtn.layer.cornerRadius = self.finishBtn.frame.size.width/2;
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)finishBtnClicked:(id)sender {
    
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self setClaimStatus];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)setClaimStatus
{
    
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"sid", nil];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];

    
    NSString *url = [NSString stringWithFormat:@"is_status"];
    NSLog(@"%@",param);
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.173/petlox_svn/mobile/is_claim
    
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        
        
        if (operation.response.statusCode==206) {
            
        }
        else if(operation.response.statusCode==200)
        {
            if(responseDict==Nil)        {
                [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
                
            }
            
            else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            {
                [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"is_status"] forKey:UD_STATUS_COMPLETE];
                [[NSUserDefaults standardUserDefaults]synchronize];
                NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:UD_STATUS_COMPLETE]);
//                APPDELEGATE.window=[[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
                
                IIViewDeckController* deckController = [APPDELEGATE generateControllerStack];
                
                [APPDELEGATE.window setRootViewController:deckController];
                [APPDELEGATE.window makeKeyAndVisible];

                
//                IIViewDeckController* deckController = [APPDELEGATE generateControllerStack];
//                
//                [APPDELEGATE.window setRootViewController:deckController];
//                [APPDELEGATE.window makeKeyAndVisible];
                
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
                
            }
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo   responce%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              //  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self setClaimStatus];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}


@end
