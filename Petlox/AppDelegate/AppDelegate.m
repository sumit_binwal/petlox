//
//  AppDelegate.m
//  Petlox
//
//  Created by Sumit Sharma on 21/07/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "AppDelegate.h"
#import "SignInVC.h"
#import "RightMenuVC.h"
#import <GoogleMaps/GoogleMaps.h>
#import "HomeScreenSpecsVC.h"
#import "ChooseHoursVC.h"
#import "ReservationVC.h"
#import "ReserveNowVC.h"
#import "MyReservationVC.h"
#import "MainHomeVC.h"
#import "CancelReasonVC.h"
#import "CalendarVC.h"
#import "UploadPhotoVC.h"
#import "BusinessRightMenu.h"
#import "MessageInboxVC.h"
//#import <Fabric/Fabric.h>
//#import <Crashlytics/Crashlytics.h>

#import "HomeScreenSpecsVC.h"

//facebook login
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

//Google login
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
//#import <Crittercism/Crittercism.h>

#import "NotificationVC.h"

// normal user pet reservation tabbar view controllers
#import "ServiceProviderVC.h"
#import "WriteReviewVC.h"
#import "OperationHoursVC.h"
#import "ViewPhotoVC.h"
#import "ThanksVC.h"
#import "ReviewShowVC.h"

@interface AppDelegate ()<GPPSignInDelegate,GPPDeepLinkDelegate>
{
 
}
@end

@implementation AppDelegate
@synthesize window=_window;
@synthesize centerController = _viewController;
@synthesize leftController = _leftController;
@synthesize locationManager;
@synthesize K_dictUserDdetails;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
//    NSLog(@"%@",[UIFont fontNamesForFamilyName:@"Open Sans"]);
    
// Override point for customization after application launch.
//    locationManager.delegate = self; // we set the delegate of locationManager to self.
//    locationManager.desiredAccuracy = kCLLocationAccuracyBest; // setting the accuracy
//    
//    [locationManager startUpdatingLocation];
    //[Fabric with:@[CrashlyticsKit]];

    wbServiceCount=1;
    
    
    [self updateUserPresentLocation];
    
   // [Crittercism enableWithAppID:@"563069308d4d8c0a00d07f95"];
    
    NSString *isprofileCompeleteFlag=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:UD_IS_PET_PROFILE_COMPLETE]];
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:UD_STATUS_COMPLETE]);
      NSLog(@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID]);
    NSString *isCompleteStatus=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:UD_STATUS_COMPLETE]];
    
    
    if ([[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID]&&[isCompleteStatus isEqualToString:@"1"])
    {
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:UD_BUSINESS_TYPE]isEqualToString:@"business"])
        {

            //self.window=[[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
            
            IIViewDeckController* deckController = [self generateBusinessControllerStack];
            
            [self.window setRootViewController:deckController];
            [self.window makeKeyAndVisible];

        }
        else
        {
           // self.window=[[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
            IIViewDeckController* deckController = [self generateControllerStack];
            
            [self.window setRootViewController:deckController];
            [self.window makeKeyAndVisible];
        }
    }
    else
    {
        
        MainHomeVC *sivc=[[MainHomeVC alloc]init];
        //CalendarVC *sivc=[[CalendarVC alloc]init];
        self.navController = [[UINavigationController alloc] initWithRootViewController:sivc];
        self.window.rootViewController = self.navController;
        [self.window makeKeyAndVisible];
    
    }
   // [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    
    [GMSServices provideAPIKey:@"AIzaSyDe1YduKOUMdRuj1JO9Gv3nK2mKpcUIoiw"];
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    [GPPSignIn sharedInstance].clientID = kGoogleplusClientID;
    [GPPDeepLink setDelegate:self];
    [self getDeviceTokenId];
    //[CommonFunctions showActivityIndicatorWithText:@""];
   [self resetBedgeCount];
    
    [self createNormalUserPetReserveTabBar];
    return YES;
}


-(void)getDeviceTokenId
{
    //This code will work in iOS 8.0 xcode 6.0 or later
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeNewsstandContentAvailability| UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    
}
#pragma mark-Register for Device token

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString *devicToken = [[[[deviceToken description]
                              stringByReplacingOccurrencesOfString: @"<" withString: @""]
                             stringByReplacingOccurrencesOfString: @">" withString: @""]
                            stringByReplacingOccurrencesOfString: @" " withString: @""];
    NSLog(@"Device_Token     -----> %@\n",devicToken);
    
    pushDeviceToken=devicToken;
 //   [UserDefaults setObject:devicToken forKey:@"deviceToken"];
//    [UserDefaults synchronize];
    
}


//code by chhagan
-(void)updateUserPresentLocation
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        [self.locationManager requestAlwaysAuthorization] ;
        [self.locationManager startUpdatingLocation];
    }
    else
    {
        locationManager = [[CLLocationManager alloc] init];
        [locationManager setDistanceFilter:kCLDistanceFilterNone];
        [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
        [locationManager setDelegate:self];
        [locationManager startUpdatingLocation];
    }
}


- (IIViewDeckController*)generateControllerStack
{
    RightMenuVC *rearViewController = [[RightMenuVC alloc]initWithNibName:@"RightMenuVC" bundle:[NSBundle mainBundle]];
    UINavigationController *rearNaviCtrl = [[UINavigationController alloc]initWithRootViewController:rearViewController];
    
    UIViewController *centerController;
    centerController = [[HomeScreenSpecsVC alloc]initWithNibName:@"HomeScreenSpecsVC" bundle:nil];
//    centerController = [[CalendarVC alloc]initWithNibName:@"CalendarVC" bundle:nil];

    centerController = [[UINavigationController alloc] initWithRootViewController:centerController];
    
    IIViewDeckController* deckController =  [[IIViewDeckController alloc] initWithCenterViewController:centerController leftViewController:rearNaviCtrl rightViewController:nil];
    deckController.leftSize = 0;
    deckController.shadowEnabled=NO;
    [deckController disablePanOverViewsOfClass:NSClassFromString(@"_UITableViewHeaderFooterContentView")];
    return deckController;
}

- (IIViewDeckController*)generateBusinessControllerStack
{
    BusinessRightMenu *rearViewController = [[BusinessRightMenu alloc]init];
    UIViewController *centerController;
    centerController = [[ReservationVC alloc]initWithNibName:@"ReservationVC" bundle:nil];
    // centerController = [[CancelReasonVC alloc]initWithNibName:@"CancelReasonVC" bundle:nil];
    centerController = [[UINavigationController alloc] initWithRootViewController:centerController];
    IIViewDeckController* deckController =  [[IIViewDeckController alloc] initWithCenterViewController:centerController leftViewController:rearViewController rightViewController:nil];
    deckController.leftSize = 0;
    deckController.shadowEnabled=NO;
    [deckController disablePanOverViewsOfClass:NSClassFromString(@"_UITableViewHeaderFooterContentView")];
    [self.window makeKeyAndVisible];
    return deckController;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
       [self resetBedgeCount];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
   
return ([[FBSDKApplicationDelegate sharedInstance] application:application
                                                       openURL:url
                                             sourceApplication:sourceApplication
                                                    annotation:annotation]||
        
        [GPPURLHandler handleURL:url sourceApplication:sourceApplication annotation:annotation]);

}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    [FBSDKAppEvents activateApp];
application.applicationIconBadgeNumber = 0;
    // Do the following if you use Mobile App Engagement Ads to get the deferred
    // app link after your app is installed.
    [FBSDKAppLinkUtility fetchDeferredAppLink:^(NSURL *url, NSError *error) {
        if (error) {
            NSLog(@"Received error while fetching deferred app link %@", error);
        }
        if (url) {
            [[UIApplication sharedApplication] openURL:url];
        }
    }];
    
    [self updateUserPresentLocation];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[GPPSignIn sharedInstance] disconnect];
    [[GPPSignIn sharedInstance] signOut];
}

#pragma mark - LocationManager Delegate Methods
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
//    UIAlertView *errorAlert = [[UIAlertView alloc]
//                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //NSLog(@"didUpdateToLocation: %f", newLocation.coordinate.longitude);
    currentLocation = newLocation;
    
    if (currentLocation != nil)
    {
        
        CurrentLongitude = currentLocation.coordinate.longitude;
        CurrentLatitude = currentLocation.coordinate.latitude;

//        if ([[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID])
//        {
//        HomeScreenSpecsVC *home=[[HomeScreenSpecsVC alloc]init];
//        [home userComeFromBackGroundActiveState];
//        }        
    }
    
}


#pragma mark- webservice implemented
-(void)UpdateUserDataWithCompletetion:(void (^)(NSArray *))completion WithFailure:(void (^)(NSString *))failure
{
    NSLog(@"=-098=-0=-=--=-=-=-=-=----###################################");
    
    NSMutableArray  *arryRecords=[[NSMutableArray alloc]init];
    NSString *latitude=[NSString stringWithFormat:@"%f",CurrentLatitude];
    NSString *longitutde=[NSString stringWithFormat:@"%f",CurrentLongitude];
    NSString *tokenID=[[NSUserDefaults standardUserDefaults]valueForKey:UD_TOKEN_ID];
    if (tokenID.length>0) {
        
        NSLog(@"getting responseObject frm bg %@,-------%@-------%@",latitude,longitutde,tokenID);
        NSMutableDictionary *params=[[NSMutableDictionary alloc]initWithDictionary:@{@"user_id":tokenID,@"lat":latitude,@"lon":longitutde,@"page":@(1).stringValue}];
        
        [[ConnectionManager sharedInstance]startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:@"serviceListingRange" withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             NSLog(@"getting responseObject frm bg %@",responseObject);
             
             NSDictionary* result = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                    options:kNilOptions
                                                                      error:nil];
             NSLog(@"getting results frm bg %@",result);
             [arryRecords addObjectsFromArray:[result valueForKey:@"Services"]];
             completion(arryRecords);
         } withFailure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             [CommonFunctions removeActivityIndicator];
         }];
    }
}

#pragma mark - NOtification Recive
- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:UD_BUSINESS_TYPE]);
    if ([application applicationState]==UIApplicationStateBackground ||[application applicationState]==UIApplicationStateInactive ) {
        if([[userInfo objectForKey:@"pushType"]isEqualToString:@"message"])
        {
            IIViewDeckController* deckController = [self generateControllerStackForPushNotificationMessage];
            
            [self.window setRootViewController:deckController];
            
        }
        else if ([[[NSUserDefaults standardUserDefaults]objectForKey:UD_BUSINESS_TYPE]isEqualToString:@"user"] && [[userInfo objectForKey:@"pushType"]isEqualToString:@"reservation"])
        {
            IIViewDeckController* deckController = [self generateControllerStackForPushNotificationUserReservation];
            
            [self.window setRootViewController:deckController];
        }
        else if ([[[NSUserDefaults standardUserDefaults]objectForKey:UD_BUSINESS_TYPE]isEqualToString:@"business"] && [[userInfo objectForKey:@"pushType"]isEqualToString:@"reservation"])
        {
            IIViewDeckController* deckController = [self generateControllerStackForPushNotificationBusinessReservation];
            
            [self.window setRootViewController:deckController];

        }

    }

//    if ([application applicationState]==UIApplicationStateActive) {
//        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
//        if([[userInfo objectForKey:@"pushType"]isEqualToString:@"message"])
//        {
//            IIViewDeckController* deckController = [self generateControllerStackForPushNotificationMessage];
//            
//            [self.window setRootViewController:deckController];
//        }
//
//    }
//    //your code execution will here.
//    if([application applicationState] == UIApplicationStateInactive)
//    {
//        if([[userInfo objectForKey:@"pushType"]isEqualToString:@"message"])
//        {
//            IIViewDeckController* deckController = [self generateControllerStackForPushNotificationMessage];
//            
//            [self.window setRootViewController:deckController];
//            
//        }
//
//    }
    
}

- (IIViewDeckController*)generateControllerStackForPushNotificationMessage
{
    RightMenuVC *rearViewController = [[RightMenuVC alloc]init];
    UIViewController *centerController;
    centerController = [[MessageInboxVC alloc]initWithNibName:@"MessageInboxVC" bundle:nil];
    // centerController = [[CancelReasonVC alloc]initWithNibName:@"CancelReasonVC" bundle:nil];
    
    centerController = [[UINavigationController alloc] initWithRootViewController:centerController];
    
    IIViewDeckController* deckController =  [[IIViewDeckController alloc] initWithCenterViewController:centerController leftViewController:rearViewController rightViewController:nil];
    deckController.leftSize = 0;
    deckController.shadowEnabled=NO;
    [deckController disablePanOverViewsOfClass:NSClassFromString(@"_UITableViewHeaderFooterContentView")];
    return deckController;
}
- (IIViewDeckController*)generateControllerStackForPushNotificationUserReservation
{
    RightMenuVC *rearViewController = [[RightMenuVC alloc]init];
    UIViewController *centerController;
    centerController = [[MyReservationVC alloc]initWithNibName:@"MyReservationVC" bundle:nil];
    // centerController = [[CancelReasonVC alloc]initWithNibName:@"CancelReasonVC" bundle:nil];
    
    centerController = [[UINavigationController alloc] initWithRootViewController:centerController];
    
    IIViewDeckController* deckController =  [[IIViewDeckController alloc] initWithCenterViewController:centerController leftViewController:rearViewController rightViewController:nil];
    deckController.leftSize = 0;
    deckController.shadowEnabled=NO;
    [deckController disablePanOverViewsOfClass:NSClassFromString(@"_UITableViewHeaderFooterContentView")];
    return deckController;
}
- (IIViewDeckController*)generateControllerStackForPushNotificationBusinessReservation
{
    RightMenuVC *rearViewController = [[RightMenuVC alloc]init];
    UIViewController *centerController;
    centerController = [[ReservationVC alloc]initWithNibName:@"ReservationVC" bundle:nil];
    // centerController = [[CancelReasonVC alloc]initWithNibName:@"CancelReasonVC" bundle:nil];
    
    centerController = [[UINavigationController alloc] initWithRootViewController:centerController];
    
    IIViewDeckController* deckController =  [[IIViewDeckController alloc] initWithCenterViewController:centerController leftViewController:rearViewController rightViewController:nil];
    deckController.leftSize = 0;
    deckController.shadowEnabled=NO;
    [deckController disablePanOverViewsOfClass:NSClassFromString(@"_UITableViewHeaderFooterContentView")];
    return deckController;
}

#pragma mark - WebServiceApi
-(void)resetBedgeCount
{
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    
    NSMutableDictionary *param;
    
    NSString *url = [NSString stringWithFormat:@"resetUserBadgeCount"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/resetUserBadgeCount
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
       // [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
           // [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
                    }
        else
        {
           // [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                            
                                             // [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                
                                                  [self resetBedgeCount];
                                              }
                                              else
                                              {
                                                
                                               //   [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}


-(void)createNormalUserPetReserveTabBar{
    
    self.bottomBar = [[UIView alloc] initWithFrame:CGRectMake(0, K_SCREEN_HEIGHT-51, K_SCREEN_WIDTH, 51)];
    self.bottomBar.backgroundColor = [UIColor colorWithRed:216.0/255.0 green:216.0/255.0 blue:216.0/255.0 alpha:1.0];
    self.petReservationTabBar = [[UITabBar alloc] initWithFrame:CGRectMake(0, 1, K_SCREEN_WIDTH, 50)];
    self.petReservationTabBar.delegate = self;
    self.petReservationTabBar.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0];
    [self.petReservationTabBar setTintColor:[UIColor colorWithRed:71.0/255.0 green:13.0/255.0 blue:52.0/255.0 alpha:1.0]];

    UITabBarItem *itemReserve = [[UITabBarItem alloc] initWithTitle:@"Reserve" image:[UIImage imageNamed:@"reserve_unfoucs"] selectedImage:[UIImage imageNamed:@"reserve_foucs"]];
    itemReserve.tag = 0;
    UITabBarItem *itemReview = [[UITabBarItem alloc] initWithTitle:@"Review" image:[UIImage imageNamed:@"review_unfoucs"] selectedImage:[UIImage imageNamed:@"review_foucs"]];
    itemReview.tag = 1;
    UITabBarItem *itemCall = [[UITabBarItem alloc] initWithTitle:@"Call" image:[UIImage imageNamed:@"call_unfoucs"] selectedImage:[UIImage imageNamed:@"call_foucs"]];
    itemCall.tag = 2;
    UITabBarItem *itemHours = [[UITabBarItem alloc] initWithTitle:@"Hours" image:[UIImage imageNamed:@"hours_unfoucs"] selectedImage:[UIImage imageNamed:@"hours_foucs"]];
    itemHours.tag = 3;
    UITabBarItem *itemPhotos = [[UITabBarItem alloc] initWithTitle:@"Photos" image:[UIImage imageNamed:@"photos_unfoucs"] selectedImage:[UIImage imageNamed:@"photos_foucs"]];
    itemPhotos.tag = 4;
    
    self.petReservationTabBar.items = [NSArray arrayWithObjects:itemReserve, itemReview, itemCall, itemHours, itemPhotos, nil];
    
    [self.bottomBar addSubview:self.petReservationTabBar];
    [[UIApplication sharedApplication].keyWindow addSubview:self.bottomBar];
    [self showPetReservationTabBar:NO];
}

-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    IIViewDeckController *rootVC = (IIViewDeckController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    self.navController = (UINavigationController *)rootVC.centerController;
    
    if(item.tag==0) {
        ServiceProviderVC *serviceProviderVC = (ServiceProviderVC *)[CommonFunctions exists:[ServiceProviderVC class] in:self.navController];
        if(serviceProviderVC == nil){
            serviceProviderVC = [[ServiceProviderVC alloc] init];
            [self.navController pushViewController:serviceProviderVC animated:YES];
        } else {
            [self.navController popToViewController:serviceProviderVC animated:YES];
        }
    } else if(item.tag==1) {
        
        [self checkUserReviewStatuswith:self.navController];
        
    } else if(item.tag==2) {
        UIDevice *device = [UIDevice currentDevice];
        NSString *cellNameStr=[K_dictUserDdetails objectForKey:@"phone"];
        if ([[device model] isEqualToString:@"iPhone"] ) {
            
            NSString *phoneNumber = [@"telprompt://" stringByAppendingString:cellNameStr];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
            
            #if TARGET_OS_SIMULATOR
                [CommonFunctions alertTitle:@"" withMessage:@"Your device doesn't support this feature."];
            #endif
            
        } else {
            
            [CommonFunctions alertTitle:@"" withMessage:@"Your device doesn't support this feature."];
        }
        [self highlightReserveTab];
        
    } else if(item.tag==3) {
        OperationHoursVC *operationHoursVC = (OperationHoursVC *)[CommonFunctions exists:[OperationHoursVC class] in:self.navController];
        if(operationHoursVC == nil){
            operationHoursVC = [[OperationHoursVC alloc] init];
            operationHoursVC.businessUserID=[K_dictUserDdetails valueForKey:@"user_id"];
            [self.navController pushViewController:operationHoursVC animated:YES];
        } else {
            operationHoursVC.businessUserID=[K_dictUserDdetails valueForKey:@"user_id"];
            [self.navController popToViewController:operationHoursVC animated:YES];
        }
    } else if(item.tag==4) {
        ViewPhotoVC *viewPhotoVC = (ViewPhotoVC *)[CommonFunctions exists:[ViewPhotoVC class] in:self.navController];
        if(viewPhotoVC == nil){
            viewPhotoVC = [[ViewPhotoVC alloc] init];
            viewPhotoVC.businessID=[K_dictUserDdetails objectForKey:@"user_id"];
            [self.navController pushViewController:viewPhotoVC animated:YES];
        } else {
            viewPhotoVC.businessID=[K_dictUserDdetails objectForKey:@"user_id"];
            [self.navController popToViewController:viewPhotoVC animated:YES];
        }
    }
}

-(void)showPetReservationTabBar:(BOOL)shouldShow{
    [[UIApplication sharedApplication].keyWindow bringSubviewToFront:self.bottomBar];
    self.petReservationTabBar.selectedItem = [self.petReservationTabBar.items objectAtIndex:0];
    self.bottomBar.hidden = !shouldShow;
    
}

-(void)highlightReserveTab{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        self.petReservationTabBar.selectedItem = [self.petReservationTabBar.items objectAtIndex:0];
    });
}

-(void)checkUserReviewStatuswith:(UINavigationController *)navigationController
{
    NSMutableDictionary *param;
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"user_id",[K_dictUserDdetails objectForKey:@"user_id"],@"business_id", nil];
    
    NSString *url = [NSString stringWithFormat:@"checkUserReview"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/checkUserReview
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            ReviewShowVC *rsvc=[[ReviewShowVC alloc]init];
            rsvc.dictReviewPost=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[responseDict objectForKey:@"review"],@"msg",[responseDict objectForKey:@"rating"],@"rating", nil];
            rsvc.dictBusinessUserDetail=K_dictUserDdetails;
            [navigationController pushViewController:rsvc animated:YES];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"error"])
        {
            if ([[[responseDict objectForKey:@"service"]stringValue ]isEqualToString:@"1"]) {
                
                WriteReviewVC *writeReviewVC = (WriteReviewVC *)[CommonFunctions exists:[WriteReviewVC class] in:self.navController];
                if(writeReviewVC == nil){
                    writeReviewVC = [[WriteReviewVC alloc] init];
                    writeReviewVC.dictBusinessDetail=K_dictUserDdetails;
                    [self.navController pushViewController:writeReviewVC animated:YES];
                } else {
                    writeReviewVC.dictBusinessDetail=K_dictUserDdetails;
                    [self.navController popToViewController:writeReviewVC animated:YES];
                }
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:@" Please take service of business before writing review."];
                [self highlightReserveTab];
            }
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              [self highlightReserveTab];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self checkUserReviewStatuswith:navigationController];
                                              }
                                              else
                                              {
                                                  [CommonFunctions removeActivityIndicator];
                                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                                  [self highlightReserveTab];
                                              }
                                              
                                          }
                                          
                                      }];
}


@end
