//
//  BusinessClaimVC.m
//  Petlox
//
//  Created by Sumit Sharma on 25/08/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "BusinessClaimVC.h"
#import "BusinessClaimCustomeCell.h"
#import "CallMeNowVC.h"
#import "BusinessSignupFirstVC.h"
@interface BusinessClaimVC ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate>
{
    IBOutlet UILabel *lblSearchYourBusiness;
    IBOutlet UILabel *lblMsg;
    
    IBOutlet UIButton *btnAddBusiness;
    
    IBOutlet UITextField *txtSearchBox;
    
    NSMutableArray *businessListArr;
    
    NSString *imgBaseUrl;
    
    IBOutlet UITableView *tblVwBusinessList;
}
@end

@implementation BusinessClaimVC

#pragma mark - Life Cycle Method
- (void)viewDidLoad {
    wbServiceCount=1;
    [super viewDidLoad];
    [self setUpView];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    btnAddBusiness.titleLabel.numberOfLines = 0;
    btnAddBusiness.titleLabel.textAlignment =NSTextAlignmentCenter;
     

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setUpView
{
    //SetupFor Navigation Bar...
    
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Find Business"];
    txtSearchBox = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, 208*SCREEN_XScale, 25.0)];
    
    txtSearchBox.clearButtonMode = UITextFieldViewModeWhileEditing;
    UIView *vw=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, 0)];
    txtSearchBox.leftViewMode=UITextFieldViewModeAlways;
    txtSearchBox.leftView=vw;
    txtSearchBox.delegate=self;
    txtSearchBox.placeholder=@"Search";
    txtSearchBox.autocorrectionType=UITextAutocorrectionTypeNo;
    //TextField Placeholder Color Change
    [txtSearchBox setValue:[UIColor colorWithRed:133.0f/255.0f green:133.0f/255.0f blue:133.0f/255.0f alpha:1]
                   forKeyPath:@"_placeholderLabel.textColor"];
    txtSearchBox.layer.cornerRadius=5.0f;
    
    
    
    [txtSearchBox setFont:[UIFont fontWithName:FONT_OPENSANS size:13.0f*SCREEN_XScale]];
    txtSearchBox.tintColor=[UIColor blackColor];
    [txtSearchBox setBackgroundColor:[UIColor whiteColor]];
    
    self.navigationItem.titleView = txtSearchBox;

    
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];

    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureBtnClicked)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];
    
}


#pragma mark - IbAction Button Methods
-(IBAction)claimBtnClicked:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if ([[[businessListArr objectAtIndex:btn.tag]objectForKey:@"is_claim"]isEqualToString:@"0"]) {
        CallMeNowVC *cmvc=[[CallMeNowVC alloc]init];
        cmvc.phnNumber=[[businessListArr objectAtIndex:btn.tag] objectForKey:@"phone"];
        [[NSUserDefaults standardUserDefaults]setObject:[[businessListArr objectAtIndex:btn.tag] objectForKey:@"user_id"] forKey:UD_CLAIM_ID];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self.navigationController pushViewController:cmvc animated:YES];
        
    }
    
}
-(void)tapGestureBtnClicked
{
    [self.view endEditing:YES];
}
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)addBusinesstoPetlox:(id)sender {
    BusinessSignupFirstVC *bsfvc=[[BusinessSignupFirstVC alloc]init];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_CLAIM_ID];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self.navigationController pushViewController:bsfvc animated:YES];
}

#pragma mark - TextFieldDelegate Methods
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
  return [textField textInputMode] != nil;
}
-(BOOL)textFieldShouldClear:(UITextField *)textField
{

    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.text.length>1) {
        if (textField==txtSearchBox) {
            if ([CommonFunctions reachabiltyCheck]) {
                [CommonFunctions showActivityIndicatorWithText:@""];
                [self searchBusiness];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection"];
            }
            
        }
    }
    return YES;
}

#pragma mark - TableView Delegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return businessListArr.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 94*SCREEN_XScale;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [txtSearchBox resignFirstResponder];
    static NSString *cellIdentifier=@"claimCellIdentifier";
    BusinessClaimCustomeCell *cell=[[BusinessClaimCustomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    if (cell==nil) {
        cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }

    if (indexPath.row>0) {
        [cell.imgTopDevider setHidden:YES];
    }

    
    if ([[[businessListArr objectAtIndex:indexPath.row]objectForKey:@"is_claim"]isEqualToString:@"0"]) {
        [cell.claimBtn addTarget:self action:@selector(claimBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.claimBtn.tag=indexPath.row;
        [cell.imgClaimedBg setHidden:YES];
    }
    else
    {
        [cell.claimBtn setImage:[UIImage imageNamed:@"btnClaimed"] forState:UIControlStateNormal];
        [cell.claimBtn setTitle:@"" forState:UIControlStateNormal];
                [cell.imgClaimedBg setHidden:NO];
    }
    cell.lblBusinessName.text=[[[businessListArr objectAtIndex:indexPath.row] objectForKey:@"business_name"] capitalizedString];
    cell.lblBusinessAddress.text=[[businessListArr objectAtIndex:indexPath.row] objectForKey:@"address"];
    cell.lblBusinessPhnNumber.text=[[businessListArr objectAtIndex:indexPath.row] objectForKey:@"phone"];
    
    NSString *imgStr=[[businessListArr objectAtIndex:indexPath.row] objectForKey:@"user_image"];
    if (imgStr.length>1) {
        NSURL *imgUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[businessListArr objectAtIndex:indexPath.row] objectForKey:@"user_image"]]];
        
        [cell.imgBusinessimg setImageWithURL:imgUrl usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
    }
    else
    {
        [cell.imgBusinessimg setImage:[UIImage imageNamed:@"GroomingIcon"]];
    }
    cell.imgBusinessimg.clipsToBounds=YES;

    return cell;
    
}

#pragma mark- WebServices API..

-(void)getBusinessList
{

    
    NSString *url = [NSString stringWithFormat:@"businesslisting"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.173/petlox_svn/mobile/businesslisting
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            businessListArr =[[NSMutableArray alloc]initWithArray:[responseDict objectForKey:@"data"]];
            imgBaseUrl=[responseDict objectForKey:@"image_prefix"];
            [lblMsg setHidden:YES];
            [btnAddBusiness setHidden:YES];
            [tblVwBusinessList setHidden:NO];
            [tblVwBusinessList reloadData];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self getBusinessList];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          
                                      }];
}

-(void)searchBusiness
{
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:txtSearchBox.text,@"keyword", nil];
    NSString *url = [NSString stringWithFormat:@"businesslisting"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.173/petlox_svn/mobile/businesslisting

    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
                NSLog(@"Responce Code : %ld",(long)operation.response.statusCode);
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];

        
        if (operation.response.statusCode==206) {
            [tblVwBusinessList setHidden:YES];
            [lblMsg setHidden:NO];
            [lblSearchYourBusiness setHidden:YES];
            [btnAddBusiness setHidden:NO];
        }
        else if(operation.response.statusCode==200)
        {
            if(responseDict==Nil)        {
                [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
                
            }
        
            else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            {
                [lblSearchYourBusiness setHidden:YES];
                [txtSearchBox resignFirstResponder];
                businessListArr =[[NSMutableArray alloc]initWithArray:[responseDict objectForKey:@"data"]];
                imgBaseUrl=[responseDict objectForKey:@"image_prefix"];
                [lblMsg setHidden:YES];
                [btnAddBusiness setHidden:YES];
                [tblVwBusinessList setHidden:NO];
                [tblVwBusinessList reloadData];
             }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            }
    }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo   responce%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self searchBusiness];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          
                                      }];
}

@end
