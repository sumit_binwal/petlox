//
//  SelectCardTypeVC.m
//  Petlox
//
//  Created by Sumit Sharma on 02/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "SelectCardTypeVC.h"
#import "AddCardVC.h"
#import "SelectCardCell.h"
@interface SelectCardTypeVC ()<UITableViewDelegate,UITableViewDataSource>
{
    
    IBOutlet UITableView *tblVw;
    IBOutlet UILabel *lblSelected;
    IBOutlet UIButton *showCardBtn;
    BOOL isCardSelected;
    NSString *cardType;
    IBOutlet UIButton *btnArrow;
    NSMutableArray *arrData;
    IBOutlet UIButton *btnNext;
}
@end

@implementation SelectCardTypeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    // Do any additional setup after loading the view from its nib.
}
-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];

    btnNext.layer.cornerRadius=btnNext.bounds.size.width/2*SCREEN_XScale;
    [btnNext.titleLabel setFont:[UIFont fontWithName:btnNext.titleLabel.font.fontName size:btnNext.titleLabel.font.pointSize*SCREEN_XScale]];

    
    arrData =[[NSMutableArray alloc]initWithObjects:@"VisaCard",@"MasterCardSelected",@"AmericanCardSelected",@"DiscoverCard",@"PaypalCard", nil];
    
    [self.navigationItem setTitle:@"Payment Method"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction Button Method
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)visaButtonClicked:(id)sender {
        [showCardBtn setImage:[UIImage imageNamed:@"VisaCardSelected"] forState:UIControlStateNormal];
    cardType=@"visa";
    [lblSelected setHidden:NO];
    [btnArrow setHidden:NO];
    isCardSelected=true;
}
- (IBAction)masterCardBtnClicked:(id)sender {
        [showCardBtn setImage:[UIImage imageNamed:@"MasterCardSelected"] forState:UIControlStateNormal];
        cardType=@"master";
    [lblSelected setHidden:NO];
    [btnArrow setHidden:NO];
        isCardSelected=true;
}
- (IBAction)americanExpressBtnClicked:(id)sender {
    [showCardBtn setImage:[UIImage imageNamed:@"AmericanCardSelected"] forState:UIControlStateNormal];
            cardType=@"american";
    [lblSelected setHidden:NO];
    [btnArrow setHidden:NO];
        isCardSelected=true;
}
- (IBAction)discoverCardbtnClicked:(id)sender {
        [showCardBtn setImage:[UIImage imageNamed:@"DiscoverCardSelected"] forState:UIControlStateNormal];
                cardType=@"discover";
    [lblSelected setHidden:NO];
    [btnArrow setHidden:NO];
        isCardSelected=true;
}
- (IBAction)paypalBtnClicked:(id)sender {
        [showCardBtn setImage:[UIImage imageNamed:@"PaypalCardSelected"] forState:UIControlStateNormal];
                    cardType=@"paypal";
    [lblSelected setHidden:NO];
    [btnArrow setHidden:NO];
        isCardSelected=true;
}
- (IBAction)nextButtonClicked:(id)sender {
    if (cardType.length>1) {
        AddCardVC *acvc=[[AddCardVC alloc]init];
        acvc.cardType=cardType;
        [self.navigationController pushViewController:acvc animated:YES];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please select atleast one card ."];
    }
}

#pragma mark - UITableView Delegate Method

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrData.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f*SCREEN_XScale;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
 static NSString *cellIdentifier=@"SelectCardCell";
    
    SelectCardCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[[SelectCardCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(SelectCardCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell.imgCardDetail setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrData objectAtIndex:indexPath.row]]]];
    cell.backgroundColor=[UIColor clearColor];
    
    if ([cardType isEqualToString:[arrData objectAtIndex:indexPath.row]]) {
        [cell.btnCheckUncheck setImage:[UIImage imageNamed:@"cardCheck"] forState:UIControlStateNormal];
    }
    else
    {
        [cell.btnCheckUncheck setImage:[UIImage imageNamed:@"cardUncheck"] forState:UIControlStateNormal];
    }
    
    
    [cell.btnCheckUncheck addTarget:self action:@selector(checkUnCheckBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnCheckUncheck.tag=indexPath.row;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    cardType=arrData[indexPath.row];
    [tblVw reloadData];
}

-(IBAction)checkUnCheckBtnClicked:(UIButton*)sender
{
    cardType=arrData[sender.tag];
    [tblVw reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
