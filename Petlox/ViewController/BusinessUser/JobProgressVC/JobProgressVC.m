//
//  CustomerInfoVC.m
//  Petlox
//
//  Created by Sumit Sharma on 15/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "JobProgressVC.h"
#import "DeclineVC.h"
#import "ReservationAcceptedVC.h"
#import "CancelReservationVC.h"
#import "UpdateStatusVC.h"
@interface JobProgressVC ()<UIAlertViewDelegate>
{
    
    IBOutlet UIButton *btnMore;
    IBOutlet UIScrollView *scrllVw;

    IBOutlet UIButton *btnFinish;
    IBOutlet UIButton *btnUpdate;
    IBOutlet UIImageView *imgPetImage;
    IBOutlet UIImageView *imgUserImage;
    IBOutlet UILabel *lblDIscription;
    IBOutlet UILabel *lblPetWeight;
    IBOutlet UILabel *lblPetAge;
    IBOutlet UILabel *lblPetBreed;
    IBOutlet UILabel *lblStatus;
    IBOutlet UIButton *btnCancleReservation;
    IBOutlet UILabel *lblReservationTime;
    IBOutlet UILabel *lblPetName;
    IBOutlet UIButton *acceptBtn;
    IBOutlet UILabel *lblUserName;
    IBOutlet UIButton *declineBtn;
    IBOutlet UIView *vwForNewReservation;
    IBOutlet UIView *vwForAcceptedReservation;
    IBOutlet UILabel *lblServiceRequested;
    IBOutlet UIView *vwDiscription;
}
@end

@implementation JobProgressVC
@synthesize dictCustomerInfo,strFromTap,strstatusForJobComplection;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    
    int numberofLine=(int)[self lineCountForText:[dictCustomerInfo objectForKey:@"petdiscription"]];
    if ([UIScreen mainScreen].bounds.size.width>=375) {
        [btnMore setHidden:YES];
        [vwDiscription setHidden:NO];
        lblDIscription.numberOfLines=0;
        
        
    }
    else if ([UIScreen mainScreen].bounds.size.height==568)
    {
        if (numberofLine<=2) {
        [btnMore setHidden:YES];
        }
        else
        {
                    [btnMore setHidden:NO];
        }

        [vwDiscription setHidden:NO];
        lblDIscription.numberOfLines=2;
    }
    else if ([UIScreen mainScreen].bounds.size.height<568)
    {

        [vwDiscription setHidden:NO];
        lblDIscription.numberOfLines=0;
        [btnMore setHidden:YES];
        [scrllVw setScrollEnabled:YES];
        [scrllVw setContentSize:CGSizeMake(320.0f, self.view.frame.size.height+vwDiscription.frame.size.height)];
    }

    wbServiceCount=1;
    // Do any additional setup after loading the view from its nib.
}

-(void)setUpView
{

    
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Job In Progress"];
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
    [imgUserImage setHidden:YES];
    [imgUserImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dictCustomerInfo objectForKey:@"image"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    imgPetImage.clipsToBounds=YES;
    
    [imgPetImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dictCustomerInfo objectForKey:@"petimage"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    lblPetAge.text=[NSString stringWithFormat:@"%@ Years",[dictCustomerInfo objectForKey:@"age"]];
    lblPetBreed.text=[NSString stringWithFormat:@"%@, %@",[dictCustomerInfo objectForKey:@"pet"],[dictCustomerInfo objectForKey:@"breed"]];
    lblPetName.text=[dictCustomerInfo objectForKey:@"pet_name"];
    lblServiceRequested.text=[dictCustomerInfo objectForKey:@"service"];
    lblPetWeight.text=[NSString stringWithFormat:@"%@ lbs",[dictCustomerInfo objectForKey:@"weight"]];

    if ([UIScreen mainScreen].bounds.size.height<=568) {
        [btnMore setHidden:NO];
            lblDIscription.text=[dictCustomerInfo objectForKey:@"petdiscription"];
                }
    else
    {
            lblDIscription.text=[dictCustomerInfo objectForKey:@"petdiscription"];
    }
    
    
    
    btnUpdate.layer.cornerRadius=btnUpdate.bounds.size.width/2*SCREEN_XScale;
    [btnUpdate.titleLabel setFont:[UIFont fontWithName:btnUpdate.titleLabel.font.fontName size:btnUpdate.titleLabel.font.pointSize*SCREEN_XScale]];
    
    btnFinish.layer.cornerRadius=btnFinish.bounds.size.width/2*SCREEN_XScale;
    [btnFinish.titleLabel setFont:[UIFont fontWithName:btnFinish.titleLabel.font.fontName size:btnFinish.titleLabel.font.pointSize*SCREEN_XScale]];
    
    btnFinish.titleLabel.textAlignment=NSTextAlignmentCenter;
    btnFinish.titleLabel.numberOfLines=0;
    
    btnUpdate.titleLabel.textAlignment=NSTextAlignmentCenter;
    btnUpdate.titleLabel.numberOfLines=0;

    
    lblReservationTime.text=[dictCustomerInfo objectForKey:@"reservation_date"];
    lblUserName.text=[NSString stringWithFormat:@"%@ %@",[dictCustomerInfo objectForKey:@"first_name"],[dictCustomerInfo objectForKey:@"last_name"]];
    [vwDiscription setHidden:YES];
    
    if ([strFromTap isEqualToString:@"0"])
    {
        [vwForAcceptedReservation setHidden:YES];
        [vwForNewReservation setHidden:NO];
    }
    else
    {

        [vwForAcceptedReservation setHidden:NO];
        [vwForNewReservation setHidden:YES];
    }
   }

- (int)lineCountForText:(NSString *) text
{
    UIFont *font = [UIFont fontWithName:@"Arial" size:14.0f];
    
    CGRect rect = [text boundingRectWithSize:CGSizeMake(200, MAXFLOAT)
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:@{NSFontAttributeName : font}
                                     context:nil];
    
    return ceil(rect.size.height / font.lineHeight);
}

-(void)viewDidLayoutSubviews
{
    if ([UIScreen mainScreen].bounds.size.height<568) {
        [scrllVw setContentSize:CGSizeMake(320.0f, self.view.frame.size.height+lblDIscription.frame.size.height+160)];
        [scrllVw setScrollEnabled:YES];
    }
}
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction Methods

- (IBAction)btnAcceptClicked:(id)sender {
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self acceptReservation];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }
}
- (IBAction)btnDeclineClicked:(id)sender {
    DeclineVC *dvc=[[DeclineVC alloc]init];
    dvc.fromCancel=@"";
    dvc.reservationID=[dictCustomerInfo objectForKey:@"id"];
    [self.navigationController pushViewController:dvc animated:YES];
}

- (IBAction)moreBtnClicked:(id)sender {
    UIButton *btn=(UIButton *)sender;
    NSLog(@"%f",lblDIscription.frame.size.height);
    if ([UIScreen mainScreen].bounds.size.height==568) {
        if (btn.tag==0) {
            
            [btnMore setImage:[UIImage imageNamed:@"btnLessImg"] forState:UIControlStateNormal];
            [scrllVw setScrollEnabled:YES];
            [UIView animateWithDuration:0.2 animations:^ {
                
                lblDIscription.numberOfLines=0;
                [scrllVw setContentSize:CGSizeMake(320.0f, ((self.view.frame.size.height+lblDIscription.frame.size.height)+90))];
                [scrllVw setContentOffset:CGPointMake(0.0f,lblDIscription.frame.size.height+90)];
                
            }];
            btn.tag=1;
        }
        else
        {
            [btnMore setImage:[UIImage imageNamed:@"btnMoreImg"] forState:UIControlStateNormal];
            [UIView animateWithDuration:0.2 animations:^ {
                [scrllVw setScrollEnabled:NO];
                [scrllVw setContentOffset:CGPointZero];
                lblDIscription.numberOfLines=2;
            }];
            btn.tag=0;
        }

    }
    
    
}
- (IBAction)btnStartdisJobBtnClicked:(id)sender
{
}
- (IBAction)cancelreservationBtnClicked:(id)sender
{
    CancelReservationVC *crvc=[[CancelReservationVC alloc]init];
    crvc.strReservationID=[dictCustomerInfo valueForKey:@"id"];
    [self.navigationController pushViewController:crvc animated:YES];
}
- (IBAction)updateStatusBtnClicked:(id)sender {
    
    if ([strstatusForJobComplection isEqualToString:@"yes"]) {
       
    }
    else
    {
        UpdateStatusVC *usvc=[[UpdateStatusVC alloc]init];
        usvc.reservationID=[dictCustomerInfo valueForKey:@"id"];
      
   [usvc setZipCodeInputBlock:^(NSString *str)
    {
        strstatusForJobComplection=str;
    }];
          [self.navigationController pushViewController:usvc animated:YES];
    }
}
- (IBAction)finishJobBtnClicked:(id)sender {
    
    UpdateStatusVC *usvc=[[UpdateStatusVC alloc]init];
    usvc.fromFinishJob=@"fromFinishJob";
    usvc.reservationID=[dictCustomerInfo valueForKey:@"id"];
    [self.navigationController pushViewController:usvc animated:YES];
}


-(void)acceptReservation
{
    
    NSString *url = [NSString stringWithFormat:@"acceptReservation"];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[dictCustomerInfo objectForKey:@"id"],@"id", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/acceptReservation
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            ReservationAcceptedVC *ravc=[[ReservationAcceptedVC alloc]initWithNibName:@"ReservationAcceptedVC" bundle:nil];
            ravc.dictUserData=dictCustomerInfo;
            [self.navigationController pushViewController:ravc animated:YES];
            
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self acceptReservation];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}
-(void)completeJob
{
    
    NSString *url = [NSString stringWithFormat:@"addReservationCompleteStatus"];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[dictCustomerInfo objectForKey:@"id"],@"id", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/checkReservationStartStatus
    
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"] withDelegate:self withTag:100];
        }
        else
        {
            
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{

                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self completeJob];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}


@end
