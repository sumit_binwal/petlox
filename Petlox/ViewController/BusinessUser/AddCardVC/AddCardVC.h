//
//  AddCardVC.h
//  Petlox
//
//  Created by Sumit Sharma on 02/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddCardVC : UIViewController
{
    
}
@property (strong,nonatomic)NSString *cardType;
@property(strong,nonatomic)NSMutableDictionary *cardDataDict;
@end
