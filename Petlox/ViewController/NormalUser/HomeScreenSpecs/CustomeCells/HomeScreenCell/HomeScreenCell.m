//
//  HomeScreenCell.m
//  DemoPetLox
//
//  Created by Chagan Singh on 14/09/15.
//  Copyright (c) 2015 Chagan Singh. All rights reserved.
//

#import "HomeScreenCell.h"

@implementation HomeScreenCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self = (HomeScreenCell *)[[[NSBundle mainBundle] loadNibNamed:@"HomeScreenCell" owner:self options:nil] firstObject];
        
    }
    
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


@end
