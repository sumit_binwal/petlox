//
//  ReportBugVC.m
//  Petlox
//
//  Created by Sumit Sharma on 14/12/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "ReportBugVC.h"

@interface ReportBugVC ()
{
    IBOutlet UIButton *btnSubmit;
}

@end

@implementation ReportBugVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    wbServiceCount=1;
    // Do any additional setup after loading the view from its nib.
}
-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Report A Bug"];
    
    UIBarButtonItem *backButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"menu-icon"] style:UIBarButtonItemStylePlain target:self.viewDeckController action:@selector(toggleLeftView)];
    [self.navigationItem setLeftBarButtonItem:backButton];
    
    [_txtTitle setValue:[UIColor colorWithRed:98.0f/255.0f green:98.0f/255.0f blue:98.0f/255.0f alpha:1]
            forKeyPath:@"_placeholderLabel.textColor"];

    
    UIView *v1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 15.0f, 0)];
    _txtTitle.leftViewMode=UITextFieldViewModeAlways;
    _txtTitle.leftView=v1;
    
    btnSubmit.clipsToBounds = YES;
    
    //half of the width
    btnSubmit.layer.cornerRadius = 125.0/2.0f;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITextField Delegate Method
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==_txtTitle) {
        [_txtBugDiscription becomeFirstResponder];
    }
    return NO;
}

#pragma mark - UITextView Delegate Method
-(void)textViewDidChange:(UITextView *)textView
{
    if (textView.text.length>0) {
        [_lblTxtDiscription setHidden:YES];
    }
    else
    {
        [_lblTxtDiscription setHidden:NO];
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{

    if ([text isEqualToString:@"\n"]) {
        [_txtBugDiscription resignFirstResponder];
    }
        return YES;

}

- (IBAction)submitBtnClicked:(id)sender
{
    if ([CommonFunctions isValueNotEmpty:_txtTitle.text]) {
        if ([CommonFunctions isValueNotEmpty:_txtBugDiscription.text]) {
            if ([CommonFunctions reachabiltyCheck]) {
                [CommonFunctions showActivityIndicatorWithText:@""];
                [self postABug];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
            }
        }
        else
        {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter description."];
        }
        
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter title."];
    }
    
}


#pragma mark - WebServiceAPI


-(void)postABug
{
    
    NSString *url = [NSString stringWithFormat:@"report_a_bug"];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"user_id",[CommonFunctions trimSpaceInString:_txtTitle.text],@"title",[CommonFunctions trimSpaceInString:_txtBugDiscription.text],@"description", nil];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/businessUserReservaionList
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            _txtBugDiscription.text=@"";
            _txtTitle.text=@"";
            [_lblTxtDiscription setHidden:NO];
            [_txtTitle becomeFirstResponder];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self postABug];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}

@end
