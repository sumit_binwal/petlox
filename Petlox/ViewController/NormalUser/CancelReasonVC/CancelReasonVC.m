//
//  CancelReasonVC.m
//  Petlox
//
//  Created by Sumit Sharma on 03/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "CancelReasonVC.h"
#import "ReservationCanceledVC.h"
#import "SettingsCell.h"

@interface CancelReasonVC ()<UITextViewDelegate,UIGestureRecognizerDelegate, UITableViewDelegate, UITableViewDataSource>
{
    
    IBOutlet UIView *textFieldVw;
    IBOutlet UIScrollView *scrllVw;
    IBOutlet UIImageView *imgSelectedArrow1;
    IBOutlet UIImageView *imgSelectedArrow2;
    IBOutlet UIImageView *imgSelectedArrow3;
    IBOutlet UIButton *btnOther;
    IBOutlet UIButton *btnChangedMyMind;
    IBOutlet UIButton *btnReservedByAccident;
    IBOutlet UILabel *lblTxtViewTxt;
    IBOutlet UISwitch *onOffSwitch;
    IBOutlet UILabel *lblMsgCount;
    IBOutlet UITextView *txtViewDiscription;
    NSString *strReasonForCancle;
    
    
    IBOutlet UITableView *tblView;
    IBOutlet UIButton *btnSend;
    NSInteger selectedIndexPath;
}
@end

@implementation CancelReasonVC
@synthesize reservationID;
- (void)viewDidLoad {
    [super viewDidLoad];
    selectedIndexPath = -1;
    [self setUpView];
    // Do any additional setup after loading the view from its nib.
}

-(void)setUpView
{
    wbServiceCount=1;
    [textFieldVw setHidden:YES];
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Reason For Cancellation"];
         [self.navigationController.navigationBar setHidden:NO];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];

//    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTap)];
//    tapGesture.numberOfTapsRequired=1;
//    tapGesture.delegate=self;
//    [self.view addGestureRecognizer:tapGesture];
    
//    if ([UIScreen mainScreen].bounds.size.height<568) {
//        [scrllVw setContentSize:CGSizeMake(320.0f, 568.0f)];
//        [scrllVw setScrollEnabled:YES];
//    }
    
    btnSend.clipsToBounds = YES;
}
-(void)viewDidLayoutSubviews
{
//    if ([UIScreen mainScreen].bounds.size.height<568) {
//        [scrllVw setContentSize:CGSizeMake(320.0f, 568.0f)];
//        [scrllVw setScrollEnabled:YES];
//    }
    //half of the width
    btnSend.layer.cornerRadius = btnSend.frame.size.width/2;
}
-(void)singleTap{
    [self.view endEditing:YES];
    [scrllVw setScrollEnabled:NO];
    [scrllVw setContentOffset:CGPointZero];
    [scrllVw setScrollEnabled:NO];
    [scrllVw setScrollEnabled:NO];
    
}
- (IBAction)cancleReservationReasonBtnClicked:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    switch (btn.tag) {
        case 0:
        {
//            [imgSelectedArrow1 setHidden:NO];
//            [imgSelectedArrow2 setHidden:YES];
//            [imgSelectedArrow3 setHidden:YES];
            strReasonForCancle=@"Reserved By Accident";
            [onOffSwitch setHidden:YES];
            [textFieldVw setHidden:YES];
            break;
        }
        case 1:
        {
//            [imgSelectedArrow1 setHidden:YES];
//            [imgSelectedArrow2 setHidden:NO];
//            [imgSelectedArrow3 setHidden:YES];
            strReasonForCancle=@"Changed my Mind";
            [onOffSwitch setHidden:YES];
            [textFieldVw setHidden:YES];
            break;
        }
        case 2:
        {
//            [imgSelectedArrow1 setHidden:YES];
//            [imgSelectedArrow2 setHidden:YES];
//            [imgSelectedArrow3 setHidden:NO];
            strReasonForCancle=@"Other";
            [onOffSwitch setHidden:NO];
            txtViewDiscription.text=@"";
            [onOffSwitch setOn:YES animated:YES];
            [textFieldVw setHidden:NO];
            txtViewDiscription.userInteractionEnabled=YES;
            lblMsgCount.text=@"200 Characters";
            break;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextView Delegate Methods
-(void)textViewDidChange:(UITextView *)textView
{
    if (textView.text.length>0) {
        [lblTxtViewTxt setHidden:YES];
    }
    else
    {
        [lblTxtViewTxt setHidden:NO];
    }
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self scrollViewToCenterOfScreen:textView];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (textView==txtViewDiscription) {
        lblMsgCount.text = [NSString stringWithFormat:@"%u Characters",200-range.location];
        
        
        if (range.location>=200) {
            lblMsgCount.text=@"0 Character";
            return NO;
            
            
        }
        else
        {
            if ([text isEqualToString:@"\n"]) {
                // [self scrollToNormalView];
                [txtViewDiscription resignFirstResponder];
                [self singleTap];
                return YES;
            }
            return YES;
        }
    }
    
    else
    {
        return YES;
    }
    return YES;
}



#pragma mark-Scroll View Method
-(void)scrollViewToCenterOfScreen:(UITextView *)textField
{
    [scrllVw setScrollEnabled:YES];
    float difference;
    if (scrllVw.contentSize.height == 600)
        difference = 50.0f;
    else
        difference = 50.0f;
    CGFloat viewCenterY = textField.center.y+150;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat avaliableHeight = applicationFrame.size.height - 10.0f;
    CGFloat y = viewCenterY - avaliableHeight / 10.0f;
    
    NSLog(@"%f",y);
    if (y < 0)
        y = 0;
    
    [scrllVw setContentOffset:CGPointMake(0, y) animated:YES];
    [scrllVw setContentSize:CGSizeMake(320.0f, 730.0f)];
}


- (IBAction)switchButtonClicked:(id)sender {
    [self singleTap];
    if ([sender isOn]) {
        txtViewDiscription.userInteractionEnabled=YES;
        [textFieldVw setHidden:NO];
    }
    else
    {
        txtViewDiscription.userInteractionEnabled=NO;
        txtViewDiscription.text=@"";
        [textFieldVw setHidden:YES];
        [self textViewDidChange:txtViewDiscription];
    }
}
- (IBAction)cancelReservationBtnClicked:(id)sender {
   
    [self animateTextView: NO];
    [txtViewDiscription resignFirstResponder];
    
    if (strReasonForCancle.length>1)
    {
        if (onOffSwitch.isOn && [strReasonForCancle isEqualToString:@"Other"]) {
           
            if (txtViewDiscription.text.length>0) {
                if ([CommonFunctions reachabiltyCheck]) {
                    [CommonFunctions showActivityIndicatorWithText:@""];
                    [self cancleReaservation];
                    
                }
                else
                {
                    [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
                }
                
            }
            else
            {
                 [CommonFunctions alertTitle:@"" withMessage:@"Please enter reason for cancellation."];
            }
            
        }
        
        else
        {
            if ([CommonFunctions reachabiltyCheck]) {
                [CommonFunctions showActivityIndicatorWithText:@""];
                [self cancleReaservation];
                
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
            }
        }
    }
    else
        
    {
         [CommonFunctions alertTitle:@"" withMessage:@"Please select reason for cancellation."];
    }

   
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark- WebServices API..

-(void)cancleReaservation
{
    NSMutableDictionary *param;
    
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:reservationID,@"id",strReasonForCancle,@"reason_for_cancel",txtViewDiscription.text,@"massage", nil];
NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    
    NSString *url = [NSString stringWithFormat:@"cancelReservation"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/cancelReservation
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            ReservationCanceledVC *rcvc=[[ReservationCanceledVC alloc]init];
            rcvc.dictCancleReservationData=[[responseDict objectForKey:@"data"] objectAtIndex:0];
            [self.navigationController pushViewController:rcvc animated:YES];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              

                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self cancleReaservation];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          
                                      }];
}

#pragma mark - UITableViewDelegate / DataSource Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float rowHeight = 0.0f;
    rowHeight = 50;
    return rowHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 3;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *settingsCell   = @"SettingsCell";
    
    SettingsCell *cell = (SettingsCell *)[tableView dequeueReusableCellWithIdentifier:settingsCell];
    
    if (cell == nil)
    {
        cell = (SettingsCell*)[[[NSBundle mainBundle] loadNibNamed:@"SettingsCell" owner:self options:nil] objectAtIndex:0];
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    cell.imgViewArrow.hidden = YES;
    
    if (indexPath.row == 0)
    {
        cell.lblTxt.text = @"Reserved By Accident";
    }
    else if (indexPath.row == 1)
    {
        cell.lblTxt.text = @"Changed my Mind";
    }
    else if (indexPath.row == 2)
    {
        cell.lblTxt.text = @"Other";
    }
    
    if(selectedIndexPath == indexPath.row){
        cell.imgViewArrow.hidden = NO;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedIndexPath = indexPath.row;
    UIButton *btn = [UIButton new];
    btn.tag = indexPath.row;
    [self cancleReservationReasonBtnClicked:btn];
    [tableView reloadData];
}

#pragma mark -
#pragma mark UITextView Delegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    [self animateTextView: YES];
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    [self animateTextView: NO];
    return YES;
}

- (void) animateTextView:(BOOL) up
{
    if (up)
    {
        // Assign new frame to your view
        [self.view setFrame:CGRectMake(0,-40,[APPDELEGATE window].frame.size.width,[APPDELEGATE window].frame.size.height-40)];
    }
    else
    {
        // Assign new frame to your view
        [self.view setFrame:CGRectMake(0,64,[APPDELEGATE window].frame.size.width,[APPDELEGATE window].frame.size.height-64)];
    }
}


@end
