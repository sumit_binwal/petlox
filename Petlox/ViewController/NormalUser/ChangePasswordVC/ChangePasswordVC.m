//
//  ChangePasswordVC.m
//  Petlox
//
//  Created by Sumit Sharma on 10/12/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "ChangePasswordVC.h"

@interface ChangePasswordVC ()<UITextFieldDelegate>
{
    
    IBOutlet UITextField *txtOldPassword;
    IBOutlet UITextField *txtNewPassword;
    IBOutlet UITextField *txtCnfirmPassword;
    
    IBOutlet UIButton *btnSave;
    IBOutlet UIView *view1, *view2, *view3;
}
@end

@implementation ChangePasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
        // Do any additional setup after loading the view from its nib.
}

-(void)viewDidLayoutSubviews
{
    btnSave.layer.cornerRadius = btnSave.frame.size.width/2;
    view1.layer.cornerRadius = 5.0f;
    view2.layer.cornerRadius = 5.0f;
    view3.layer.cornerRadius = 5.0f;
}

-(void)setUpView
{
    wbServiceCount=1;
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(bckBtnClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
    [txtOldPassword setValue:[UIColor colorWithRed:129.0f/255.0f green:128.0f/255.0f blue:128.0f/255.0f alpha:1]
                  forKeyPath:@"_placeholderLabel.textColor"];
    [txtNewPassword setValue:[UIColor colorWithRed:129.0f/255.0f green:128.0f/255.0f blue:128.0f/255.0f alpha:1]
                  forKeyPath:@"_placeholderLabel.textColor"];
    [txtCnfirmPassword setValue:[UIColor colorWithRed:129.0f/255.0f green:128.0f/255.0f blue:128.0f/255.0f alpha:1]
                     forKeyPath:@"_placeholderLabel.textColor"];
    
    [self.navigationItem setTitle:@"Change Password"];

}
- (IBAction)bckBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)isTextFieldValidate
{
    
    if (![CommonFunctions isValueNotEmpty:txtOldPassword.text]) {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter old password."];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtNewPassword.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter new password."];
        return NO;
    }
    else if (txtNewPassword.text.length<6) {
        [CommonFunctions alertTitle:@"" withMessage:@"Password length between 6 to 14 characters."];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtCnfirmPassword.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter confirm password."];
        return NO;
    }
    else if (![txtNewPassword.text isEqualToString:txtCnfirmPassword.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Password and confirm password does not match."];
        return NO;
    }
    return YES;
}

#pragma mark - IBAction Methods
- (IBAction)saveBtnClicked:(id)sender {
    [self.view endEditing:YES];
    if ([self isTextFieldValidate]) {
        if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self changePasswordAPI];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection"];
        }
    }
}



#pragma mark - UITextField Delegate methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==txtOldPassword) {
        [txtNewPassword becomeFirstResponder];
    }
    else if (textField==txtNewPassword)
    {
        [txtCnfirmPassword becomeFirstResponder];
    }
    else if (textField==txtCnfirmPassword)
    {
        [txtCnfirmPassword resignFirstResponder];
    }
    return YES;
}

#pragma mark - WebService API

-(void)changePasswordAPI
{
    NSString *url = [NSString stringWithFormat:@"changePassword"];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"user_id",[CommonFunctions trimSpaceInString:txtOldPassword.text],@"oldpassword",[CommonFunctions trimSpaceInString:txtCnfirmPassword.text],@"password", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://mymeetingdesk.com/mobile/petlox/mobile/changePassword
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            
            txtCnfirmPassword.text=@"";
            txtNewPassword.text=@"";
            txtOldPassword.text=@"";
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            
            
        }
        else
        {
            
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self changePasswordAPI];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          
                                      }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
