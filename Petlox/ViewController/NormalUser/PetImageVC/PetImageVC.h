//
//  PetImageVC.h
//  Petlox
//
//  Created by Sumit Sharma on 14/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PetImageVC : UIViewController
@property(nonatomic,strong)NSString *petID;
@property(nonatomic,strong)NSString *strPetImage;
@property (weak, nonatomic) IBOutlet UIButton *btnContinue;
@end
