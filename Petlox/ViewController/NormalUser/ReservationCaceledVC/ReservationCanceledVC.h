//
//  ReservationCanceledVC.h
//  Petlox
//
//  Created by Sumit Sharma on 06/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReservationCanceledVC : UIViewController
@property(nonatomic,strong)NSMutableDictionary *dictCancleReservationData;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@end
