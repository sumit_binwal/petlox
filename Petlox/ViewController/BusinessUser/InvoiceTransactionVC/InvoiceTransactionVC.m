//
//  InvoiceTransactionVC.m
//  Petlox
//
//  Created by Sumit Sharma on 09/11/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "InvoiceTransactionVC.h"

@interface InvoiceTransactionVC ()
{
    
    IBOutlet UILabel *lblServiceRequested;
    IBOutlet UILabel *lblServiceStaus;
    IBOutlet UILabel *lblDate;
    IBOutlet UILabel *lblPetName;
    IBOutlet UILabel *lblPetStatus;
    IBOutlet UILabel *lblPetBreed;
    IBOutlet UILabel *lblPetAge;
    IBOutlet UILabel *lblWeight;
    IBOutlet UILabel *lblPetDescription;
    IBOutlet UIImageView *imgPetVw;
    IBOutlet UIView *vwButtons;
    
    IBOutlet UIButton *btnMore;
    IBOutlet UIScrollView *scrllVw;
    IBOutlet UILabel *lblBusinessName;
    IBOutlet UILabel *lblReservationDate;
    IBOutlet UIImageView *imgBusinessVw;
    
    IBOutlet UILabel *lblInvoiceStatus;
    IBOutlet UILabel *lblInvoiceDate;
    IBOutlet UILabel *lblInvoiceAmounrt;
    IBOutlet UIImageView *imgInvoiceStatus;
    IBOutlet UILabel *lblInvoiceTitle;
    
    IBOutlet UILabel *lblReview;
    IBOutlet UIImageView *imgStar1;
    IBOutlet UIImageView *imgStar2;
    IBOutlet UIImageView *imgStar3;
    IBOutlet UIImageView *imgStar4;
    IBOutlet UIImageView *imgStar5;
    
    IBOutlet UIView *vwDiscription;
    IBOutlet UIView *vwTransaction;
    IBOutlet UIButton *btnReview;
    IBOutlet UIView *vwService;
    IBOutlet UIView *vwReview;
    IBOutlet UIButton *btnService;
    IBOutlet UIButton *btnTransaction;
    NSMutableDictionary *dictDetailedInvoiceData;
}
@end

@implementation InvoiceTransactionVC
@synthesize strReservationID;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    wbServiceCount=1;
    
    
    
    // Do any additional setup after loading the view from its nib.
}


-(void)viewDidLayoutSubviews
{
    if ([UIScreen mainScreen].bounds.size.height<568) {
        [scrllVw setContentSize:CGSizeMake(320.0f, self.view.frame.size.height+lblPetDescription.frame.size.height+160)];
        [scrllVw setScrollEnabled:YES];
    }
    
    else if ([UIScreen mainScreen].bounds.size.height==568 )  {
        lblPetDescription.numberOfLines=0;
        [scrllVw setContentSize:CGSizeMake(320.0f, self.view.frame.size.height+lblPetDescription.frame.size.height+90)];
      //  [scrllVw setScrollEnabled:YES];
    }
    else if ([UIScreen mainScreen].bounds.size.width>=375 )  {
        lblPetDescription.numberOfLines=0;
        [btnMore setHidden:YES];
        [scrllVw setScrollEnabled:NO];
    }
    //    else if ([UIScreen mainScreen].bounds.size.height==568) {
    //        NSLog(@"%f",vwDiscription.frame.size.height+vwDiscription.frame.origin.y);
    //        [scrllVw setContentSize:CGSizeMake(320.0f, 667.0f)];
    //
    //        if (vwDiscription.isHidden==YES) {
    //        [scrllVw setScrollEnabled:NO];
    //            NSLog(@"fsdaf");
    //        }
    //        else
    //        {
    //        [scrllVw setScrollEnabled:YES];
    //        }
    //    }
    else if ([UIScreen mainScreen].bounds.size.width==375)
    {
        [scrllVw setContentSize:CGSizeMake(320.0f,667.0f)];
        if (lblPetDescription.numberOfLines==3) {
            [scrllVw setScrollEnabled:NO];
        }
        else
        {
            [scrllVw setScrollEnabled:YES];
        }
        
    }
}


-(void)setupView
{
    [vwDiscription setHidden:YES];
    
    [btnTransaction setImage:[UIImage imageNamed:@"imgTransactionFocus"] forState:UIControlStateNormal];
    [btnService setImage:[UIImage imageNamed:@"imgService"] forState:UIControlStateNormal];
    [btnReview setImage:[UIImage imageNamed:@"imgReview"] forState:UIControlStateNormal];


    [self.navigationItem setTitle:@"Transaction"];

    [vwTransaction setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-(vwButtons.frame.size.height+self.navigationController.navigationBar.frame.size.height))];
    [self.view addSubview:vwTransaction];
    
    
    
    [self.view bringSubviewToFront:vwButtons];
    
    
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getInvoiceDetailData];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }
    
    
    if ([UIScreen mainScreen].bounds.size.width==414) {
        [btnMore setHidden:YES];
        [vwDiscription setHidden:NO];
        
    }
    else if ([UIScreen mainScreen].bounds.size.width==375)
    {
        [btnMore setHidden:NO];
         [vwDiscription setHidden:NO];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568)
    {
                    [btnMore setHidden:NO];
    }

    imgBusinessVw.clipsToBounds=YES;
    imgPetVw.clipsToBounds=YES;
}
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction Button Clicked

- (IBAction)moreBtnClicked:(id)sender {
    UIButton *btn=(UIButton *)sender;
    NSLog(@"%f",lblPetDescription.frame.size.height);
    
    if (btn.tag==0) {
        
        [btnMore setImage:[UIImage imageNamed:@"btnLessImg"] forState:UIControlStateNormal];
        [scrllVw setScrollEnabled:YES];
        [UIView animateWithDuration:0.2 animations:^ {
            [scrllVw setContentSize:CGSizeMake(320.0f, ((self.view.frame.size.height+vwDiscription.frame.size.height)+190))];
            [scrllVw setContentOffset:CGPointMake(0.0f,vwDiscription.frame.size.height+110)];
//            [scrllVw setContentSize:CGSizeMake(320.0f, ((self.view.frame.size.height+lblPetDescription.frame.size.height)+40))];
//            [scrllVw setContentOffset:CGPointMake(0.0f,lblPetDescription.frame.size.height+40)];
            [vwDiscription setHidden:NO];
        }];
        btn.tag=1;
    }
    else
    {
        [btnMore setImage:[UIImage imageNamed:@"btnMoreImg"] forState:UIControlStateNormal];
        [UIView animateWithDuration:0.2 animations:^ {
            [scrllVw setScrollEnabled:NO];
            [scrllVw setContentOffset:CGPointZero];
            [vwDiscription setHidden:YES];
        }];
        btn.tag=0;
    }
}

- (IBAction)segmentButtonClicked:(id)sender {
    
    UIButton *btn=(UIButton *)sender;
    int btnTag=btn.tag;
    [btnTransaction setImage:[UIImage imageNamed:@"imgTransaction"] forState:UIControlStateNormal];
    [btnService setImage:[UIImage imageNamed:@"imgService"] forState:UIControlStateNormal];
    [btnReview setImage:[UIImage imageNamed:@"imgReview"] forState:UIControlStateNormal];
    [vwTransaction setFrame:[UIScreen mainScreen].bounds];
    [vwService setFrame:[UIScreen mainScreen].bounds];
    [vwReview setFrame:[UIScreen mainScreen].bounds];
    
    
    CGRect thisRect = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-(vwButtons.frame.size.height+self.navigationController.navigationBar.frame.size.height));

    switch (btnTag) {
        case 0:
        {
        
    [vwTransaction setFrame:[UIScreen mainScreen].bounds];
            [self.navigationItem setTitle:@"Transaction"];
            [vwTransaction setFrame:thisRect];
            
            [vwService removeFromSuperview];
            [vwReview removeFromSuperview];
            [self.view addSubview:vwTransaction];
            [btnTransaction setImage:[UIImage imageNamed:@"imgTransactionFocus"] forState:UIControlStateNormal];
            [self.view bringSubviewToFront:vwButtons];
            
            break;
        }
        case 1:
        {
            
           [vwService setFrame:[UIScreen mainScreen].bounds];
                        [self.navigationItem setTitle:@"Service"];
            [self.navigationController.navigationBar setTranslucent:NO];
            CGRect thisRect = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-vwButtons.frame.size.height);
            [vwService setFrame:thisRect];
            
            [btnService setImage:[UIImage imageNamed:@"imgServiceFocus"] forState:UIControlStateNormal];
            [vwTransaction removeFromSuperview];
            [vwReview removeFromSuperview];
            [self.view addSubview:vwTransaction];
            
            [self.view addSubview:vwService];
            [self.view bringSubviewToFront:vwButtons];
            break;
        }
        case 2:
        {

            [vwReview setFrame:[UIScreen mainScreen].bounds];
            [self.navigationItem setTitle:@"Review"];
            [self.navigationController.navigationBar setTranslucent:NO];
            [vwReview setFrame:thisRect];
            [btnReview setImage:[UIImage imageNamed:@"imgReviewFocus"] forState:UIControlStateNormal];
            [vwTransaction removeFromSuperview];
            [vwService removeFromSuperview];
            [self.view addSubview:vwReview];
            [self.view bringSubviewToFront:vwButtons];
            break;
        }
        default:
            break;
    }

}


#pragma mark- WebService Calling Methods
-(void)getInvoiceDetailData
{
    NSString *url;
    url = [NSString stringWithFormat:@"ReservationBillingHistory"];
     NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:strReservationID,@"id", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://mymeetingdesk.com/mobile/petlox/mobile/ReservationBillingHistory
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            
            
            
            
            CGRect thisRect = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-(vwButtons.frame.size.height+self.navigationController.navigationBar.frame.size.height));
            
            //modify required frame parameter (.origin.x/y, .size.width/height)
            
            [self.navigationItem setTitle:@"Transaction"];
            [vwTransaction setFrame:thisRect];
            [self.view addSubview:vwTransaction];
[self.view bringSubviewToFront:vwButtons];
            
            dictDetailedInvoiceData=[[responseDict objectForKey:@"data"] objectAtIndex:0];
            lblInvoiceTitle.text=[NSString stringWithFormat:@"Invoice - %@",[dictDetailedInvoiceData objectForKey:@"invoice_number"]];
            lblInvoiceAmounrt.text=[NSString stringWithFormat:@"$ %@",[dictDetailedInvoiceData objectForKey:@"invoice_amount"]];
            lblInvoiceDate.text=[NSString stringWithFormat:@"%@",[dictDetailedInvoiceData objectForKey:@"invoice_date"]];
//            lblInvoiceStatus.text=[NSString stringWithFormat:@"%@",[dictDetailedInvoiceData objectForKey:@""]];
            if ([[dictDetailedInvoiceData objectForKey:@"invoice_status"] isEqualToString:@"1"]) {
                [imgInvoiceStatus setImage:[UIImage imageNamed:@"checkMarkActive"]];
                lblInvoiceStatus.text=@"Completed";
                lblServiceStaus.text=@"Completed";
            }
            else
            {
                [imgInvoiceStatus setImage:[UIImage imageNamed:@"checkMarkInactive"]];
                                lblInvoiceStatus.text=@"Pending";
                lblServiceStaus.text=@"Pending";
            }
            
            lblBusinessName.text=[NSString stringWithFormat:@"%@ %@",[dictDetailedInvoiceData valueForKey:@"first_name"],[dictDetailedInvoiceData valueForKey:@"last_name"]];
            lblReservationDate.text=[dictDetailedInvoiceData valueForKey:@"reservation_date"];
            NSString *imgStr=[dictDetailedInvoiceData objectForKey:@"user_image"];

            imgBusinessVw.clipsToBounds=YES;
            if (imgStr.length>0) {
                
                [imgBusinessVw setImageWithURL:[NSURL URLWithString:imgStr] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            }
            
            
            lblPetAge.text=[NSString stringWithFormat:@"%@ Years",[dictDetailedInvoiceData valueForKey:@"age"]];
            lblPetBreed.text=[NSString stringWithFormat:@"%@, %@",[dictDetailedInvoiceData valueForKey:@"pet"],[dictDetailedInvoiceData valueForKey:@"breed"]];
            lblPetDescription.text=[dictDetailedInvoiceData valueForKey:@"pet_description"];
            lblPetName.text=[dictDetailedInvoiceData valueForKey:@"pet_name"];
            lblPetStatus.text=[dictDetailedInvoiceData valueForKey:@"pet_status"];
            lblServiceRequested.text=[dictDetailedInvoiceData valueForKey:@"service"];
            lblWeight.text=[NSString stringWithFormat:@"%@ lbs",[dictDetailedInvoiceData valueForKey:@"weight"]];
            lblDate.text=[dictDetailedInvoiceData valueForKey:@"invoice_date"];
            NSString *strPetImg=[dictDetailedInvoiceData valueForKey:@"image"];
            
            imgPetVw.clipsToBounds=YES;
            if (strPetImg.length>0) {
                [imgPetVw setImageWithURL:[NSURL URLWithString:strPetImg] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            }
            lblReview.text=[dictDetailedInvoiceData valueForKey:@"Review"];
            if (!lblReview.text.length>0) {
                lblReview.text=@"No Review Posted";
            }
            if ([self isNotNull:[dictDetailedInvoiceData valueForKey:@"ratingavg"]]) {
                int avgRating=[[dictDetailedInvoiceData valueForKey:@"ratingavg"]intValue];
                switch (avgRating) {
                    case 0:
                    {
                        break;
                    }
                    case 1:
                    {
                        [imgStar1 setImage:[UIImage imageNamed:@"star_foucs"]];
                        break;
                    }
                    case 2:
                    {
                        [imgStar1 setImage:[UIImage imageNamed:@"star_foucs"]];
                        [imgStar2 setImage:[UIImage imageNamed:@"star_foucs"]];
                        break;
                    }
                    case 3:
                    {
                        [imgStar1 setImage:[UIImage imageNamed:@"star_foucs"]];
                        [imgStar2 setImage:[UIImage imageNamed:@"star_foucs"]];
                        [imgStar3 setImage:[UIImage imageNamed:@"star_foucs"]];
                        break;
                    }
                    case 4:
                    {
                        [imgStar1 setImage:[UIImage imageNamed:@"star_foucs"]];
                        [imgStar2 setImage:[UIImage imageNamed:@"star_foucs"]];
                        [imgStar3 setImage:[UIImage imageNamed:@"star_foucs"]];
                        [imgStar4 setImage:[UIImage imageNamed:@"star_foucs"]];
                        break;
                    }
                    case 5:
                    {
                        [imgStar1 setImage:[UIImage imageNamed:@"star_foucs"]];
                        [imgStar2 setImage:[UIImage imageNamed:@"star_foucs"]];
                        [imgStar3 setImage:[UIImage imageNamed:@"star_foucs"]];
                        [imgStar4 setImage:[UIImage imageNamed:@"star_foucs"]];
                        [imgStar5 setImage:[UIImage imageNamed:@"star_foucs"]];
                        break;
                    }
                        
                    default:
                        break;
                }
                
            }
            
        
        }
        else if ([operation.response statusCode]==206) {
            
            if ([[responseDict valueForKey:@"replyCode"] isEqualToString:@"error"]) {
                UIAlertController *alrtCntroller=[UIAlertController alertControllerWithTitle:K_APP_NAME message:[NSString stringWithFormat:@"%@",[responseDict valueForKey:@"replyMsg"]] preferredStyle:UIAlertControllerStyleAlert];
                [alrtCntroller addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self.navigationController popViewControllerAnimated:YES];
                    
                }]];
                [self presentViewController:alrtCntroller animated:YES completion:nil];
            }
            else
            {
                            CGRect thisRect = [UIScreen mainScreen].bounds;
                
                            //modify required frame parameter (.origin.x/y, .size.width/height)
                            thisRect.origin.y = 0;
                            thisRect.size.width=[UIScreen mainScreen].bounds.size.width;
                            [self.navigationItem setTitle:@"Transaction"];
                            [vwTransaction setFrame:thisRect];
                            [self.view addSubview:vwTransaction];
                            [self.view bringSubviewToFront:vwButtons];
            }
            
            



        }
        else
        {
                        [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
                    }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{

                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self getInvoiceDetailData];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                      }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
