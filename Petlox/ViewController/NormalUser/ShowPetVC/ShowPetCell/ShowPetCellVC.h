//
//  ShowPetCellVC.h
//  Petlox
//
//  Created by Bharat Kumar Pathak on 21/07/16.
//  Copyright © 2016 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowPetCellVC : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *petImageView;
@property (weak, nonatomic) IBOutlet UILabel *lblPetName;
@end
