//
//  MessageInboxVC.m
//  Petlox
//
//  Created by Sumit Sharma on 15/12/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "MessageInboxVC.h"
#import "MsgInboxCustomeCell.h"
#import "WriteMsgVC.h"
#import "MessageBusinessListVC.h"
@interface MessageInboxVC ()<UITableViewDataSource,UITableViewDelegate>
{
    UIRefreshControl *refreshControl;
    IBOutlet UITableView *tblVw;
    IBOutlet UILabel *lblMsgError;
    NSMutableArray *arrInboxMsg;
    NSMutableArray *arrSelectedIndex;
    BOOL isTableViewEditing;
    UIBarButtonItem *rightDltBtn;
    UIButton *btn1;
}
@end

@implementation MessageInboxVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    wbServiceCount=1;
    
    refreshControl = [[UIRefreshControl alloc]init];
    [tblVw addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    // Do any additional setup after loading the view from its nib.
}

- (void)refreshTable {
    //TODO: refresh your data
    NSLog(@"fsdafhsadlfjsal;kfj;j");
    
    if ([CommonFunctions reachabiltyCheck]) {
        [self getAllMessagesRefreshControl];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection"];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController.navigationBar setHidden:NO];
    self.navigationController.navigationBarHidden=NO;
    
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
            [self getAllMessages];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection"];
    }
}
-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Messages"];
    
    arrSelectedIndex=[[NSMutableArray alloc]init];
    
    UIBarButtonItem *backButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"menu-icon"] style:UIBarButtonItemStylePlain target:self.viewDeckController action:@selector(toggleLeftView)];
    [self.navigationItem setLeftBarButtonItem:backButton];
    
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:UD_BUSINESS_TYPE]isEqualToString:@"user"]) {
        UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(90.0f, 0.0f, 60.0f, 30.0f)];

        
        btn1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [btn1 setFrame:CGRectMake(0.0f, 0.0f, 23.0f, 24.0f)];
        [btn1 setImage:[UIImage imageNamed:@"imgDeleteMsgIcon"] forState:UIControlStateNormal];
        [btn1 addTarget:self action:@selector(deleteMessageBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [customView addSubview:btn1];
        
        UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [btn2 setFrame:CGRectMake(35.0f, 0.0f, 23.0f, 24.0f)];
        [btn2 setImage:[UIImage imageNamed:@"imgWriteMsgIcon"] forState:UIControlStateNormal];
        [btn2 addTarget:self action:@selector(writeMsgBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [customView addSubview:btn2];
        
        UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] initWithCustomView:customView];
        [self.navigationItem setRightBarButtonItem:rightBtn];

    }
    else
    {
        
        rightDltBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"imgDeleteMsgIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(deleteMessageBtnClicked:)];

        [self.navigationItem setRightBarButtonItem:rightDltBtn];
    }
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBACtionButton Methods
-(IBAction)writeMsgBtnClicked:(id)sender
{
    MessageBusinessListVC *mblvc=[[MessageBusinessListVC alloc]init];
    [self.navigationController pushViewController:mblvc animated:YES];
}

-(IBAction)deleteMessageBtnClicked:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    NSLog(@"%ld",(long)btn.tag);
    if (btn.tag==0) {
        
        [arrSelectedIndex removeAllObjects];
        [tblVw setEditing:YES];
        tblVw.allowsMultipleSelectionDuringEditing = true;
        isTableViewEditing=YES;
        [tblVw reloadData];
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:UD_BUSINESS_TYPE]isEqualToString:@"user"]) {
                    [btn1 setImage:[UIImage imageNamed:@"imgCheckMark"] forState:UIControlStateNormal];
   btn1.tag=1;
        }
        else
        {
            rightDltBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"imgCheckMark"] style:UIBarButtonItemStylePlain target:self action:@selector(deleteMessageBtnClicked:)];
            
            [self.navigationItem setRightBarButtonItem:rightDltBtn];
            [arrInboxMsg removeObjectsInArray:arrSelectedIndex];
            [tblVw reloadData];
            rightDltBtn.tag=1;

        }

     
    }
    else if (btn.tag==1)
    {
        [tblVw setEditing:NO];
        tblVw.allowsMultipleSelectionDuringEditing = NO;
        isTableViewEditing=NO;
        [tblVw reloadData];
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:UD_BUSINESS_TYPE]isEqualToString:@"user"]) {
            [btn1 setImage:[UIImage imageNamed:@"imgDeleteMsgIcon"] forState:UIControlStateNormal];
            btn1.tag=0;

        }
        else
        {

            rightDltBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"imgDeleteMsgIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(deleteMessageBtnClicked:)];
            
            [self.navigationItem setRightBarButtonItem:rightDltBtn];
            rightDltBtn.tag=0;
            
        }
        
                if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
                    [self removeMessages];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please check your internet connection."];
        }

    }
}

#pragma mark - UITableView Delegate Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrInboxMsg.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (105.0/667.0)*K_SCREEN_HEIGHT;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"MsgInboxCustomeCell";
    MsgInboxCustomeCell *cell=[[MsgInboxCustomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    
    NSString *strBusinessImg=[[arrInboxMsg objectAtIndex:indexPath.row] valueForKey:@"image"];
    if (strBusinessImg.length>0) {
//        cell.imgVwProfileImg.layer.cornerRadius=cell.imgVwProfileImg.frame.size.height/2;
        cell.imgVwProfileImg.clipsToBounds=YES;
        cell.imgVwProfileImg.layer.borderColor=[UIColor whiteColor].CGColor;
        cell.imgVwProfileImg.layer.borderWidth=2.0f;
        [cell.imgVwProfileImg setImageWithURL:[NSURL URLWithString:strBusinessImg] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    else
    {
        [cell.imgVwProfileImg setImage:[UIImage imageNamed:@"DefaultProfileImg"]];
    }
    if ([self isNotNull:[[arrInboxMsg objectAtIndex:indexPath.row] objectForKey:@"business_name"]]) {
    cell.lblBusinessName.text=[[arrInboxMsg objectAtIndex:indexPath.row] objectForKey:@"business_name"];
    }
    else
    {
        cell.lblBusinessName.text=@"";
    }

    if (!(cell.lblBusinessName.text.length>0)) {
    cell.lblBusinessName.text=[NSString stringWithFormat:@"%@ %@",[[arrInboxMsg objectAtIndex:indexPath.row] objectForKey:@"first_name"],[[arrInboxMsg objectAtIndex:indexPath.row] objectForKey:@"last_name"]];
    }
    
    NSData *nsdataFromBase64String = [[NSData alloc]
                                      initWithBase64EncodedString:[[arrInboxMsg objectAtIndex:indexPath.row]objectForKey:@"message"] options:0];
    
    // Decoded NSString from the NSData
    NSString *base64Decoded = [[NSString alloc]
                               initWithData:nsdataFromBase64String encoding:NSUTF8StringEncoding];
    
    cell.lblReviewDiscription.text=base64Decoded;
    
    //cell.lblTime.text=[CommonFunctions convertTimeStampToDate:[[arrInboxMsg objectAtIndex:indexPath.row]objectForKey:@"created"]];
    
    cell.lblTime.text = [NSDate prettyTimestampSinceDate:[NSDate dateWithTimeIntervalSince1970:[[[arrInboxMsg objectAtIndex:indexPath.row]objectForKey:@"created"]doubleValue]]];
    
  //  cell.lblTime.text=[[arrInboxMsg objectAtIndex:indexPath.row]objectForKey:@"created"];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isTableViewEditing) {
        [arrSelectedIndex addObject:[[arrInboxMsg objectAtIndex:indexPath.row] objectForKey:@"user_id"]];
            NSLog(@"After Adding %@",arrSelectedIndex);
    }
    else{
            WriteMsgVC *wmvc=[[WriteMsgVC alloc]init];
            wmvc.dataDict=[arrInboxMsg objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:wmvc animated:YES];
        
    }
   // NSLog(@"%ld",(long)indexPath.row);
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
     [arrSelectedIndex removeObject:[[arrInboxMsg objectAtIndex:indexPath.row]objectForKey:@"user_id"]];

    
    NSLog(@"After Removing %@",arrSelectedIndex);
}
-(UITableViewCellEditingStyle)tableView: (UITableView *)tableView editingStyleForRowAtIndexPath: (NSIndexPath *)indexPath
{
    [tableView setValue:[UIColor colorWithRed:(181.0/225.0) green:(37.0/225.0) blue:(86.0/225.0) alpha:1.0] forKey:@"multiselectCheckmarkColor"];
    NSLog(@"%ld",(long)indexPath.row);
    //Do needed stuff here. Like removing values from stored NSMutableArray or UITableView datasource
    return YES;
}

#pragma mark - WebService API
-(void)getAllMessages
{
    NSMutableDictionary *param;
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"user_id", nil];
    
    NSString *url = [NSString stringWithFormat:@"inboxMessages"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/userReview
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            lblMsgError.text=@"";
            arrInboxMsg=[responseDict objectForKey:@"message"];
            [tblVw reloadData];
            
        }
        else if([operation.response statusCode]  == 206 ){
           // NSLog(@"impo response %@",operation.response);
            lblMsgError.text=@"No messages found";
            [arrInboxMsg removeAllObjects];
            [tblVw reloadData];
            
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{

                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self getAllMessages];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}

-(void)getAllMessagesRefreshControl
{
    NSMutableDictionary *param;
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"user_id", nil];
    
    NSString *url = [NSString stringWithFormat:@"inboxMessages"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/userReview
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [refreshControl endRefreshing];
      

        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            lblMsgError.text=@"";
            arrInboxMsg=[responseDict objectForKey:@"message"];
            [tblVw reloadData];
            
        }
        else if([operation.response statusCode]  == 206 ){
            // NSLog(@"impo response %@",operation.response);
            lblMsgError.text=@"No messages found";
            [arrInboxMsg removeAllObjects];
            [tblVw reloadData];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                                    [refreshControl endRefreshing];
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self getAllMessages];
                                              }
                                              else
                                              {      [refreshControl endRefreshing];
                                                  [CommonFunctions removeActivityIndicator];
                                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}

-(void)removeMessages
{
    NSMutableDictionary *param;
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"user_id",arrSelectedIndex,@"businessInfo", nil];
    
    NSString *url = [NSString stringWithFormat:@"deleteAllMessage"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/userReview
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
           // lblMsgError.text=@"";
            [self getAllMessages];
            //[tblVw reloadData];
            
        }
        else if([operation.response statusCode]  == 206 ){
            // NSLog(@"impo response %@",operation.response);
            lblMsgError.text=@"No messages found";
            
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{

                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self removeMessages];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
