//
//  EditProfileVC.m
//  Petlox
//
//  Created by Sumit Sharma on 20/11/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "UserEditProfileVC.h"

@interface UserEditProfileVC ()<UIActionSheetDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    IBOutlet UIImageView *imgVwProfile;
    IBOutlet UIScrollView *scrllVw;
    IBOutlet UITextField *txtLastName;
    IBOutlet UITextField *txtFrstName;
    IBOutlet UITextField *txtBusinessName;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtPhnNumbre;
    IBOutlet UITextField *txtAddress;
    IBOutlet UIView *vwBackgrd;
    IBOutlet UIView *vwBckGrndImg;
    IBOutlet UIView *vwTxtFld1;
    IBOutlet UIView *vwTxtFld2;
    IBOutlet UIView *vwTxtFld3;
    IBOutlet UIView *vwTxtFld4;
    IBOutlet UIView *vwTxtFld5;
    IBOutlet UIView *vwTxtFld6;
    UITextField *activeTxtFld;
    
    IBOutlet UIButton *btnSave;
}
@end

@implementation UserEditProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUpView];
    wbServiceCount=1;
}


-(void)setUpView
{
    
      [btnSave.titleLabel setFont:[UIFont fontWithName:btnSave.titleLabel.font.fontName size:btnSave.titleLabel.font.pointSize*SCREEN_XScale]];
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Edit Profile"];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self.viewDeckController action:@selector(toggleLeftViewAnimated:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
//    [btn addTarget:self.viewDeckController action:@selector(toggleLeftViewAnimated:) forControlEvents:UIControlEventTouchUpInside];
    
    [vwBackgrd setBackgroundColor:[UIColor colorWithRed:0.0f/255.0f green:172.0f/255.0f blue:238.0f/255.0f alpha:1.0]];
    
    [txtAddress setValue:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1]
              forKeyPath:@"_placeholderLabel.textColor"];
    [txtBusinessName setValue:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1]
              forKeyPath:@"_placeholderLabel.textColor"];
    [txtEmail setValue:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1]
              forKeyPath:@"_placeholderLabel.textColor"];
    [txtFrstName setValue:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1]
              forKeyPath:@"_placeholderLabel.textColor"];
    [txtLastName setValue:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1]
              forKeyPath:@"_placeholderLabel.textColor"];
    [txtPhnNumbre setValue:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1]
                                                                       forKeyPath:@"_placeholderLabel.textColor"];
    
//    vwBckGrndImg.layer.cornerRadius=vwBckGrndImg.frame.size.height/2;
//    imgVwProfile.layer.cornerRadius=imgVwProfile.frame.size.height/2;
    vwBckGrndImg.clipsToBounds=YES;
    imgVwProfile.clipsToBounds=YES;
//    imgVwProfile.layer.borderWidth=2.0f;
//    imgVwProfile.layer.borderColor=[UIColor colorWithRed:148.0f/255.0f green:208.0f/255.0f blue:245.0f/255.0f alpha:1.0f].CGColor;
//    
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getProfileData];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTapClicked)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];

}

-(void)singleTapClicked
{
    [self.view endEditing:YES];
    [self scrollToNormalView];
}

-(void)viewDidLayoutSubviews
{

    btnSave.layer.cornerRadius = btnSave.frame.size.width/2;
    if ([UIScreen mainScreen].bounds.size.height<568) {
        [scrllVw setScrollEnabled:YES];
        [scrllVw setContentSize:CGSizeMake(320.0f, 568.0f)];
    }

}
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIScrollView Methods
-(void)scrollViewToCenterOfScreen:(UITextField *)textField cntainter:(UIView *)view
{
    [scrllVw setScrollEnabled:YES];
    float difference;
    if (scrllVw.contentSize.height == 600)
        difference = 50.0f;
    else
        difference = 50.0f;
    CGFloat viewCenterY = textField.center.y+view.frame.origin.y;
    NSLog(@"center.y -- %f",textField.center.y);
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    CGFloat avaliableHeight = applicationFrame.size.height - 5.0f;
    NSLog(@"avaliableHeight---%f",applicationFrame.size.height);
    
    CGFloat y = viewCenterY - avaliableHeight / 10.0f;
    if (y < 0)
        y = 0;
    NSLog(@"%f",y);
    [scrllVw setContentOffset:CGPointMake(0, y) animated:YES];
    [scrllVw setContentSize:CGSizeMake(320.0f, 630.0f)];
}

-(void)scrollToNormalView
{
    [scrllVw setContentOffset:CGPointZero];
    [scrllVw setScrollEnabled:NO];
}


#pragma mak - UITextField Validation 

-(BOOL)isTextFieldValidation
{
    if (![CommonFunctions isValueNotEmpty:txtFrstName.text]) {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter first name."];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtLastName.text]) {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter last name."];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtEmail.text]) {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter email address."];
        return NO;
    }
    else if (![CommonFunctions IsValidEmail:txtEmail.text]) {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter valid email."];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtPhnNumbre.text]) {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter phone number."];
        return NO;
    }
    else
    {
    return YES;
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==txtFrstName) {
        [txtLastName becomeFirstResponder];
    }
    else if (textField==txtLastName)
    {
        [txtEmail becomeFirstResponder];
    }
    else if (textField==txtEmail)
    {
        [txtPhnNumbre becomeFirstResponder];
    }
    else if (textField==txtPhnNumbre)
    {
        [txtPhnNumbre resignFirstResponder];
        [self scrollToNormalView];
    }

    return YES;
}


#pragma mark - IBAction Methods
- (IBAction)saveButtonClicked:(id)sender {
    [self.view endEditing:YES];
    [self scrollToNormalView];
    if ([self isTextFieldValidation]) {
        if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self editUserRegisterDetail];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
        }
    }
}
- (IBAction)imgPickerBtnClicked:(id)sender
{
    UIActionSheet *actnSheet=[[UIActionSheet alloc]initWithTitle:@"Choose Image" delegate:self  cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Image",@"Choose From Gallery", nil] ;
    [actnSheet showInView:self.view];
}

#pragma mark - UITextField Delegate Methods
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeTxtFld=textField;

    if (txtFrstName==textField) {
        [self scrollViewToCenterOfScreen:textField cntainter:vwTxtFld1];
    }
    else if (txtLastName==textField) {
        [self scrollViewToCenterOfScreen:textField cntainter:vwTxtFld2];
    }
    else if (txtBusinessName==textField) {
        [self scrollViewToCenterOfScreen:textField cntainter:vwTxtFld3];
    }
    else if (txtEmail==textField) {
        [self scrollViewToCenterOfScreen:textField cntainter:vwTxtFld4];
    }
    else if (txtPhnNumbre==textField) {
        [self scrollViewToCenterOfScreen:textField cntainter:vwTxtFld5];
    }
    else if (txtAddress==textField) {
        [self scrollViewToCenterOfScreen:textField cntainter:vwTxtFld6];
    }
}

#pragma mark - UINavigation Controller Delegate Method
-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    navigationController.navigationBar.backgroundColor=[UIColor colorWithRed:71.0f/255.0f green:13.0f/255.0f blue:52.0f/255.0f alpha:1];
    
    UIView *statusView=[[UIView alloc]initWithFrame:CGRectMake(0, -[UIApplication sharedApplication].statusBarFrame.size.height,[UIApplication sharedApplication].statusBarFrame.size.width, [UIApplication sharedApplication].statusBarFrame.size.height)];
    [statusView setBackgroundColor:[UIColor colorWithRed:71.0f/255.0f green:13.0f/255.0f blue:52.0f/255.0f alpha:1]];
    [navigationController.navigationBar addSubview:statusView];
    [navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:FONT_GULIM size:18.85f]}];
    
    viewController.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    
}


#pragma mark - UIActionSheet Delegate Method
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        UIImagePickerController *imgPicker=[[UIImagePickerController alloc]init];
        imgPicker.delegate=self;
        imgPicker.sourceType=UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imgPicker animated:YES completion:nil];
    }
    else if (buttonIndex==1)
    {
        UIImagePickerController *imgPicker=[[UIImagePickerController alloc]init];
        imgPicker.delegate=self;
        imgPicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:imgPicker animated:YES completion:nil];
        
    }
}
#pragma mark - UIImagePickerCntroller Delegate Methods
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *imageEdited=[self scaleAndRotateImage:[info objectForKey:UIImagePickerControllerOriginalImage]];
    
    NSData *imgData1 = UIImageJPEGRepresentation(imageEdited, 1.0f);
    NSLog(@"1.0 size: %d", imgData1.length);
    
    NSData *imgData2 = UIImageJPEGRepresentation(imageEdited, 0.7f);
    NSLog(@"0.7 size: %d", imgData2.length);
    
    NSData *imgData3 = UIImageJPEGRepresentation(imageEdited, 0.4f);
    NSLog(@"0.4 size: %d", imgData3.length);
    
    NSData *imgData4 = UIImageJPEGRepresentation(imageEdited, 0.0f);
    NSLog(@"0.0 size: %d", imgData4.length);
    
    // Don't convert NSData back to UIImage before writing to disk
    imgVwProfile.image=[UIImage imageWithData:imgData4];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(UIImage *)scaleAndRotateImage:(UIImage *)image
{
    int kMaxResolution = 1242; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}



#pragma mark - WebService API

-(void)getProfileData
{
    NSString *url = [NSString stringWithFormat:@"myProfile"];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"user_id", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/myProfile
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {

                        txtPhnNumbre.text=[responseDict valueForKey:@"phone"];
                        txtLastName.text=[responseDict valueForKey:@"last_name"];
                        txtFrstName.text=[responseDict valueForKey:@"first_name"];
                        txtEmail.text=[responseDict valueForKey:@"email"];
                        txtBusinessName.text=[responseDict valueForKey:@"business"];
                        txtAddress.text=[responseDict valueForKey:@"address"];
            
            NSString *imgStr=[responseDict valueForKey:@"image"];
            if (imgStr.length>0) {
                [imgVwProfile setImageWithURL:[NSURL URLWithString:imgStr] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            }
            

        }
        else
        {
            
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self getProfileData];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          
                                      }];
}

-(void)editUserRegisterDetail
{
    
    NSMutableDictionary *param;
    NSString *url;
    if (pushDeviceToken.length==0)
    {
        pushDeviceToken=[UserDefaults objectForKey:@"deviceToken"];
    }
    if ([[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID])
    {
        param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"sid",[CommonFunctions trimSpaceInString:txtFrstName.text],@"first_name",[CommonFunctions trimSpaceInString:txtLastName.text],@"last_name",[CommonFunctions trimSpaceInString:txtEmail.text],@"email",[CommonFunctions trimSpaceInString:txtPhnNumbre.text],@"phone",@"iphone",@"device_type",[CommonFunctions trimSpaceInString:txtBusinessName.text],@"business_name",[CommonFunctions trimSpaceInString:txtAddress.text],@"address", nil];
    }
    
    NSLog(@"%@",param);
    
    
    url = [NSString stringWithFormat:@"%@editProfile",serverURL];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:url]];
    
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID] forHTTPHeaderField:@"token"];
    
    AFHTTPRequestOperation *op = [manager POST:@"" parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if (imgVwProfile.image != nil)
        {
            NSData *imageData = UIImageJPEGRepresentation(imgVwProfile.image, 0.8f);
            
            [formData appendPartWithFileData:imageData name:@"image" fileName:@"ProfileImage.jpg" mimeType:@"image/jpeg"];
        }
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        
        if (responseObject != nil)
        {
            NSLog(@"Profile  responseObject error: %@",error);
            
            NSDictionary *jsonDic = (NSDictionary *)responseObject;
            NSLog(@"Profile  JSON: %@",jsonDic);
            
            if (jsonDic != nil)
            {
                if ([[jsonDic objectForKey:@"replyCode"] isEqualToString:@"success"])
                {
                    [[NSUserDefaults standardUserDefaults]setObject:[jsonDic objectForKey:@"image"] forKey:UD_USER_IMG];
                    [[NSUserDefaults standardUserDefaults]setObject:[jsonDic objectForKey:@"first_name"] forKey:UD_FIRST_NAME];
                    [[NSUserDefaults standardUserDefaults]setObject:[jsonDic objectForKey:@"last_name"] forKey:UD_LAST_NAME];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    
                    
                    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:UD_USER_IMG]);
                    [CommonFunctions alertTitle:@"" withMessage:@"Your profile updated successfully."];
                    
                }
                else
                {
                    [CommonFunctions alertTitle:@"" withMessage:[jsonDic objectForKey:@"replyMsg"]];
                }
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[jsonDic objectForKey:@"replyMsg"]];
            }
        }
        [CommonFunctions removeActivityIndicator];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
        [CommonFunctions removeActivityIndicator];
        
    }];
    [op start];
}
/*
#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
