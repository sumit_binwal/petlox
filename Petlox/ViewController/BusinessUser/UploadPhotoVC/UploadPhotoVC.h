//
//  UploadPhotoVC.h
//  Petlox
//
//  Created by Sumit Sharma on 03/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UploadPhotoVC : UIViewController<UIGestureRecognizerDelegate>
{
    BOOL isForDelete;
    BOOL longPressEffect;
    
    UIBarButtonItem *btn2;
    
    NSMutableArray *arrObjectsDelete;
}
@end
