//
//  BillingWeekCustomeCell.m
//  Petlox
//
//  Created by Sumit Sharma on 09/11/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "BillingHistoryCustomeCell.h"

@implementation BillingHistoryCustomeCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    return self=[[[NSBundle mainBundle]loadNibNamed:@"BillingHistoryCustomeCell" owner:self options:nil] objectAtIndex:0];
}
- (void)awakeFromNib {
    // Initialization code
    [_lblInvoiceTitle setFont:[UIFont fontWithName:_lblInvoiceTitle.font.fontName size:_lblInvoiceTitle.font.pointSize*SCREEN_XScale]];
    [_lblInvoiceAmount setFont:[UIFont fontWithName:_lblInvoiceAmount.font.fontName size:_lblInvoiceAmount.font.pointSize*SCREEN_XScale]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
