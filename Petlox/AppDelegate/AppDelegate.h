//
//  AppDelegate.h
//  Petlox
//
//  Created by Sumit Sharma on 21/07/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//
#import "IIViewDeckController.h"

#import <dispatch/dispatch.h>


#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>

#import <Crittercism/Crittercism.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,IIViewDeckControllerDelegate,CLLocationManagerDelegate, UITabBarDelegate>
{
      CLLocation *currentLocation;
}
@property (strong, nonatomic)IBOutlet UIWindow *window;
@property (strong, nonatomic) UINavigationController *navController;
@property (retain, nonatomic) UIViewController *centerController;
@property (retain, nonatomic) UIViewController *leftController;
@property (retain, nonatomic) UIView *bottomBar;
@property (retain, nonatomic) UITabBar *petReservationTabBar;


@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) CLLocationManager             *locationManager;
@property (nonatomic, assign) CLLocationCoordinate2D currentLocationCoordinate;


- (IIViewDeckController*)generateControllerStack;
- (IIViewDeckController*)generateBusinessControllerStack;

-(void)UpdateUserDataWithCompletetion:(void (^)(NSArray *data))completion WithFailure:(void (^)(NSString *error))failure;
-(void)showPetReservationTabBar:(BOOL)shouldShow;
-(void)createNormalUserPetReserveTabBar;
@property (retain, nonatomic) NSMutableDictionary *K_dictUserDdetails;
@end

