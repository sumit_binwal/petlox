//
//  ReservationVC.m
//  Petlox
//
//  Created by Sumit Sharma on 07/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "MyReservationVC.h"
#import "SignInVC.h"
#import "MyReservationCustomeCell.h"
#import "IIViewDeckController.h"
#import "CustomerInfoVC.h"
#import "MapPathVC.h"
#import "BillingWeekHistoryVC.h"
#import "GraphReservationVC.h"
#import "MapVC.h"
#import "MyReservationDetailVC.h"

@interface MyReservationVC ()<UITableViewDataSource,UITableViewDelegate>
{
    
    NSMutableArray *arrSelectedIndex;
    NSMutableArray *arrIndexSelectionSelectedIndex;
    IBOutlet UIView *tabBarView;
    IBOutlet UIButton *btnCompleted;
    BOOL isTableViewEditing;
    
    IBOutlet UIButton *btnAccepted;
    IBOutlet UIButton *btnNew;
    IBOutlet UITableView *tblVw;
    NSMutableArray *arrReserveUser;
    IBOutlet UILabel *lblMsgError;
    NSString *selectedTabIndex;
    NSArray *arrUserSectionTitle;
    NSMutableDictionary *users;
    UIBarButtonItem *rightButton;
}
@end

@implementation MyReservationVC

- (void)viewDidLoad {
    [super viewDidLoad];
     wbServiceCount=1;
    [self setUpView];
    // Do any additional setup after loading the view from its nib.
}
-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    
    NSLog(@"%f",[[UIScreen mainScreen] bounds].size.height-tabBarView.frame.size.height);
    NSLog(@"%f",tabBarView.frame.size.height);
        arrSelectedIndex=[[NSMutableArray alloc]init];
    arrIndexSelectionSelectedIndex=[[NSMutableArray alloc]init];
    [tabBarView setFrame:CGRectMake(0,([[UIScreen mainScreen] bounds].size.height-(tabBarView.frame.size.height+65)), [[UIScreen mainScreen] bounds].size.width, 60)];
    [self.view addSubview:tabBarView];
    selectedTabIndex=@"0";
    /*
    [btnNew setBackgroundColor:[UIColor colorWithRed:110.0f/255.0f green:200.0f/255.0f blue:253.0f/255.0f alpha:1]];
    [btnNew setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [btnAccepted setBackgroundColor:[UIColor colorWithRed:152.0f/255.0f green:217.0f/255.0f blue:255.0f/255.0f alpha:1]];
    [btnAccepted setTitleColor:[UIColor colorWithRed:0.0f/255.0f green:172.0f/255.0f blue:238.0f/255.0f alpha:1] forState:UIControlStateNormal];
    [btnCompleted setBackgroundColor:[UIColor colorWithRed:152.0f/255.0f green:217.0f/255.0f blue:255.0f/255.0f alpha:1]];
    [btnCompleted setTitleColor:[UIColor colorWithRed:0.0f/255.0f green:172.0f/255.0f blue:238.0f/255.0f alpha:1] forState:UIControlStateNormal];
*/
    /*
    [btnNew setImage:[UIImage imageNamed:@"newadd_foucs"] forState:UIControlStateNormal];
    [btnNew setImage:[UIImage imageNamed:@"newadd_unfoucs"] forState:UIControlStateNormal];
    
    [btnAccepted setImage:[UIImage imageNamed:@"accepted_foucs"] forState:UIControlStateNormal];
    [btnAccepted setImage:[UIImage imageNamed:@"accepted_unfoucs"] forState:UIControlStateNormal];
    
    [btnCompleted setImage:[UIImage imageNamed:@"archived_foucs"] forState:UIControlStateNormal];
    [btnCompleted setImage:[UIImage imageNamed:@"archived_unfoucs"] forState:UIControlStateNormal];
    */
    
    UIBarButtonItem *backButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"menu-icon"] style:UIBarButtonItemStylePlain target:self.viewDeckController action:@selector(toggleLeftView)];
    [self.navigationItem setLeftBarButtonItem:backButton];

    [self.navigationItem setTitle:@"My Reservations"];
    arrReserveUser=[[NSMutableArray alloc]init];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getMyReservationList];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - IBAction Button Clicked
- (IBAction)reservationGraphBtnClicked:(id)sender
{
    GraphReservationVC *grvc=[[GraphReservationVC alloc]init];
    [self.navigationController pushViewController:grvc animated:YES];
}

-(IBAction)directionBtnClicked:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    MapVC *mvc=[[MapVC alloc]init];
    mvc.dictUserData=[arrReserveUser objectAtIndex:btn.tag];
    [self.navigationController pushViewController:mvc animated:YES];
}

- (IBAction)billingHistoryBtnClicked:(id)sender {
    BillingWeekHistoryVC *bwhvc=[[BillingWeekHistoryVC alloc]initWithNibName:@"BillingWeekHistoryVC" bundle:nil];
    [self.navigationController pushViewController:bwhvc animated:YES];
}

- (IBAction)newRsrvationBtnClicked:(id)sender {
   
    UIButton *btn=(UIButton *)sender;
    int btnTag=btn.tag;
    
//    [btnAccepted setBackgroundColor:[UIColor colorWithRed:152.0f/255.0f green:217.0f/255.0f blue:255.0f/255.0f alpha:1]];
//    [btnAccepted setTitleColor:[UIColor colorWithRed:0.0f/255.0f green:172.0f/255.0f blue:238.0f/255.0f alpha:1] forState:UIControlStateNormal];
//    [btnCompleted setBackgroundColor:[UIColor colorWithRed:152.0f/255.0f green:217.0f/255.0f blue:255.0f/255.0f alpha:1]];
//    [btnCompleted setTitleColor:[UIColor colorWithRed:0.0f/255.0f green:172.0f/255.0f blue:238.0f/255.0f alpha:1] forState:UIControlStateNormal];
//    [btnNew setBackgroundColor:[UIColor colorWithRed:152.0f/255.0f green:217.0f/255.0f blue:255.0f/255.0f alpha:1]];
//    [btnNew setTitleColor:[UIColor colorWithRed:0.0f/255.0f green:172.0f/255.0f blue:238.0f/255.0f alpha:1] forState:UIControlStateNormal];
    
 self.navigationItem.rightBarButtonItem = nil;
    rightButton.tag=0;
    [tblVw setEditing:NO];
    tblVw.allowsMultipleSelectionDuringEditing = NO;
    isTableViewEditing=NO;
    [tblVw reloadData];
    
    switch (btnTag) {
        case 0:
        {
            [self.navigationItem setTitle:@"My Reservations"];

            selectedTabIndex=@"0";
//            [btn setBackgroundColor:[UIColor colorWithRed:110.0f/255.0f green:200.0f/255.0f blue:253.0f/255.0f alpha:1]];
//            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            [btnNew setImage:[UIImage imageNamed:@"newadd_foucs"] forState:UIControlStateNormal];
            [btnAccepted setImage:[UIImage imageNamed:@"accepted_unfoucs"] forState:UIControlStateNormal];
            [btnCompleted setImage:[UIImage imageNamed:@"archived_unfoucs"] forState:UIControlStateNormal];

            if ([CommonFunctions reachabiltyCheck]) {
                [CommonFunctions showActivityIndicatorWithText:@""];
                [self getMyReservationList];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
            }
            
            break;
        }
        case 1:
        {
            [self.navigationItem setTitle:@"My Reservations"];
                        selectedTabIndex=@"1";
//            [btn setBackgroundColor:[UIColor colorWithRed:110.0f/255.0f green:200.0f/255.0f blue:253.0f/255.0f alpha:1]];
//            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            [btnNew setImage:[UIImage imageNamed:@"newadd_unfoucs"] forState:UIControlStateNormal];
            [btnAccepted setImage:[UIImage imageNamed:@"accepted_foucs"] forState:UIControlStateNormal];
            [btnCompleted setImage:[UIImage imageNamed:@"archived_unfoucs"] forState:UIControlStateNormal];
            
            if ([CommonFunctions reachabiltyCheck]) {
                [CommonFunctions showActivityIndicatorWithText:@""];
                [self getMyReservationList];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
            }
            break;
        }
        case 2:
            
        {
            rightButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"imgDeleteMsgIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(deleteBtnClicked:)];
            [self.navigationItem setRightBarButtonItem:rightButton];

            selectedTabIndex=@"2";
            [self.navigationItem setTitle:@"My Reservations"];
//            [btn setBackgroundColor:[UIColor colorWithRed:110.0f/255.0f green:200.0f/255.0f blue:253.0f/255.0f alpha:1]];
//            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            [btnNew setImage:[UIImage imageNamed:@"newadd_unfoucs"] forState:UIControlStateNormal];
            [btnAccepted setImage:[UIImage imageNamed:@"accepted_unfoucs"] forState:UIControlStateNormal];
            [btnCompleted setImage:[UIImage imageNamed:@"archived_foucs"] forState:UIControlStateNormal];
            
            if ([CommonFunctions reachabiltyCheck]) {
                [CommonFunctions showActivityIndicatorWithText:@""];
                [self getMyReservationList];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
            }
            break;
        }
        default:
            break;
    }
    
    }

-(IBAction)deleteBtnClicked:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    NSLog(@"%ld",(long)btn.tag);
    if (btn.tag==0) {
        
        [arrSelectedIndex removeAllObjects];
        [tblVw setEditing:YES];
        
        tblVw.allowsMultipleSelectionDuringEditing = YES;
        isTableViewEditing=YES;
        [tblVw reloadData];
        [rightButton setImage:[UIImage imageNamed:@"imgCheckMark"]];
        btn.tag=1;
    }
    else if (btn.tag==1)
    {
        [rightButton setImage:[UIImage imageNamed:@"imgDeleteMsgIcon"]];
        btn.tag=0;
        [tblVw setEditing:NO];
        tblVw.allowsMultipleSelectionDuringEditing = NO;
        NSLog(@"%@",arrIndexSelectionSelectedIndex);
        isTableViewEditing=NO;
        [tblVw reloadData];
        
        if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self deleteReservations];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
        }
    }
}


- (IBAction)cmpltedRservatinBtnClicked:(id)sender {
    
}
- (IBAction)logoutBtnClicked:(id)sender {
    SignInVC *sivc=[[SignInVC alloc]init];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_TOKEN_ID];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_BUSINESS_TYPE];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_BUSINESS_TYPE];
    [[NSUserDefaults standardUserDefaults]synchronize];
    APPDELEGATE.navController = [[UINavigationController alloc] initWithRootViewController:sivc];
    APPDELEGATE.window.rootViewController = APPDELEGATE.navController;
    [APPDELEGATE.window makeKeyAndVisible];
}


#pragma mark - WebService API
-(void)logoutUser
{
    
    NSString *url = [NSString stringWithFormat:@"logout"];

    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.173/petlox_svn/mobile/logout
    
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            SignInVC *sivc=[[SignInVC alloc]init];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_TOKEN_ID];
            [[NSUserDefaults standardUserDefaults]synchronize];
            APPDELEGATE.navController = [[UINavigationController alloc] initWithRootViewController:sivc];
            APPDELEGATE.window.rootViewController = APPDELEGATE.navController;
            [APPDELEGATE.window makeKeyAndVisible];
    }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self logoutUser];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}


-(void)getMyReservationList
{
    
    NSString *url = [NSString stringWithFormat:@"UserReservationList"];

        NSMutableDictionary *header=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"token", nil];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"user_id",selectedTabIndex,@"reservation_type",[NSString stringWithFormat:@"%f",CurrentLatitude],@"lat",[NSString stringWithFormat:@"%f",CurrentLongitude],@"lon", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/businessUserReservaionList
    

    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            lblMsgError.text=@"";
            arrReserveUser=[responseDict objectForKey:@"Reservation"];
//            NSLog(@"Data Aee -- %@",[[arrReserveUser valueForKey:@"21 Oct,2015"] objectAtIndex:0]);
//            NSMutableDictionary *dictTemp=[responseDict objectForKey:@"data"];
//            
//            // get all keys into array
//          //  NSArray * keys = [dictTemp allKeys];
//            
//            // sort it
//      //      NSArray * sorted_keys = [keys sortedArrayUsingSelector:@selector(compare:)];
//            
//            NSArray *aUnsorted = [dictTemp allKeys];
//            NSArray *arrKeys = [aUnsorted sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
//                NSDateFormatter *df = [[NSDateFormatter alloc] init];
//                [df setDateFormat:@"dd MMM,yyyy"];
//                NSDate *d1 = [df dateFromString:(NSString*) obj1];
//                NSDate *d2 = [df dateFromString:(NSString*) obj2];
//                return [d1 compare: d2];
//            }];
//            
//
//            
//            NSMutableDictionary *sortedValues = [[NSMutableDictionary alloc] init];
//          
//            sortedValues = [[NSMutableDictionary alloc] init];
//            for (int i=0;i<arrKeys.count;i++) {
//                NSString *key = [arrKeys objectAtIndex:i];
//                id your_value = [dictTemp objectForKey:key];
//                [sortedValues setObject:your_value forKey:key];
//            }
//            users=[responseDict objectForKey:@"data"];
//           // arrUserSectionTitle=[[[responseDict objectForKey:@"data"] allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
//            
//            
//            
//            NSSortDescriptor *sortOrder = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO selector:@selector(localizedCaseInsensitiveCompare:)];
//            arrUserSectionTitle = [[[responseDict objectForKey:@"data"] allKeys] sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortOrder]];
            [tblVw reloadData];
            
        }
        else
        {
            [arrReserveUser removeAllObjects];
            lblMsgError.text=@"No Reservation Found.";
            [users removeAllObjects];
            arrUserSectionTitle = nil;
            [tblVw reloadData];
            //[CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{

                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self getMyReservationList];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}

-(void)deleteReservations
{
    
    NSString *url = [NSString stringWithFormat:@"deleteReservation"];
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"token", nil];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:arrSelectedIndex,@"reservationInfo", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/businessUserReservaionList
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            [self getMyReservationList];
            
        }
        else
        {
            [arrReserveUser removeAllObjects];
            lblMsgError.text=@"No Reservation Found.";
            [users removeAllObjects];
            arrUserSectionTitle = nil;
            [tblVw reloadData];
            //[CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{

                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self deleteReservations];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}


#pragma mark- UITableView Delegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    NSString *sectionTitle = [arrUserSectionTitle objectAtIndex:section];
//    
//    NSLog(@"%@",arrReserveUser );
//    NSMutableArray *sectionDates = [users objectForKey:sectionTitle] ;
    return arrReserveUser.count;

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 142.0f;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"MyReservationCustomeCell";
    MyReservationCustomeCell *cell=[[MyReservationCustomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.btnDirection.tag = indexPath.row;
    [cell.btnDirection addTarget:self action:@selector(directionBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    

//    cell.imgBusinessProfileImg.layer.borderColor=[UIColor whiteColor].CGColor;
//    cell.imgBusinessProfileImg.layer.borderWidth=1;
//    cell.imgBusinessProfileImg.layer.cornerRadius=cell.imgBusinessProfileImg.frame.size.width/2;
    cell.imgBusinessProfileImg.clipsToBounds=YES;
//
//    cell.imgPetProfileImg.layer.borderColor=[UIColor whiteColor].CGColor;
//    cell.imgPetProfileImg.layer.borderWidth=1;
//    cell.imgPetProfileImg.layer.cornerRadius=5.0f;
    cell.imgPetProfileImg.clipsToBounds=YES;
    
    
    
    if ([selectedTabIndex isEqualToString:@"2"]) {
        [cell.lblTimeLeft setHidden:YES];
        [cell.lblDistance setHidden:YES];
        [cell.lblDirection setHidden:YES];
        [cell.lblTimeReaming setHidden:YES];
        [cell.lblIcon1 setHidden:YES];
        [cell.lblIcon2 setHidden:YES];
        [cell.btnDirection setHidden:YES];
    }
    else
    {
    cell.lblTimeLeft.text=[[arrReserveUser objectAtIndex:indexPath.row] valueForKey:@"interval"];
    cell.lblDistance.text=[[arrReserveUser objectAtIndex:indexPath.row] valueForKey:@"distance"];
    }
    if ([[[arrReserveUser objectAtIndex:indexPath.row]valueForKey:@"time_flag"]isEqualToString:@"Time left"] || [[[arrReserveUser objectAtIndex:indexPath.row]valueForKey:@"time_flag"]isEqualToString:@"In progress"]) {
        [cell.lblTimeLeft setHidden:YES];
    }
    
    NSLog(@"%@",[[arrReserveUser objectAtIndex:indexPath.row] valueForKey:@"time_flag"]);
    cell.lblTimeReaming.text=[[arrReserveUser objectAtIndex:indexPath.row] valueForKey:@"time_flag"];
    cell.lblBusinessName.text=[[arrReserveUser objectAtIndex:indexPath.row] valueForKey:@"business_name"];
    cell.lblBusinessAddress.text=[[arrReserveUser objectAtIndex:indexPath.row] valueForKey:@"address"];
    cell.lblPetname.text=[[arrReserveUser objectAtIndex:indexPath.row] valueForKey:@"pet_name"];

    cell.lblReserveDate.text=[[arrReserveUser objectAtIndex:indexPath.row] valueForKey:@"ReservationDate"];
    
    NSString *strBusinessImg=[[arrReserveUser objectAtIndex:indexPath.row] valueForKey:@"image"];
    if (strBusinessImg.length>0) {
    
    [cell.imgBusinessProfileImg setImageWithURL:[NSURL URLWithString:[[arrReserveUser objectAtIndex:indexPath.row] valueForKey:@"image"]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
}
    else
    {
        [cell.imgBusinessProfileImg setImage:[UIImage imageNamed:@"DefaultProfileImg"]];
    }
    
    
    NSString *strPetImg=[[arrReserveUser objectAtIndex:indexPath.row] valueForKey:@"pet_image"];
    if (strPetImg.length>0) {
    [cell.imgPetProfileImg setImageWithURL:[NSURL URLWithString:[[arrReserveUser objectAtIndex:indexPath.row] valueForKey:@"pet_image"]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    else
    {
        [cell.imgPetProfileImg setImage:[UIImage imageNamed:@"defaultPetImage"]];
    }
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    

    if (![selectedTabIndex isEqualToString:@"2"] || !isTableViewEditing) {
        
        if (![selectedTabIndex isEqualToString:@"2"])
        {
            MyReservationDetailVC *mrdvc=[[MyReservationDetailVC alloc]init];
            mrdvc.dictData=[arrReserveUser objectAtIndex:indexPath.row];
            mrdvc.strSelectedIndex=selectedTabIndex;
            [self.navigationController pushViewController:mrdvc animated:YES];
        }
        
    }
    
    else
    {
        [arrSelectedIndex addObject:[[arrReserveUser objectAtIndex:indexPath.row] objectForKey:@"id"]];

    [arrIndexSelectionSelectedIndex addObject:[NSNumber numberWithInt:indexPath.row]];
        NSLog(@"After Adding %@",arrIndexSelectionSelectedIndex);
    }
    
}


-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [arrSelectedIndex removeObject:[[arrReserveUser objectAtIndex:indexPath.row]objectForKey:@"id"]];
    
    [arrIndexSelectionSelectedIndex removeObject:[NSNumber numberWithInt:indexPath.row]];
    NSLog(@"After Removing %@",arrIndexSelectionSelectedIndex);
}
-(UITableViewCellEditingStyle)tableView: (UITableView *)tableView editingStyleForRowAtIndexPath: (NSIndexPath *)indexPath
{
    NSLog(@"%ld",(long)indexPath.row);
    //Do needed stuff here. Like removing values from stored NSMutableArray or UITableView datasource
    return YES;
}

/*
#pragma mark - Navigation

 
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
