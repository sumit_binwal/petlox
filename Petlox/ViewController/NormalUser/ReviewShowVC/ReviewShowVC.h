//
//  ReviewShowVC.h
//  Petlox
//
//  Created by Sumit Sharma on 09/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewShowVC : UIViewController
{
    
}
@property (nonatomic,strong)NSMutableDictionary *dictReviewPost;
@property(nonatomic,strong)NSMutableDictionary *dictBusinessUserDetail;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
- (IBAction)backBtnAction:(id)sender;
@end
