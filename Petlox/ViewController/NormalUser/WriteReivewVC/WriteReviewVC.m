//
//  WriteReviewVC.m
//  Petlox
//
//  Created by Sumit Sharma on 06/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "WriteReviewVC.h"
#import "ReviewShowVC.h"
#import "ServiceProviderVC.h"
@interface WriteReviewVC ()<UIGestureRecognizerDelegate>
{
    IBOutlet UILabel *lblTxtBoxLimit;
    IBOutlet UIImageView *ImgVwBusinessProfileImg;
    IBOutlet UILabel *lblBusinessName;
    IBOutlet UITextView *txtViewDetail;
    IBOutlet UILabel *lblBusinessAddress;
    IBOutlet UIButton *btnStar1;
    NSString *strStarRating;
    IBOutlet UIButton *btnStar5;
    IBOutlet UIButton *btnStar4;
    IBOutlet UIButton *btnStar3;
    IBOutlet UIButton *btnStar2;
    IBOutlet UIScrollView *scrllVw;
}
@end

@implementation WriteReviewVC
@synthesize dictBusinessDetail,dictAllreadyReviewed,strBusinessID;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    wbServiceCount=1;
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [APPDELEGATE showPetReservationTabBar:NO];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.btnPost.layer.cornerRadius = self.btnPost.frame.size.width/2;
    self.reviewView.layer.cornerRadius = 5.0f;
}

-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Write a Review"];
    [self.navigationController.navigationBar setHidden:NO];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
    
//    ImgVwBusinessProfileImg.layer.cornerRadius=ImgVwBusinessProfileImg.frame.size.width/2;
    ImgVwBusinessProfileImg.clipsToBounds=YES;
    ImgVwBusinessProfileImg.layer.borderColor=[UIColor whiteColor].CGColor;
    ImgVwBusinessProfileImg.layer.borderWidth=2.0f;
    NSString *strBusinessImg=[dictBusinessDetail objectForKey:@"image"];
    if (strBusinessImg.length>1) {
        [ImgVwBusinessProfileImg setImageWithURL:[NSURL URLWithString:strBusinessImg] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    lblBusinessName.text=[dictBusinessDetail objectForKey:@"business_name"];
    lblBusinessAddress.text=[dictBusinessDetail objectForKey:@"address"];

    if (dictAllreadyReviewed.count>0) {
        txtViewDetail.text=[dictAllreadyReviewed valueForKey:@"msg"];
        lblTxtBoxLimit.text=[NSString stringWithFormat:@"%u Characters",200-txtViewDetail.text.length];
        int avgRating=[[dictAllreadyReviewed valueForKey:@"rating"] intValue];
        switch (avgRating) {
            case 1:
            {
                [btnStar1 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
                strStarRating=@"1";
                break;
            }
            case 2:
            {
                [btnStar1 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
                [btnStar2 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
                                strStarRating=@"2";
                break;
            }
            case 3:
            {
                [btnStar1 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
                [btnStar2 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
                [btnStar3 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
                                                strStarRating=@"3";
                break;
            }
            case 4:
            {
                [btnStar1 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
                [btnStar2 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
                [btnStar3 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
                [btnStar4 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
                strStarRating=@"4";
                break;
            }
            case 5:
            {
                [btnStar1 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
                [btnStar2 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
                [btnStar3 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
                [btnStar4 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
                [btnStar5 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
                                                strStarRating=@"5";
                break;
            }
                
        }
        
    }
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTap)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];

}


-(void)singleTap{
    [self.view endEditing:YES];
    [scrllVw setScrollEnabled:NO];
    [scrllVw setContentOffset:CGPointZero];
    [scrllVw setScrollEnabled:NO];
    [scrllVw setScrollEnabled:NO];
    
}
-(IBAction)backBarButtonClicked:(id)sender
{
//    [self.navigationController popViewControllerAnimated:YES];
    
    ServiceProviderVC *serviceProviderVC = (ServiceProviderVC *)[CommonFunctions exists:[ServiceProviderVC class] in:self.navigationController];
    if(serviceProviderVC == nil){
        serviceProviderVC = [[ServiceProviderVC alloc] init];
        [self.navigationController pushViewController:serviceProviderVC animated:YES];
    } else {
        [self.navigationController popToViewController:serviceProviderVC animated:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-Scroll View Method
-(void)scrollViewToCenterOfScreen:(UITextView *)textField
{
    [scrllVw setScrollEnabled:YES];
    float difference;
    if (scrllVw.contentSize.height == 600)
        difference = 50.0f;
    else
        difference = 50.0f;
    CGFloat viewCenterY = textField.center.y+150;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat avaliableHeight = applicationFrame.size.height - 10.0f;
    CGFloat y = viewCenterY - avaliableHeight / 10.0f;
    
    NSLog(@"%f",y);
    if (y < 0)
        y = 0;
    
    [scrllVw setContentOffset:CGPointMake(0, y) animated:YES];
    [scrllVw setContentSize:CGSizeMake(320.0f, 830.0f)];
}



- (IBAction)starButtonClicked:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    switch (btn.tag) {
        case 0:
        {
            [btnStar1 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
            [btnStar2 setImage:[UIImage imageNamed:@"star_large"] forState:UIControlStateNormal];
            [btnStar3 setImage:[UIImage imageNamed:@"star_large"] forState:UIControlStateNormal];
            [btnStar4 setImage:[UIImage imageNamed:@"star_large"] forState:UIControlStateNormal];
            [btnStar5 setImage:[UIImage imageNamed:@"star_large"] forState:UIControlStateNormal];
            strStarRating=@"1";
            break;
        }
        case 1:
        {
            [btnStar1 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
            [btnStar2 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
            [btnStar3 setImage:[UIImage imageNamed:@"star_large"] forState:UIControlStateNormal];
            [btnStar4 setImage:[UIImage imageNamed:@"star_large"] forState:UIControlStateNormal];
            [btnStar5 setImage:[UIImage imageNamed:@"star_large"] forState:UIControlStateNormal];
                        strStarRating=@"2";
            break;
        }
        case 2:
        {
            [btnStar1 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
            [btnStar2 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
            [btnStar3 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
            [btnStar4 setImage:[UIImage imageNamed:@"star_large"] forState:UIControlStateNormal];
            [btnStar5 setImage:[UIImage imageNamed:@"star_large"] forState:UIControlStateNormal];
                        strStarRating=@"3";
            break;
        }
        case 3:
        {
            [btnStar1 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
            [btnStar2 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
            [btnStar3 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
            [btnStar4 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
            [btnStar5 setImage:[UIImage imageNamed:@"star_large"] forState:UIControlStateNormal];
                        strStarRating=@"4";
            break;
        }
        case 4:
        {
            [btnStar1 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
            [btnStar2 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
            [btnStar3 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
            [btnStar4 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
            [btnStar5 setImage:[UIImage imageNamed:@"star_large_focus"] forState:UIControlStateNormal];
                        strStarRating=@"5";
            break;
        }
            
    }
}

#pragma mark - UITextView Delegate Methods
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self scrollViewToCenterOfScreen:textView];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (textView==txtViewDetail) {
        lblTxtBoxLimit.text=[NSString stringWithFormat:@"%u Characters",200-range.location];
        
        
        if (range.location>=200) {
            lblTxtBoxLimit.text=@"0 Character";
            return NO;
            
            
        }
        else if (range.location==0 && text.length>0) {
          lblTxtBoxLimit.text=@"0 Character";
            NSString *str = [text substringToIndex: MIN(199, [text length])];
            txtViewDetail.text=str;
            return NO;
        }
        else
        {
            if ([text isEqualToString:@"\n"]) {
               // [self scrollToNormalView];
                [lblTxtBoxLimit resignFirstResponder];
                [self singleTap];
                return NO;
            }
            return YES;
        }
    }
    
    else
    {
        return YES;
    }
    return YES;
}


- (IBAction)postReviewBtnClicked:(id)sender {
    
    if ([strStarRating intValue]>0) {
        if (txtViewDetail.text.length > 1) {
            if ([CommonFunctions reachabiltyCheck]) {
                [CommonFunctions showActivityIndicatorWithText:@""];
                [self postReview];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
            }
        }
else
{
            [CommonFunctions alertTitle:@"" withMessage:@"Please enter review message."];
}
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please give rating to business."];
    }
}

#pragma mark- WebServices API..

-(void)postReview
{
    NSMutableDictionary *param;
    if (dictBusinessDetail.count>1) {
        strBusinessID=[dictBusinessDetail objectForKey:@"user_id"];
    }
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN_ID],@"user_id",strBusinessID,@"business_id",strStarRating,@"rating",txtViewDetail.text,@"review", nil];
    
    NSString *url = [NSString stringWithFormat:@"writeReview"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/confirmReservation
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            ReviewShowVC *rsvc=[[ReviewShowVC alloc]init];
            rsvc.dictReviewPost=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[responseDict objectForKey:@"message"],@"msg",[responseDict objectForKey:@"rating"],@"rating",[dictBusinessDetail objectForKey:@"user_id"],@"businessID", nil];
            rsvc.dictBusinessUserDetail=dictBusinessDetail;
            [self.navigationController pushViewController:rsvc animated:YES];

        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self postReview];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          
                                      }];
}



@end
