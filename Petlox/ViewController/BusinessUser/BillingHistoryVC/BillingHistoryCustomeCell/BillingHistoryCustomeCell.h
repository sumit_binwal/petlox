//
//  BillingWeekCustomeCell.h
//  Petlox
//
//  Created by Sumit Sharma on 09/11/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BillingHistoryCustomeCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblInvoiceTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblInvoiceAmount;
@property (strong, nonatomic) IBOutlet UIImageView *imgInvoiceStatus;

@end
