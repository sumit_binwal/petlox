//
//  UploadPhotoCustomeCell.h
//  Petlox
//
//  Created by Sumit Sharma on 03/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UploadPhotoCustomeCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgPhoto;
@property (strong, nonatomic) IBOutlet UIButton *imgAddBtn;
@property (strong, nonatomic) IBOutlet UIButton *imgDeleteBtn;

@end
