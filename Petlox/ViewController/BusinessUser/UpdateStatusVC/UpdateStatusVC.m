//
//  UpdateStatusVC.m
//  Petlox
//
//  Created by Sumit Sharma on 02/11/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "UpdateStatusVC.h"
#import "JobProgressVC.h"
@interface UpdateStatusVC ()
{
    
    IBOutlet UIButton *btnEtcReservation;
    IBOutlet UIButton *btnEtcUpdate;
    IBOutlet UILabel *lbl1UpdateStatus;
    IBOutlet UILabel *lbl2UpdateStatus;
    IBOutlet UILabel *lbl1CompleteMark;
    IBOutlet UILabel *lbl2CompleteMark;
    IBOutlet UILabel *lbl3CompleteMark;
    IBOutlet UIButton *btnCmpleteMark;
    IBOutlet UILabel *lblMarkThisJob;
    IBOutlet UIButton *btnCompleteJob;
    IBOutlet UILabel *lblEstimatedTimeofComplection;
    IBOutlet UIButton *btnSubmitETC;
    IBOutlet UIButton *btnMarkAsComplete;
    IBOutlet UIButton *btnETC;
    IBOutlet UIDatePicker *datePicker;
    IBOutlet UIView *vwDatePicker;
    IBOutlet UILabel *lblComplectionTime;
    IBOutlet UIView *vwUpdateETC;
    IBOutlet UIView *vwMarkasComplete;
    IBOutlet UIView *vwETCCntainer;
    IBOutlet UIView *vwCompleteETC;
    IBOutlet NSLayoutConstraint *viewBckGroundHightConstrint;
    IBOutlet UIView *vwCompleteMarkasComplete;
    IBOutlet UIView *vwCompleteMarkasCompleteContainer;
    NSString *selectedDate;
    NSMutableDictionary *dictStatusData;
    IBOutlet UIView *vwBtn;
}
@end

@implementation UpdateStatusVC
@synthesize reservationID;
- (void)viewDidLoad {
    [super viewDidLoad];
        // Do any additional setup after loading the view from its nib.
    [self setUpView];
}

-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Update Status"];
    
    
    [lblEstimatedTimeofComplection setFont:[UIFont fontWithName:lblEstimatedTimeofComplection.font.fontName size:lblEstimatedTimeofComplection.font.pointSize*SCREEN_XScale]];
    [lblMarkThisJob setFont:[UIFont fontWithName:lblMarkThisJob.font.fontName size:lblMarkThisJob.font.pointSize*SCREEN_XScale]];
    
    [lbl1CompleteMark setFont:[UIFont fontWithName:lbl1CompleteMark.font.fontName size:lbl1CompleteMark.font.pointSize*SCREEN_XScale]];
    [lbl2CompleteMark setFont:[UIFont fontWithName:lbl2CompleteMark.font.fontName size:lbl2CompleteMark.font.pointSize*SCREEN_XScale]];
    [lbl3CompleteMark setFont:[UIFont fontWithName:lbl3CompleteMark.font.fontName size:lbl3CompleteMark.font.pointSize*SCREEN_XScale]];
    
    [lbl1UpdateStatus setFont:[UIFont fontWithName:lbl1UpdateStatus.font.fontName size:lbl1UpdateStatus.font.pointSize*SCREEN_XScale]];
    [lbl2UpdateStatus setFont:[UIFont fontWithName:lbl2UpdateStatus.font.fontName size:lbl2UpdateStatus.font.pointSize*SCREEN_XScale]];
    
    
    
    btnSubmitETC.layer.cornerRadius=btnSubmitETC.bounds.size.width/2*SCREEN_XScale;
    [btnSubmitETC.titleLabel setFont:[UIFont fontWithName:btnSubmitETC.titleLabel.font.fontName size:btnSubmitETC.titleLabel.font.pointSize*SCREEN_XScale]];
    
    btnCompleteJob.layer.cornerRadius=btnCompleteJob.bounds.size.width/2*SCREEN_XScale;
    [btnCompleteJob.titleLabel setFont:[UIFont fontWithName:btnCompleteJob.titleLabel.font.fontName size:btnCompleteJob.titleLabel.font.pointSize*SCREEN_XScale]];
    btnCmpleteMark.layer.cornerRadius=btnCmpleteMark.bounds.size.width/2*SCREEN_XScale;
    [btnCmpleteMark.titleLabel setFont:[UIFont fontWithName:btnCmpleteMark.titleLabel.font.fontName size:btnCmpleteMark.titleLabel.font.pointSize*SCREEN_XScale]];

    
    btnEtcUpdate.layer.cornerRadius=btnEtcUpdate.bounds.size.width/2*SCREEN_XScale;
    [btnEtcUpdate.titleLabel setFont:[UIFont fontWithName:btnEtcUpdate.titleLabel.font.fontName size:btnEtcUpdate.titleLabel.font.pointSize*SCREEN_XScale]];
    btnEtcUpdate.titleLabel.numberOfLines=0;
    btnEtcUpdate.titleLabel.textAlignment=NSTextAlignmentCenter;
    btnEtcReservation.layer.cornerRadius=btnEtcReservation.bounds.size.width/2*SCREEN_XScale;
    [btnEtcReservation.titleLabel setFont:[UIFont fontWithName:btnEtcReservation.titleLabel.font.fontName size:btnEtcReservation.titleLabel.font.pointSize*SCREEN_XScale]];
    btnEtcReservation.titleLabel.numberOfLines=0;
    btnEtcReservation.titleLabel.textAlignment=NSTextAlignmentCenter;

    
    
    
    vwDatePicker.layer.borderWidth=1.0f;
    vwDatePicker.layer.borderColor=[UIColor colorWithRed:156.0f/255.0f green:156.0f/255.0f blue:156.0f/255.0f alpha:1].CGColor;
    datePicker.minimumDate=[NSDate date];
    wbServiceCount=1;

    
    dictStatusData=[[NSMutableDictionary alloc]init];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [datePicker setLocale:locale];
    
    vwCompleteMarkasCompleteContainer.layer.cornerRadius=6;
    vwCompleteMarkasCompleteContainer.layer.borderWidth=1.0f;
    vwCompleteMarkasCompleteContainer.layer.borderColor=[UIColor colorWithRed:190.0f/255.0f green:190.0f/255.0f blue:190.0f/255.0f alpha:1].CGColor;

    
//    vwUpdateETC.frame=CGRectMake(0.0f, 34.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
//    [self.view addSubview:vwUpdateETC];
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
//    [btnETC setBackgroundColor:[UIColor colorWithRed:110.0f/255.0f green:200.0f/255.0f blue:253.0f/255.0f alpha:1] ];
//    [btnETC setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [btnMarkAsComplete setBackgroundColor:[UIColor colorWithRed:152.0f/255.0f green:217/255.0f blue:255/255.0f alpha:1]];
//    [btnMarkAsComplete setTitleColor:[UIColor colorWithRed:0.0f/255.0f green:172.0f/255.0f blue:238.0f/255.0f alpha:1] forState:UIControlStateNormal];
    [btnETC setImage:[UIImage imageNamed:@"btnETC"] forState:UIControlStateNormal];
    [btnMarkAsComplete setImage:[UIImage imageNamed:@"btnmrkComplete"] forState:UIControlStateNormal];
    
//    btnETCFocus
    
    if ([UIScreen mainScreen].bounds.size.height>375) {
        viewBckGroundHightConstrint.constant=280.0f;
    }
    
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self checkJobStatus];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction Method
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)segmentTypeBtnClicked:(id)sender {
    [vwCompleteETC removeFromSuperview];
    [vwUpdateETC removeFromSuperview];
    [vwCompleteMarkasComplete removeFromSuperview];
    [vwMarkasComplete removeFromSuperview];
//    [btnETC setBackgroundColor:[UIColor colorWithRed:152.0f/255.0f green:217/255.0f blue:255/255.0f alpha:1]];
//    [btnETC setTitleColor:[UIColor colorWithRed:0.0f/255.0f green:172.0f/255.0f blue:238.0f/255.0f alpha:1] forState:UIControlStateNormal];
//    [btnMarkAsComplete setBackgroundColor:[UIColor colorWithRed:152.0f/255.0f green:217/255.0f blue:255/255.0f alpha:1]];
//    [btnMarkAsComplete setTitleColor:[UIColor colorWithRed:0.0f/255.0f green:172.0f/255.0f blue:238.0f/255.0f alpha:1] forState:UIControlStateNormal];
    [btnETC setImage:[UIImage imageNamed:@"btnETC"] forState:UIControlStateNormal];
    [btnMarkAsComplete setImage:[UIImage imageNamed:@"btnmrkComplete"] forState:UIControlStateNormal];

    
    UIButton *btn=(UIButton *)sender;
    switch (btn.tag) {
        case 0:
        {
            if ([[dictStatusData valueForKey:@"completedjob"]isEqualToString:@"yes"])
            {
//                [btnMarkAsComplete setBackgroundColor:[UIColor colorWithRed:110.0f/255.0f green:200.0f/255.0f blue:253.0f/255.0f alpha:1] ];
//                [btnMarkAsComplete setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [btnETC setImage:[UIImage imageNamed:@"btnETC"] forState:UIControlStateNormal];
                [btnMarkAsComplete setImage:[UIImage imageNamed:@"btnmrkCompleteFocus"] forState:UIControlStateNormal];
                [vwCompleteETC removeFromSuperview];
                [vwUpdateETC removeFromSuperview];
                [vwCompleteMarkasComplete removeFromSuperview];
                [vwMarkasComplete removeFromSuperview];
                vwCompleteMarkasComplete.frame=CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-(vwBtn.frame.size.height+66));
                [self.view addSubview:vwCompleteMarkasComplete];
                [CommonFunctions alertTitle:@"" withMessage:@"You can not update estimated time after job mark as completed."];
            }
            else
            {
                [btnETC setImage:[UIImage imageNamed:@"btnETCFocus"] forState:UIControlStateNormal];
                [btnMarkAsComplete setImage:[UIImage imageNamed:@"btnmrkComplete"] forState:UIControlStateNormal];
                
                NSString *strDate=[dictStatusData valueForKey:@"servicestartdate"];
                if (strDate.length>2) {
                    [vwCompleteETC removeFromSuperview];
                    [vwUpdateETC removeFromSuperview];
                    [vwCompleteMarkasComplete removeFromSuperview];
                    [vwMarkasComplete removeFromSuperview];
                    vwCompleteETC.frame=CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-(vwBtn.frame.size.height+66));
                    
                    NSString *strDate=[NSString stringWithFormat:@"%@",[dictStatusData objectForKey:@"servicestartdate"]];
                    NSString *strTime=[NSString stringWithFormat:@"%@",[dictStatusData objectForKey:@"servicestarttime"]];
                    
                    NSString *completeString=[NSString stringWithFormat:@"Estimated Time of Completion\n%@ \n%@",[dictStatusData objectForKey:@"servicestartdate"],[dictStatusData objectForKey:@"servicestarttime"]];
                    NSMutableAttributedString *attriBute=[[NSMutableAttributedString alloc]initWithString:completeString];
                    
                    [attriBute addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_OPENSANS_BOLD size:15.0f*SCREEN_XScale] range:[completeString rangeOfString:strDate]];
                    
                    [attriBute addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:181.0f/255.0f green:37.0f/255.0f blue:86.0f/255.0f alpha:1] range:[completeString rangeOfString:strDate]];
                    [attriBute addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:181.0f/255.0f green:37.0f/255.0f blue:86.0f/255.0f alpha:1] range:[completeString rangeOfString:strTime]];
                    
                    [attriBute addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_OPENSANS_BOLD size:15.0f*SCREEN_XScale] range:[completeString rangeOfString:strTime]];
                    
                    lblComplectionTime.attributedText=attriBute;
//                    lblComplectionTime.text=[NSString stringWithFormat:@"Estimated Time of Completion\n%@ \n%@",[dictStatusData objectForKey:@"servicestartdate"],[dictStatusData objectForKey:@"servicestarttime"]];
                    [self.view addSubview:vwCompleteETC];
                    
                }
                else
                {
                    vwUpdateETC.frame=CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-(vwBtn.frame.size.height+66));
                    [self.view addSubview:vwUpdateETC];
                }

            }
                        break;
        }
        case 1:
        {
//            [btn setBackgroundColor:[UIColor colorWithRed:110.0f/255.0f green:200.0f/255.0f blue:253.0f/255.0f alpha:1] ];
//            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnETC setImage:[UIImage imageNamed:@"btnETC"] forState:UIControlStateNormal];
            [btnMarkAsComplete setImage:[UIImage imageNamed:@"btnmrkComplete"] forState:UIControlStateNormal];
            if (![[dictStatusData valueForKey:@"completedjob"]isEqualToString:@"No"]) {
                [vwCompleteETC removeFromSuperview];
                [vwUpdateETC removeFromSuperview];
                [vwCompleteMarkasComplete removeFromSuperview];
                [vwMarkasComplete removeFromSuperview];
                [btnETC setImage:[UIImage imageNamed:@"btnETC"] forState:UIControlStateNormal];
                [btnMarkAsComplete setImage:[UIImage imageNamed:@"btnmrkCompleteFocus"] forState:UIControlStateNormal];
                vwCompleteMarkasComplete.frame=CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-(vwBtn.frame.size.height+66));
                [self.view addSubview:vwCompleteMarkasComplete];
            }
            else
            {
                vwMarkasComplete.frame=CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-(vwBtn.frame.size.height+66));
                [btnETC setImage:[UIImage imageNamed:@"btnETC"] forState:UIControlStateNormal];
                [btnMarkAsComplete setImage:[UIImage imageNamed:@"btnmrkCompleteFocus"] forState:UIControlStateNormal];

                [self.view addSubview:vwMarkasComplete];
            }
            break;
        }
    }
}
- (IBAction)submitBtnClicked:(id)sender
{
    NSLog(@"%@",    datePicker.date);
    selectedDate=[NSString stringWithFormat:@"%@",datePicker.date];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];

    [dateformate setDateFormat:@"yyyy-MM-dd HH:mm:ssZZZ"];
    
    NSDate *date = [dateformate dateFromString:selectedDate];
    NSDateFormatter *dateformate1=[[NSDateFormatter alloc]init];
    // Convert date object into desired format
    [dateformate1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

    selectedDate = [dateformate1 stringFromDate:date];
 
    
    
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self addReservationStartStatus];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }
}
- (IBAction)back2ReservationBtnClicked:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (IBAction)updateStatusBtnClicked:(id)sender
{
    [vwCompleteETC removeFromSuperview];
    [vwUpdateETC removeFromSuperview];
    [vwCompleteMarkasComplete removeFromSuperview];
    [vwMarkasComplete removeFromSuperview];
    vwUpdateETC.frame=CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-(vwBtn.frame.size.height+66));
    [self.view addSubview:vwUpdateETC];
}
- (IBAction)completeBtnClicked:(id)sender
{
    
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
         [self completeJob];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }

   
    
}

#pragma mark - WebService API
-(void)addReservationStartStatus
{
    NSString *url = [NSString stringWithFormat:@"addReservationStartStatus"];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:reservationID,@"id",selectedDate,@"date", nil];
    NSLog(@"%@",selectedDate);
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/addReservationStartStatus
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            dictStatusData=(NSMutableDictionary*)responseDict;
                [vwCompleteETC removeFromSuperview];
                [vwUpdateETC removeFromSuperview];
                [vwCompleteMarkasComplete removeFromSuperview];
                [vwMarkasComplete removeFromSuperview];
                vwCompleteETC.frame=CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-(vwBtn.frame.size.height+66));
            NSString *strDate=[NSString stringWithFormat:@"%@",[responseDict objectForKey:@"servicestartdate"]];
            NSString *strTime=[NSString stringWithFormat:@"%@",[responseDict objectForKey:@"servicestarttime"]];
            
            NSString *completeString=[NSString stringWithFormat:@"Estimated Time of Completion\n%@ \n%@",[responseDict objectForKey:@"servicestartdate"],[responseDict objectForKey:@"servicestarttime"]];
            NSMutableAttributedString *attriBute=[[NSMutableAttributedString alloc]initWithString:completeString];
            
            [attriBute addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_OPENSANS_BOLD size:15.0f*SCREEN_XScale] range:[completeString rangeOfString:strDate]];
            
            [attriBute addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:181.0f/255.0f green:37.0f/255.0f blue:86.0f/255.0f alpha:1] range:[completeString rangeOfString:strDate]];
            [attriBute addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:181.0f/255.0f green:37.0f/255.0f blue:86.0f/255.0f alpha:1] range:[completeString rangeOfString:strTime]];
            
            
            [attriBute addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_OPENSANS_BOLD size:15.0f*SCREEN_XScale] range:[completeString rangeOfString:strTime]];
            lblComplectionTime.attributedText=attriBute;
//                lblComplectionTime.text=[NSString stringWithFormat:@"Estimated Time of Completion\n%@ \n%@",[responseDict objectForKey:@"servicestartdate"],[responseDict objectForKey:@"servicestarttime"]];
                [self.view addSubview:vwCompleteETC];
            [btnETC setImage:[UIImage imageNamed:@"btnETCFocus"] forState:UIControlStateNormal];
            [btnMarkAsComplete setImage:[UIImage imageNamed:@"btnmrkComplete"] forState:UIControlStateNormal];

            if ([[dictStatusData valueForKey:@"completedjob"]isEqualToString:@"yes"])
            {
                self.navigationItem.leftBarButtonItem = nil;
                self.navigationItem.hidesBackButton = YES;
                [self.navigationItem.leftBarButtonItem setEnabled:NO];
            }
        }
        else
        {
            
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self addReservationStartStatus];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}
-(void)checkJobStatus
{
    
    NSString *url = [NSString stringWithFormat:@"checkReservationStartStatus"];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];

    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:reservationID,@"id", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/checkReservationStartStatus
    
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            dictStatusData=(NSMutableDictionary *)responseDict;
            if ([_fromFinishJob isEqualToString:@"fromFinishJob"])
            {
//                [btnETC setBackgroundColor:[UIColor colorWithRed:152.0f/255.0f green:217/255.0f blue:255/255.0f alpha:1]];
//                [btnETC setTitleColor:[UIColor colorWithRed:0.0f/255.0f green:172.0f/255.0f blue:238.0f/255.0f alpha:1] forState:UIControlStateNormal];
//                [btnMarkAsComplete setBackgroundColor:[UIColor colorWithRed:110.0f/255.0f green:200.0f/255.0f blue:253.0f/255.0f alpha:1] ];
//                [btnMarkAsComplete setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

                vwMarkasComplete.frame=CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-(vwBtn.frame.size.height+66));
                [self.view addSubview:vwMarkasComplete];
                [btnETC setImage:[UIImage imageNamed:@"btnETC"] forState:UIControlStateNormal];
                [btnMarkAsComplete setImage:[UIImage imageNamed:@"btnmrkCompleteFocus"] forState:UIControlStateNormal];

            }
            else
            {
                NSString *strDate=[dictStatusData valueForKey:@"servicestartdate"];
                if (strDate.length>2) {
                    [vwCompleteETC removeFromSuperview];
                    [vwUpdateETC removeFromSuperview];
                    [vwCompleteMarkasComplete removeFromSuperview];
                    [vwMarkasComplete removeFromSuperview];
                    vwCompleteETC.frame=CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-(vwBtn.frame.size.height+66));
                    
                    NSString *strDate=[NSString stringWithFormat:@"%@",[responseDict objectForKey:@"servicestartdate"]];
                    NSString *strTime=[NSString stringWithFormat:@"%@",[responseDict objectForKey:@"servicestarttime"]];
                    
                    NSString *completeString=[NSString stringWithFormat:@"Estimated Time of Completion\n%@ \n%@",[responseDict objectForKey:@"servicestartdate"],[responseDict objectForKey:@"servicestarttime"]];
                    NSMutableAttributedString *attriBute=[[NSMutableAttributedString alloc]initWithString:completeString];
                    
                    [attriBute addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_OPENSANS_BOLD size:15.0f*SCREEN_XScale] range:[completeString rangeOfString:strDate]];
                    
                    [attriBute addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:181.0f/255.0f green:37.0f/255.0f blue:86.0f/255.0f alpha:1] range:[completeString rangeOfString:strDate]];
                    [attriBute addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:181.0f/255.0f green:37.0f/255.0f blue:86.0f/255.0f alpha:1] range:[completeString rangeOfString:strTime]];
                    
                    
                    [attriBute addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_OPENSANS_BOLD size:15.0f*SCREEN_XScale] range:[completeString rangeOfString:strTime]];
                    
                    lblComplectionTime.attributedText=attriBute;

                    
//                    lblComplectionTime.text=;
                    [self.view addSubview:vwCompleteETC];
                    [btnETC setImage:[UIImage imageNamed:@"btnETCFocus"] forState:UIControlStateNormal];
                    [btnMarkAsComplete setImage:[UIImage imageNamed:@"btnmrkComplete"] forState:UIControlStateNormal];
                    if ([[dictStatusData valueForKey:@"completedjob"]isEqualToString:@"yes"])
                    {
                        self.navigationItem.leftBarButtonItem = nil;
                        self.navigationItem.hidesBackButton = YES;
                        [self.navigationItem.leftBarButtonItem setEnabled:NO];
                    }
                    
                    
                }
                else
                {
                    vwUpdateETC.frame=CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-(vwBtn.frame.size.height+66));
                    
                    [btnETC setImage:[UIImage imageNamed:@"btnETCFocus"] forState:UIControlStateNormal];
                    [btnMarkAsComplete setImage:[UIImage imageNamed:@"btnmrkComplete"] forState:UIControlStateNormal];
                    
                    [self.view addSubview:vwUpdateETC];
                    
                }
            }
            
            
        }
        else
        {
            
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self checkJobStatus];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}
-(void)completeJob
{
    
    NSString *url = [NSString stringWithFormat:@"addReservationCompleteStatus"];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token", nil];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:reservationID,@"id", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/checkReservationStartStatus
    
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            dictStatusData=(NSMutableDictionary*)responseDict;
            [vwCompleteETC removeFromSuperview];
            [vwUpdateETC removeFromSuperview];
            [vwCompleteMarkasComplete removeFromSuperview];
            [vwMarkasComplete removeFromSuperview];
            vwCompleteMarkasComplete.frame=CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-(vwBtn.frame.size.height+66));
            [self.view addSubview:vwCompleteMarkasComplete];
            self.navigationItem.leftBarButtonItem = nil;
            self.navigationItem.hidesBackButton = YES;
            [self.navigationItem.leftBarButtonItem setEnabled:NO];
        }
        else
        {
            
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{

                                              if (wbServiceCount==1) {

                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self completeJob];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }

                                          }
                                          
                                      }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
