//
//  AddPaymentVC.m
//  Petlox
//
//  Created by Sumit Sharma on 02/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "AddPaymentVC.h"
#import "SelectCardTypeVC.h"
@interface AddPaymentVC ()
{
    IBOutlet UILabel *label1;
    IBOutlet UILabel *label2;
    IBOutlet UIButton *btnNext;
    
}
@end

@implementation AddPaymentVC

#pragma mark - Life Cycle Method
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];

    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
    [label1 setFont:[UIFont fontWithName:label1.font.fontName size:label1.font.pointSize*SCREEN_XScale]];
    [label2 setFont:[UIFont fontWithName:label2.font.fontName size:label2.font.pointSize*SCREEN_XScale]];
    
    btnNext.layer.cornerRadius=btnNext.bounds.size.width/2*SCREEN_XScale;
    [btnNext.titleLabel setFont:[UIFont fontWithName:btnNext.titleLabel.font.fontName size:btnNext.titleLabel.font.pointSize*SCREEN_XScale]];
    
    

    
    [self.navigationItem setTitle:@"Payment"];
}



#pragma mark - IBAction Button Methods
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)addPaymentMethodBtnClicked:(id)sender {
    SelectCardTypeVC *sctvc=[[SelectCardTypeVC alloc]init];
    [self.navigationController pushViewController:sctvc animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
