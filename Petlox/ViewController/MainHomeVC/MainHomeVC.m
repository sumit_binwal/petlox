//
//  MainHomeVC.m
//  Petlox
//
//  Created by Sumit Sharma on 20/07/16.
//  Copyright © 2016 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "MainHomeVC.h"
#import "SignInVC.h"
@interface MainHomeVC ()
{
    
    IBOutlet UILabel *lblMsg;
    IBOutlet UIButton *btnBusiness;
    IBOutlet UIButton *btnUser;
}
@end

@implementation MainHomeVC

- (void)viewDidLoad {
    [CommonFunctions setNavigationBar:self.navigationController];
    [super viewDidLoad];
    [self setFontSizes];
    
    
    // Do any additional setup after loading the view from its nib.
}

-(void)setFontSizes
{
    [btnUser.titleLabel setFont:[UIFont fontWithName:btnUser.titleLabel.font.fontName size:btnUser.titleLabel.font.pointSize*SCREEN_XScale]];
    [btnBusiness.titleLabel setFont:[UIFont fontWithName:btnBusiness.titleLabel.font.fontName size:btnBusiness.titleLabel.font.pointSize*SCREEN_XScale]];
    [lblMsg setFont:[UIFont fontWithName:lblMsg.font.fontName size:lblMsg.font.pointSize*SCREEN_XScale]];
    
   
    
    NSString *completeString=[NSString stringWithFormat:@"Choose USER if you have a pet\n or \nBUSINESS if you have business"];
    NSMutableAttributedString *attriBute=[[NSMutableAttributedString alloc]initWithString:completeString];
    
    [attriBute addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_OPENSANS_BOLD size:15.0f*SCREEN_XScale] range:[completeString rangeOfString:@"USER"]];
    
    [attriBute addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_OPENSANS_BOLD size:15.0f*SCREEN_XScale] range:[completeString rangeOfString:@"BUSINESS"]];
    
    lblMsg.attributedText=attriBute;

    
    
   
//    NSLog(@"%f",btnBusiness.frame.size.width);
//    NSLog(@"%f",btnBusiness.frame.size.height);
    btnBusiness.layer.cornerRadius=(btnBusiness.bounds.size.width/2)*SCREEN_XScale;
    btnUser.layer.cornerRadius=(btnBusiness.bounds.size.width/2)*SCREEN_XScale;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnBusinessBtnClicked:(id)sender {
    SignInVC *sivc=[[SignInVC alloc]init];
    sivc.isFromBusiness=YES;
    [self.navigationController pushViewController:sivc animated:YES];
}
- (IBAction)btnUserBtnClicked:(id)sender {
    SignInVC *sivc=[[SignInVC alloc]init];
    sivc.isFromBusiness=NO;
    [self.navigationController pushViewController:sivc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
