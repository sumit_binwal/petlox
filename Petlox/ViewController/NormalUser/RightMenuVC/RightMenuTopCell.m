//
//  RightMenuTopCell.m
//  Petlox
//
//  Created by Suchita Bohra on 22/07/16.
//  Copyright © 2016 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "RightMenuTopCell.h"

@implementation RightMenuTopCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        self = (RightMenuTopCell *)[[[NSBundle mainBundle] loadNibNamed:@"RightMenuTopCell" owner:self options:nil] firstObject];
        // Initialization code
    }
    return self;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
