//
//  ViewFullImageVC.m
//  Petlox
//
//  Created by Sumit Sharma on 13/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "ViewFullImageVC.h"

@interface ViewFullImageVC ()
{
    IBOutlet UIImageView *fullImgView;
}
@end

@implementation ViewFullImageVC
@synthesize strImgURL;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"Photos"];
    [fullImgView setImageWithURL:[NSURL URLWithString:strImgURL] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backBtnClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
