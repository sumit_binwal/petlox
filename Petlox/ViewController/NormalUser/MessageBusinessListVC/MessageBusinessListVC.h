//
//  PetGrommingVC.h
//  DemoPetLox
//
//  Created by Chagan Singh on 11/09/15.
//  Copyright (c) 2015 Chagan Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageBusinessListVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UITextFieldDelegate>
{
    
    IBOutlet UISearchBar *searchBar;
    IBOutlet UITableView *tblPetGromming;
    IBOutlet UITextField *txtSearchField;
    IBOutlet UIButton *btnFooter;
    IBOutlet UIView *footerView;
    IBOutlet UILabel *lblNoResultsFound;
    IBOutlet UIActivityIndicatorView *actiVItyLoader;
    
    NSMutableArray *arrGroomers;
    NSMutableArray *arrAllrecords;
    
    int pageValue;
    int lastLoad;
    
}
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;
@property (nonatomic,strong) NSString *strKeyword;
@property (nonatomic, strong) NSString *strSearchCategoryType;

- (IBAction)btnFooterPressed:(id)sender;
- (IBAction)searchTextChanged:(UITextField *)sender;
- (IBAction)btnSearchText:(id)sender;
- (IBAction)btnBackPressed:(id)sender;

@end
