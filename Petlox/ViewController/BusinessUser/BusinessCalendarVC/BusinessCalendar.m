//
//  CalendarVC.m
//  Petlox
//
//  Created by Sumit Sharma on 15/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "BusinessCalendar.h"
#import "VRGCalendarView.h"
#import <EventKit/EventKit.h>
@interface BusinessCalendar ()<VRGCalendarViewDelegate,UIAlertViewDelegate>
{
    IBOutlet UILabel *lblDate;
    VRGCalendarView *calendar;
    IBOutlet UIView *vwCalenderVw;
    IBOutlet UILabel *lblMonth;
    EKEventStore *store;

}
@end

@implementation BusinessCalendar
@synthesize lblWeekDay,strbusiessName,dictUserData;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
         store= [EKEventStore new];
       // Do any additional setup after loading the view from its nib.
}

-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
   [self.navigationItem setTitle:@"Add To Calendar"];
    [self.navigationController.navigationBar setHidden:NO];
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];

    
    
calendar = [[VRGCalendarView alloc] init];
    calendar.delegate=self;
    
    [vwCalenderVw addSubview:calendar];
    
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd"];
    lblDate.text = [dateFormat stringFromDate:today];
    
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    [dateFormat1 setDateFormat:@"EEEE"];
    lblWeekDay.text = [dateFormat1 stringFromDate:today];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupNewMotheName:) name:@"ChangeDate" object:nil];


}
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnAddtoCalendearClicked:(id)sender {
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    
    dateFormatter.dateFormat = @"dd MMM,yyyy hh:mm:ss a";
    NSString *myDate=[dictUserData objectForKey:@"reservation_date"];
    //dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *convertDate = [dateFormatter dateFromString:myDate];
    NSString *myString = [dateFormatter stringFromDate:convertDate];
    NSLog(@"%@",myString);
    [dateFormatter setAMSymbol:@"am"];
    [dateFormatter setPMSymbol:@"pm"];
    NSDate *yourDate = [dateFormatter dateFromString:myString];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSLog(@"%@",yourDate);
    NSString *dateString=[dateFormatter stringFromDate:yourDate] ;
    NSLog(@"%@",dateString);
    NSDate *finalDate = [dateFormatter dateFromString:dateString];
    NSLog(@"%@",finalDate);

   // 26 Oct,2015 09:49:00 PM
    
    UIButton *btn=(UIButton *)sender;
   // btn.tag=0;
    if (btn.tag==0) {
        [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            if (!granted)
            {
                
                return ;
            }
            EKEvent *event = [EKEvent eventWithEventStore:store];
            NSString *fullName=[NSString stringWithFormat:@"%@ %@",[dictUserData objectForKey:@"first_name"],[dictUserData objectForKey:@"last_name"]];
            event.title = [NSString stringWithFormat:@"Reserved - %@",fullName];
            event.startDate = finalDate; //today
            event.endDate = [event.startDate dateByAddingTimeInterval:60*60*3 ];  //set 1 hour meeting
            event.calendar = [store defaultCalendarForNewEvents];
            NSError *err = nil;
            [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];

            //    self.savedEventId = event.eventIdentifier;  //save the event id if you want to access this later
        }];
        [CommonFunctions alertTitle:@"" withMessage:@"Event added sucessfully to calendar." withDelegate:self withTag:100];
        btn.tag=1;
    }
    else if (btn.tag==1)
    {
            [CommonFunctions alertTitle:@"" withMessage:@"Event allready added sucessfully to calendar."];
    }
}
-(void)ViewWillDisAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)setupNewMotheName:(NSNotification *) notification
{
    NSLog(@"%@",[NSDate date]);
    lblMonth.text=[NSString stringWithFormat:@"%@",notification.object];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)calendarView:(VRGCalendarView *)calendarView switchedToMonth:(int)month targetHeight:(float)targetHeight animated:(BOOL)animated {
    //if (month==[[NSDate date]month]) {
        NSArray *dates = [NSArray arrayWithObjects:[NSNumber numberWithInt:1],[NSNumber numberWithInt:5], nil];
        [calendarView markDates:dates];

    //}
}

-(void)calendarView:(VRGCalendarView *)calendarView dateSelected:(NSDate *)date {
    NSLog(@"Selected date = %@",date);

}

#pragma mark- UIAlertView Delegate Method
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

#pragma mark- IBActionButtons
- (IBAction)rghtbtnClicked:(id)sender {
    calendar.showNextMonth;
}

- (IBAction)leftBtnClicked:(id)sender {
    calendar.showPreviousMonth;
}


@end
