//
//  ChoosePetVC.h
//  Petlox
//
//  Created by Sumit Sharma on 10/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChoosePetVC : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

- (IBAction)dogSelected:(id)sender;
- (IBAction)catSelected:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *dogImageView;
@property (weak, nonatomic) IBOutlet UIImageView *catImageView;
@end
