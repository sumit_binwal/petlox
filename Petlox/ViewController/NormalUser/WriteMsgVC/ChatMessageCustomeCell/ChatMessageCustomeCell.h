//
//  ChatMessageCustomeCell.h
//  Petlox
//
//  Created by Sumit Sharma on 16/12/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatMessageCustomeCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *bgVw;
@property (strong, nonatomic) IBOutlet UIImageView *imgProfileImg;
@property (strong, nonatomic) IBOutlet UILabel *lblMsg;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;

@end
