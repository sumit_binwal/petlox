//
//  MapPathVC.m
//  Petlox
//
//  Created by Sumit Sharma on 04/01/16.
//  Copyright © 2016 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "MapPathVC.h"
#import <MapKit/MapKit.h>
@interface MapPathVC ()<MKMapViewDelegate>
{
    IBOutlet MKMapView *mkMapVw;
    
    IBOutlet UIWebView *wbViw;
}

@end

@implementation MapPathVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [CommonFunctions setNavigationBar:self.navigationController];


    
    
    NSString *urlAddress = @"http://maps.google.com/?saddr=26.898989,75.787270&daddr=28.898989,78.787270";
    
    //Create a URL object.
    NSURL *url = [NSURL URLWithString:urlAddress];
    
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    //Load the request in the UIWebView.
    [wbViw loadRequest:requestObj];
    


    


  //  self.searchButton.hidden = YES;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}







-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    [mkMapVw setCenterCoordinate:userLocation.coordinate animated:YES];
}

-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolylineView* aView = [[MKPolylineView alloc]initWithPolyline:(MKPolyline*)overlay] ;
        aView.strokeColor = [[UIColor blueColor] colorWithAlphaComponent:0.5];
        aView.lineWidth = 10;
        return aView;
    }
    return nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
