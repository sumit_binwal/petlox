//
//  PetGromingCell.h
//  DemoPetLox
//
//  Created by Chagan Singh on 11/09/15.
//  Copyright (c) 2015 Chagan Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PetGromingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgProfileView;
@property (weak, nonatomic) IBOutlet UILabel *lblNameGrommer;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblNoOfreview;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UIImageView *imgFavourate;
//@property (weak, nonatomic) IBOutlet UIImageView *imgFirstStar;
//@property (weak, nonatomic) IBOutlet UIImageView *imgSecndStar;
//@property (weak, nonatomic) IBOutlet UIImageView *imgThirdStar;
//@property (weak, nonatomic) IBOutlet UIImageView *imgFourthStar;
//@property (weak, nonatomic) IBOutlet UIImageView *imgFiveStar;
@property (strong, nonatomic) IBOutlet UIButton *btnFavourate;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *imsStars;
@end
