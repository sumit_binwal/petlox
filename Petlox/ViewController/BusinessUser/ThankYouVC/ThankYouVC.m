//
//  ThankYouVC.m
//  Petlox
//
//  Created by Sumit Sharma on 26/08/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "ThankYouVC.h"
#import "BusinessSignupFirstVC.h"
@interface ThankYouVC ()
{
    
    IBOutlet UILabel *lblthnksHeading;
    IBOutlet UILabel *lblthanksMsg;
    IBOutlet UIButton *btnContinue;
}
@end

@implementation ThankYouVC
@synthesize businessName;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [CommonFunctions setNavigationBar:self.navigationController];
    
    
    [lblthnksHeading setFont:[UIFont fontWithName:lblthnksHeading.font.fontName size:lblthnksHeading.font.pointSize*SCREEN_XScale]];
   
    [lblthanksMsg setFont:[UIFont fontWithName:lblthanksMsg.font.fontName size:lblthanksMsg.font.pointSize*SCREEN_XScale]];
    
    
    btnContinue.titleLabel.numberOfLines=0;
    btnContinue.titleLabel.textAlignment=NSTextAlignmentCenter;
    btnContinue.layer.cornerRadius=btnContinue.bounds.size.width/2*SCREEN_XScale;
    [btnContinue.titleLabel setFont:[UIFont fontWithName:btnContinue.titleLabel.font.fontName size:btnContinue.titleLabel.font.pointSize*SCREEN_XScale]];

    lblthanksMsg.text=[NSString stringWithFormat:@"You have successfully confirmed\nyour ownership of\n %@",businessName];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
}
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)continueBtnClicked:(id)sender {
    BusinessSignupFirstVC *bssvc=[[BusinessSignupFirstVC alloc]initWithNibName:@"BusinessSignupFirstVC" bundle:nil];
    [self.navigationController pushViewController:bssvc animated:YES];
       }







- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
