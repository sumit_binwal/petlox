//
//  PetGromingCell.m
//  DemoPetLox
//
//  Created by Chagan Singh on 11/09/15.
//  Copyright (c) 2015 Chagan Singh. All rights reserved.
//

#import "PetGromingCell.h"

@implementation PetGromingCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self = (PetGromingCell *)[[[NSBundle mainBundle] loadNibNamed:@"PetGromingCell" owner:self options:nil] firstObject];
        
    }
    
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


@end
