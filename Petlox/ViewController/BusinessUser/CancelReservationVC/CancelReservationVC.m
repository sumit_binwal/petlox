//
//  CancelReservationVC.m
//  Petlox
//
//  Created by Sumit Sharma on 02/11/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "CancelReservationVC.h"
#import "DeclineConfermationVC.h"
#import "SettingsCell.h"
@interface CancelReservationVC ()<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *tblVw;
    IBOutlet UIImageView *imgArrow1;
    IBOutlet UIImageView *imgArrow4;
    IBOutlet UIImageView *imgArrow3;
    IBOutlet UIScrollView *scrllVw;
    IBOutlet UIImageView *imgArrow2;
    NSString *strCancleReason;
    NSMutableArray *arrData;
    IBOutlet UIButton *btnCancel;
}
@end

@implementation CancelReservationVC
@synthesize strReservationID;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    // Do any additional setup after loading the view from its nib.
}
-(void)setupView
{
    wbServiceCount=1;
    
    btnCancel.layer.cornerRadius=btnCancel.bounds.size.width/2*SCREEN_XScale;
    [btnCancel.titleLabel setFont:[UIFont fontWithName:btnCancel.titleLabel.font.fontName size:btnCancel.titleLabel.font.pointSize*SCREEN_XScale]];
    
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Reason For Cancellation"];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
    arrData =[[NSMutableArray alloc]initWithObjects:@"I am Booked",@"I am Closed",@"Customer Canceled",@"Customer No-Show",nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction Btn Clicked
-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnReasonClicked:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    
    switch (btn.tag) {
        case 0:
        {
            [imgArrow1 setHidden:NO];
            [imgArrow2 setHidden:YES];
            [imgArrow3 setHidden:YES];
            [imgArrow4 setHidden:YES];
            strCancleReason=@"I am Booked";
            break;
        }
        case 1:
        {
            [imgArrow1 setHidden:YES];
            [imgArrow2 setHidden:NO];
            [imgArrow3 setHidden:YES];
            [imgArrow4 setHidden:YES];
            strCancleReason=@"I am Closed";
            break;
        }
        case 2:
        {
            [imgArrow1 setHidden:YES];
            [imgArrow2 setHidden:YES];
            [imgArrow3 setHidden:NO];
            [imgArrow4 setHidden:YES];
            strCancleReason=@"Customer Canceled";
            break;
        }
        case 3:
        {
            [imgArrow1 setHidden:YES];
            [imgArrow2 setHidden:YES];
            [imgArrow3 setHidden:YES];
            [imgArrow4 setHidden:NO];
            strCancleReason=@"Customer No-Show";
            break;
        }
        default:
            break;
    }
}

- (IBAction)cancelReservationBtnClicked:(id)sender
{
    [self.view endEditing:YES];
    if (strCancleReason.length>1) {
        if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self cancelReaservation];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
        }
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please select reason for cancellation."];
    }
}
#pragma mark - UITableView Delegate Method
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrData.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f*SCREEN_XScale;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *settingsCell   = @"SettingsCell";
    
    SettingsCell *cell = (SettingsCell *)[tableView dequeueReusableCellWithIdentifier:settingsCell];
    
    if (cell == nil)
    {
        cell = (SettingsCell*)[[[NSBundle mainBundle] loadNibNamed:@"SettingsCell" owner:self options:nil] objectAtIndex:0];
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    
        cell.lblTxt.text = arrData[indexPath.row];
    
    if ([strCancleReason isEqualToString:[arrData objectAtIndex:indexPath.row]]) {
        
        [cell.imgViewArrow setHidden:NO];
    }
    else
    {
        [cell.imgViewArrow setHidden:YES];
    }
    
    return cell;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
        {
            
            strCancleReason=@"I am Booked";
            break;
        }
        case 1:
        {
            
            strCancleReason=@"I am Closed";
            break;
        }
        case 2:
        {
            
            strCancleReason=@"Customer Canceled";
            break;
        }
        case 3:
        {
            
            strCancleReason=@"Customer No-Show";
            break;
        }
            
    }
    [tblVw reloadData];
}


#pragma mark- WebServices API..

-(void)cancelReaservation
{
    NSMutableDictionary *param;
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN_ID],@"token",@"business",@"type", nil];
    
    param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:strReservationID,@"id",strCancleReason,@"reason_for_cancel",@"business",@"type", nil];
    
    NSString *url = [NSString stringWithFormat:@"cancelReservation"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.250/petlox/mobile/cancelReservation
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            DeclineConfermationVC *dcvc=[[DeclineConfermationVC alloc]init];
            dcvc.lblfromCancel=@"yes";
            [self.navigationController pushViewController:dcvc animated:YES];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            //            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              

                                              if (wbServiceCount==1) {
                                              wbServiceCount=0;
                                                  [CommonFunctions showActivityIndicatorWithText:@""];
                                                  [self cancelReaservation];
                                              }
                                              else
                                              {
                                                                  [CommonFunctions removeActivityIndicator];
                                  [CommonFunctions alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          
                                      }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
