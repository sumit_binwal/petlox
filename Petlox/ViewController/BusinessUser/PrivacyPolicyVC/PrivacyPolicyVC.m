//
//  PrivacyPolicyVC.m
//  Petlox
//
//  Created by Sumit Sharma on 02/12/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "PrivacyPolicyVC.h"

@interface PrivacyPolicyVC ()<UIWebViewDelegate>
{
    IBOutlet UIWebView *wbVw;
    
}
@end

@implementation PrivacyPolicyVC
@synthesize strUrlString;
- (void)viewDidLoad {
    [super viewDidLoad];
    [CommonFunctions setNavigationBar:self.navigationController];

    [self.navigationItem setTitle:@"Privacy Policy"];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
    //Create a URL object.
    NSURL *url = [NSURL URLWithString:strUrlString];
    
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    //Load the request in the UIWebView.
    [wbVw loadRequest:requestObj];
    
    // Do any additional setup after loading the view from its nib.
}

-(IBAction)backBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIWebView Delegate Method
-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [CommonFunctions showActivityIndicatorWithText:@""];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [CommonFunctions removeActivityIndicator];
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [CommonFunctions alertTitle:@"" withMessage:[NSString stringWithFormat:@"%@",error]];
    [CommonFunctions removeActivityIndicator];
}



@end
