//
//  ReviewShowVC.m
//  Petlox
//
//  Created by Sumit Sharma on 09/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "ReviewShowVC.h"
#import "WriteReviewVC.h"
#import "ServiceProviderVC.h"
@interface ReviewShowVC ()
{
    

    IBOutlet UILabel *lblMsg;
    IBOutlet UIImageView *star1;
    IBOutlet UIImageView *star3;
    IBOutlet UIImageView *star4;
    IBOutlet UIImageView *star5;
    IBOutlet UIImageView *star2;
}
@end

@implementation ReviewShowVC
@synthesize dictReviewPost,dictBusinessUserDetail;
- (void)viewDidLoad {
    [super viewDidLoad];
    lblMsg.text=[dictReviewPost valueForKey:@"msg"];
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Review"];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"barLeftButton"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    

    
    
    if ([self isNotNull:[dictReviewPost valueForKey:@"rating"]]) {
        int avgRating=[[dictReviewPost valueForKey:@"rating"]intValue];
        switch (avgRating) {
            case 0:
            {
                break;
            }
                
            case 1:
            {
                [star1 setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
            case 2:
            {
                [star1 setImage:[UIImage imageNamed:@"star_foucs"]];
                [star2 setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
            case 3:
            {
                [star1 setImage:[UIImage imageNamed:@"star_foucs"]];
                [star2 setImage:[UIImage imageNamed:@"star_foucs"]];
                [star3 setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
            case 4:
            {
                [star1 setImage:[UIImage imageNamed:@"star_foucs"]];
                [star2 setImage:[UIImage imageNamed:@"star_foucs"]];
                [star3 setImage:[UIImage imageNamed:@"star_foucs"]];
                [star4 setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
            case 5:
            {
                [star1 setImage:[UIImage imageNamed:@"star_foucs"]];
                [star2 setImage:[UIImage imageNamed:@"star_foucs"]];
                [star3 setImage:[UIImage imageNamed:@"star_foucs"]];
                [star4 setImage:[UIImage imageNamed:@"star_foucs"]];
                [star5 setImage:[UIImage imageNamed:@"star_foucs"]];
                break;
            }
                
            default:
                break;
        }
        
    }

    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [APPDELEGATE showPetReservationTabBar:NO];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.backButton.layer.cornerRadius = self.backButton.frame.size.width/2;
}

-(IBAction)backBarButtonClicked:(id)sender
{
    [self navigateToServiceProvider];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)editBtnClicked:(id)sender {
    NSLog(@"%@",dictBusinessUserDetail);
    WriteReviewVC *wrvc=[[WriteReviewVC alloc]initWithNibName:@"WriteReviewVC" bundle:nil];
    wrvc.dictAllreadyReviewed=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[dictReviewPost valueForKey:@"msg"],@"msg",[dictReviewPost valueForKey:@"rating"],@"rating", nil];
    wrvc.dictBusinessDetail=dictBusinessUserDetail;
    wrvc.strBusinessID=[dictBusinessUserDetail objectForKey:@"user_id"];
    [self.navigationController pushViewController:wrvc animated:YES];
}
- (IBAction)cancleBtnClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtnAction:(id)sender {
    [self navigateToServiceProvider];
}

-(void)navigateToServiceProvider{
    ServiceProviderVC *serviceProviderVC = (ServiceProviderVC *)[CommonFunctions exists:[ServiceProviderVC class] in:self.navigationController];
    if(serviceProviderVC == nil){
        serviceProviderVC = [[ServiceProviderVC alloc] init];
        [self.navigationController pushViewController:serviceProviderVC animated:YES];
    } else {
        [self.navigationController popToViewController:serviceProviderVC animated:YES];
    }
}
@end
