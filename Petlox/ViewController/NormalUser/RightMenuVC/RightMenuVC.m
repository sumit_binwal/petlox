//
//  RightMenuVC.m
//  Petlox
//
//  Created by Sumit Sharma on 17/09/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "RightMenuVC.h"
#import "HomeScreenSpecsVC.h"
#import "SettingVC.h"
#import "MyReservationVC.h"
#import "ViewFullImageVC.h"
#import "FavoriteVC.h"
#import "UserReviewVC.h"
#import "MyPetVC.h"
#import "ReportBugVC.h"
#import "UserEditProfileVC.h"
#import "MessageInboxVC.h"
#import "SettingsCell.h"
#import "RightMenuTopCell.h"
#import "RightMenuMessageCell.h"
#import "HomeScreenSpecsVC.h"

@interface RightMenuVC ()<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UILabel *lblName;
    IBOutlet UIScrollView *scrllVw;
    IBOutlet UIImageView *imgVwProfile;
    IBOutlet UIView *viewForImg;
    
    IBOutlet UITableView *tblViewMenu;
}
@end

@implementation RightMenuVC
@synthesize recentVwController;

-(AppDelegate *)appDelegateObj
{
    return (AppDelegate *) [[UIApplication sharedApplication]delegate];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
            [self.view endEditing:YES];
    [self setUpView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"TestNotification"
                                               object:nil];
    
    [CommonFunctions setNavigationBar:self.navigationController];
    
//    if ([UIScreen mainScreen].bounds.size.height<568) {
//        [scrllVw setContentSize:CGSizeMake(320.0f, 568.0f)];
//        [scrllVw setScrollEnabled:YES];
//    }
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"homeImg"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClicked:)];
    btn.imageInsets = UIEdgeInsetsMake(0.0, -5, 0, 0);
    [self.navigationItem setLeftBarButtonItem:btn];
    
    
    
    
    UIBarButtonItem *btn2=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"rgtBarBtn1"] style:UIBarButtonItemStylePlain target:self action:@selector(editBtnClicked:)];
    
    [self.navigationItem setRightBarButtonItem:btn2];
}

-(void)viewDidLayoutSubviews
{
//    if ([UIScreen mainScreen].bounds.size.height<568) {
//        [scrllVw setContentSize:CGSizeMake(320.0f, 568.0f)];
//        [scrllVw setScrollEnabled:YES];
//    }
}
-(void)viewDidAppear:(BOOL)animated
{
        [self.view endEditing:YES];
    

}
- (IBAction)profileImgBtnClicked:(id)sender {
    NSString *urlStr=[[NSUserDefaults standardUserDefaults]objectForKey:UD_USER_IMG];
    ViewFullImageVC *fullImgVC=[[ViewFullImageVC alloc]initWithNibName:@"ViewFullImageVC" bundle:nil];
    fullImgVC.strImgURL=urlStr;
    [self presentViewController:fullImgVC animated:YES completion:nil];
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.view endEditing:YES];
    [tblViewMenu reloadData];
    /*
    NSString *urlStr=[[NSUserDefaults standardUserDefaults]objectForKey:UD_USER_IMG];
    if (urlStr.length>0) {
    [imgVwProfile setImageWithURL:[NSURL URLWithString:urlStr] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    else
    {
    [imgVwProfile setImage:[UIImage imageNamed:@"userProfileImg.jpg"]];
    }
    
    lblName.text=[NSString stringWithFormat:@"%@ %@",[[NSUserDefaults standardUserDefaults] objectForKey:UD_FIRST_NAME],[[NSUserDefaults standardUserDefaults] objectForKey:UD_LAST_NAME]];
    */

}



-(void)setUpView
{
//    viewForImg.layer.cornerRadius=viewForImg.frame.size.width/2;
//    imgVwProfile.layer.cornerRadius=imgVwProfile.frame.size.width/2;
//    imgVwProfile.clipsToBounds=YES;
    
    //code by chhagan
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [self.view addGestureRecognizer:recognizer];
}
- (void) didSwipe:(UISwipeGestureRecognizer *)recognizer
{
    if([recognizer direction] == UISwipeGestureRecognizerDirectionLeft)
    {
        //Swipe from right to left
        if ([recentVwController isKindOfClass:[HomeScreenSpecsVC class]]) {
            HomeScreenSpecsVC *home=[[HomeScreenSpecsVC alloc]initWithNibName:@"HomeScreenSpecsVC" bundle:nil];
            home.fromSwipe=@"yes";
            [(UINavigationController *)self.viewDeckController.centerController pushViewController:home animated:NO];
            [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:home]];
            [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
            }];
        }
        else
        {
            if ([recentVwController isKindOfClass:[ReportBugVC class]]) {
                recentVwController=[[ReportBugVC alloc]initWithNibName:@"ReportBugVC" bundle:nil];

            }
            else if ([recentVwController isKindOfClass:[MyPetVC class]])
            {
                recentVwController=[[MyPetVC alloc]initWithNibName:@"MyPetVC" bundle:nil];
            }
            else if ([recentVwController isKindOfClass:[MyReservationVC class]])
            {
                recentVwController=[[MyReservationVC alloc]initWithNibName:@"MyReservationVC" bundle:nil];
            }
            else if ([recentVwController isKindOfClass:[FavoriteVC class]])
            {
                recentVwController=[[FavoriteVC alloc]initWithNibName:@"FavoriteVC" bundle:nil];
            }
            else if ([recentVwController isKindOfClass:[UserReviewVC class]])
            {
                recentVwController=[[UserReviewVC alloc]initWithNibName:@"UserReviewVC" bundle:nil];
            }
            else if ([recentVwController isKindOfClass:[MessageInboxVC class]])
            {
                recentVwController=[[MessageInboxVC alloc]initWithNibName:@"MessageInboxVC" bundle:nil];
            }
            else if ([recentVwController isKindOfClass:[SettingVC class]])
            {
                recentVwController=[[SettingVC alloc]initWithNibName:@"SettingVC" bundle:nil];
            }
            else if ([recentVwController isKindOfClass:[UserEditProfileVC class]])
            {
                recentVwController=[[UserEditProfileVC alloc]initWithNibName:@"UserEditProfileVC" bundle:nil];
                
            }
            [(UINavigationController *)self.viewDeckController.centerController pushViewController:recentVwController animated:NO];
            [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:recentVwController]];
            [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
            }];
        }
    }
    else
    {
        //Swipe from left to Right
       
    }
}

- (void) receiveTestNotification:(NSNotification *) notification
{

    recentVwController=(UIViewController *)notification.userInfo;

}

- (IBAction)editBtnClicked:(id)sender
{
    UserEditProfileVC *home  = [[UserEditProfileVC alloc]init];
    [(UINavigationController *)self.viewDeckController.centerController pushViewController:home animated:NO];
    [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:home]];
    [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
    }];

}

-(void)navigationForScreenleftAndRightHomeScreenSpecsVC
{
    HomeScreenSpecsVC *home  = [[HomeScreenSpecsVC alloc]init];
    [(UINavigationController *)self.viewDeckController.centerController pushViewController:home animated:NO];
    [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:home]];
    [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
    }];
}
- (IBAction)myPetsBtnClicked:(id)sender
{
    MyPetVC *home  = [[MyPetVC alloc]init];
    [(UINavigationController *)self.viewDeckController.centerController pushViewController:home animated:NO];
    [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:home]];
    [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
    }];
}

- (IBAction)favriteBtnClicked:(id)sender {
    FavoriteVC *favoritViw  = [[FavoriteVC alloc]init];
    [(UINavigationController *)self.viewDeckController.centerController pushViewController:favoritViw animated:NO];
    [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:favoritViw]];
    [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
    }];

}
- (IBAction)reportABugBtnClicked:(id)sender {
    ReportBugVC *favoritViw  = [[ReportBugVC alloc]init];
    [(UINavigationController *)self.viewDeckController.centerController pushViewController:favoritViw animated:NO];
    [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:favoritViw]];
    [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
    }];

}
- (IBAction)myreservationBtnClicked:(id)sender {
    MyReservationVC *mrvc  = [[MyReservationVC alloc]init];
    [(UINavigationController *)self.viewDeckController.centerController pushViewController:mrvc animated:NO];
    [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:mrvc]];
    [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - IBActionButton Method

-(void)backBarButtonClicked:(id)sender
{
    //[self.navigationController popViewControllerAnimated:YES];
    HomeScreenSpecsVC *favoritViw  = [[HomeScreenSpecsVC alloc]init];
    [(UINavigationController *)self.viewDeckController.centerController pushViewController:favoritViw animated:NO];
    [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:favoritViw]];
    [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
    }];

}

- (IBAction)settingButtonClicked:(id)sender
{
    SettingVC *home  = [[SettingVC alloc]init];
    [(UINavigationController *)self.viewDeckController.centerController pushViewController:home animated:NO];
    [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:home]];
    [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
    }];
}
- (IBAction)allReviewBtnClicke:(id)sender {
    UserReviewVC *home  = [[UserReviewVC alloc]init];
    [(UINavigationController *)self.viewDeckController.centerController pushViewController:home animated:NO];
    [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:home]];
    [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
    }];
}

- (IBAction)leftBrBtnClicked:(id)sender {
    
[self navigationForScreenleftAndRightHomeScreenSpecsVC];
    
}
- (IBAction)messageButtonClicked:(id)sender {
    MessageInboxVC *home  = [[MessageInboxVC alloc]init];
    [(UINavigationController *)self.viewDeckController.centerController pushViewController:home animated:NO];
    [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:home]];
    [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDelegate / DataSource Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float rowHeight = 0.0f;
    if (indexPath.row == 0)
    {
        rowHeight = 175.0f;
    }
    else if (indexPath.row == 5)
    {
//        rowHeight = (86.0f * [self appDelegateObj].window.bounds.size.height)/568;
        rowHeight = (62.0f * [self appDelegateObj].window.bounds.size.height)/568;
    }
    else
    {
        rowHeight = (44 * [self appDelegateObj].window.bounds.size.height)/568;
    }
    return rowHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 8;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        static NSString *settingsCell   = @"RightMenuTopCell";
        RightMenuTopCell *cell = (RightMenuTopCell *)[tableView dequeueReusableCellWithIdentifier:settingsCell];
        
        if (cell == nil)
        {
            cell = (RightMenuTopCell*)[[[NSBundle mainBundle] loadNibNamed:@"RightMenuTopCell" owner:self options:nil] objectAtIndex:0];
            
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        NSString *urlStr=[[NSUserDefaults standardUserDefaults]objectForKey:UD_USER_IMG];
        if (urlStr.length>0) {
            [cell.imgViewTopPic setImageWithURL:[NSURL URLWithString:urlStr] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        }
        else
        {
            [cell.imgViewTopPic setImage:[UIImage imageNamed:@"DefaultProfileImg"]];
        }
        
        cell.lblName.text=[NSString stringWithFormat:@"%@ %@",[[NSUserDefaults standardUserDefaults] objectForKey:UD_FIRST_NAME],[[NSUserDefaults standardUserDefaults] objectForKey:UD_LAST_NAME]];
        
        return cell;
    }
    else if (indexPath.row == 5)
    {
        static NSString *settingsCell   = @"RightMenuMessageCell";
        RightMenuMessageCell *cell = (RightMenuMessageCell *)[tableView dequeueReusableCellWithIdentifier:settingsCell];
        
        if (cell == nil)
        {
            cell = (RightMenuMessageCell*)[[[NSBundle mainBundle] loadNibNamed:@"RightMenuMessageCell" owner:self options:nil] objectAtIndex:0];
            
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            cell.lblBadge.layer.masksToBounds = YES;
            CGFloat countLblSide = MIN(cell.lblBadge.frame.size.width, cell.lblBadge.frame.size.height);
            cell.lblBadge.layer.cornerRadius = countLblSide/2;
            [cell setNeedsLayout];
        });
        return cell;
        
    }
    else
    {
        static NSString *settingsCell   = @"SettingsCell";
        
        SettingsCell *cell = (SettingsCell *)[tableView dequeueReusableCellWithIdentifier:settingsCell];
        
        if (cell == nil)
        {
            cell = (SettingsCell*)[[[NSBundle mainBundle] loadNibNamed:@"SettingsCell" owner:self options:nil] objectAtIndex:0];
            
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        if (indexPath.row == 1)
        {
            cell.lblTxt.text = @"My Pets";
            [cell.imgVwDevider setHidden:NO];
        }
        else if (indexPath.row == 2)
        {
            cell.lblTxt.text = @"My Reservations";
            [cell.imgVwDevider setHidden:NO];
        }
        else if (indexPath.row == 3)
        {
            cell.lblTxt.text = @"Favorites";
            [cell.imgVwDevider setHidden:NO];
        }
        else if (indexPath.row == 4)
        {
            cell.lblTxt.text = @"All Reviews";
            [cell.imgVwDevider setHidden:YES];
        }
        else if (indexPath.row == 6)
        {
            cell.lblTxt.text = @"Settings";
            [cell.imgVwDevider setHidden:NO];
        }
        else if (indexPath.row == 7)
        {
            cell.lblTxt.text = @"Report a Bug";
            [cell.imgVwDevider setHidden:NO];
        }
        
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        [self profileImgBtnClicked:nil];
    }
    else if (indexPath.row == 1)
    {
        [self myPetsBtnClicked:nil];
    }
    else if (indexPath.row == 2)
    {
        [self myreservationBtnClicked:nil];
    }
    else if (indexPath.row == 3)
    {
        [self favriteBtnClicked:nil];
    }
    else if (indexPath.row == 4)
    {
        [self allReviewBtnClicke:nil];
    }
    else if (indexPath.row == 5)
    {
        [self messageButtonClicked:nil];
    }
    else if (indexPath.row == 6)
    {
        [self settingButtonClicked:nil];
    }
    else if (indexPath.row == 7)
    {
        [self reportABugBtnClicked:nil];
    }
}

@end
