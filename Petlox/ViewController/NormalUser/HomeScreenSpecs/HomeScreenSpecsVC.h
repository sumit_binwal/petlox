//
//  HomeScreenSpecsVC.h
//  DemoPetLox
//
//  Created by Chagan Singh on 14/09/15.
//  Copyright (c) 2015 Chagan Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeScreenCell.h"
#import "HomeCollectionCell.h"
#import "UIImageView+WebCache.h"
@interface HomeScreenSpecsVC : UIViewController<UIGestureRecognizerDelegate,UITextFieldDelegate>//UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate
{
     IBOutlet UITableView *tblMenuContents;
    
     IBOutlet UICollectionView *collectionVew;
    
     IBOutlet UITextField *txtSearchField;
    
    UITapGestureRecognizer *tap;
    
    NSMutableArray *arrGroomers;
    NSMutableArray *arrAllrecords;
    NSMutableArray *arrCollectionRecords;
    
    BOOL codeFirstCall;
    BOOL pageLoad;
    
    float collectionIndexSelected;
}

@property (nonatomic,strong)NSString *fromSwipe;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIButton *btnNearby;

- (IBAction)btnNearByPressed:(id)sender;
- (IBAction)txtSearchChanged:(UITextField *)sender;
- (IBAction)btnMenuPressed:(id)sender;

-(void)userComeFromBackGroundActiveState;

- (IBAction)btnGroomingPressed:(id)sender;
- (IBAction)btnWalkingPressed:(id)sender;
- (IBAction)btnSittingPressed:(id)sender;
- (IBAction)btnTrainingPressed:(id)sender;
- (IBAction)btnNextPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *imgGrooming;
@property (weak, nonatomic) IBOutlet UIImageView *imgWalking;
@property (weak, nonatomic) IBOutlet UIImageView *imgSitting;
@property (weak, nonatomic) IBOutlet UIImageView *imgTraining;

@end
