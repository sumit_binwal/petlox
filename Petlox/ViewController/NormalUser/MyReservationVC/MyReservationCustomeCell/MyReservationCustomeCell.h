//
//  MyReservationCustomeCell.h
//  Petlox
//
//  Created by Sumit Sharma on 07/12/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyReservationCustomeCell : UITableViewCell
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *directionwidthCnstraint;
@property (strong, nonatomic) IBOutlet UILabel *lblBusinessName;
@property (strong, nonatomic) IBOutlet UILabel *lblBusinessAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblDistance;
@property (strong, nonatomic) IBOutlet UILabel *lblPetname;
@property (strong, nonatomic) IBOutlet UILabel *lblReserveDate;
@property (strong, nonatomic) IBOutlet UIButton *btnDirection;
@property (strong, nonatomic) IBOutlet UILabel *lblTimeLeft;
@property (strong, nonatomic) IBOutlet UIImageView *imgBusinessProfileImg;
@property (strong, nonatomic) IBOutlet UIImageView *imgPetProfileImg;
@property (strong, nonatomic) IBOutlet UILabel *lblDirection;
@property (strong, nonatomic) IBOutlet UIImageView *lblIcon2;
@property (strong, nonatomic) IBOutlet UILabel *lblTimeReaming;
@property (strong, nonatomic) IBOutlet UIImageView *lblIcon1;

@property (nonatomic, strong) IBOutlet UIImageView *imgViewSep;

@end
