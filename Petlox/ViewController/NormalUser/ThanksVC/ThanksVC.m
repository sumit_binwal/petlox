//
//  ThanksVC.m
//  Petlox
//
//  Created by Sumit Sharma on 03/10/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "ThanksVC.h"
#import "HomeScreenSpecsVC.h"
#import "CancelReasonVC.h"
#import "CalendarVC.h"
@interface ThanksVC ()
{
    
    IBOutlet NSLayoutConstraint *bottomConstraint;
}
@end

@implementation ThanksVC
@synthesize reservationID,businessName,dictReservationDetail;
- (void)viewDidLoad {
    [super viewDidLoad];
    if ([UIScreen mainScreen].bounds.size.height<568) {
        bottomConstraint.constant=94;
    }
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:NO];
    self.navigationItem.hidesBackButton = YES;
    [APPDELEGATE showPetReservationTabBar:NO];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backToPetloxBtnClicked:(id)sender
{
   [self.navigationController popToRootViewControllerAnimated:YES];
   
}
- (IBAction)addToCalenderBtnClicked:(id)sender
{
    CalendarVC *calVC=[[CalendarVC alloc]initWithNibName:@"CalendarVC" bundle:nil];
    calVC.strbusiessName=businessName;
    calVC.strAppointedDate=[dictReservationDetail valueForKey:@"appointedDate"];
    NSLog(@"%@",calVC.strbusiessName);
        NSLog(@"%@",calVC.strAppointedDate);
    [self.navigationController pushViewController:calVC animated:YES];
}
- (IBAction)cancelBtnClicked:(id)sender
{
    CancelReasonVC *crvc=[[CancelReasonVC alloc]initWithNibName:@"CancelReasonVC" bundle:nil];
    crvc.reservationID=reservationID;
    [self.navigationController pushViewController:crvc animated:YES];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
